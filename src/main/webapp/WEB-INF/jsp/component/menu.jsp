<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<div class="horizontal-menu navbar-collapse collapse">
    <ul class="nav navbar-nav">
        <li <c:if test="${mainMenu == 'report'}">class="active"</c:if>>
                <a href="/report">
                    <i class="fa fa-files-o fa-fw fa-lg"></i>
                    &nbsp; <spring:message code="label.report"/>
            </a>
        </li>
        <sec:authorize access="hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')">
            <li <c:if test="${mainMenu == 'company'}">class="active"</c:if>>
                    <a href="/company">
                        <i class="fa fa-users fa-fw fa-lg"></i>
                        &nbsp; <spring:message code="label.company"/>
                </a>
            </li>
        </sec:authorize>
        <sec:authorize access="hasRole('ROLE_ADMIN')">
            <li <c:if test="${mainMenu == 'user'}">class="active"</c:if>>
                    <a href="/user">
                        <i class="fa fa-user fa-fw fa-lg"></i>
                        &nbsp; <spring:message code="label.user"/>
                </a>
            </li>
            <%-- <li <c:if test="${mainMenu == 'acl'}">class="active"</c:if>>
                    <a href="/acl" >
                        <i class="fa fa-shield fa-fw fa-lg"></i>
                        &nbsp; ACL
                    </a>
                </li> --%>
        </sec:authorize>
        <sec:authorize access="hasRole('ROLE_ADMIN') or hasRole('ROLE_EXECUTIVE')">
            <li <c:if test="${mainMenu == 'executive'}">class="active"</c:if>>
                    <a href="/executive">
                        <i class="fa fa-tachometer fa-fw fa-lg"></i>
                        &nbsp; Executive
                </a>
            </li>
            <li <c:if test="${mainMenu == 'history'}">class="active"</c:if>>
                    <a href="/history">
                        <i class="fa fa-clock-o fa-fw fa-lg"></i>
                        &nbsp; <spring:message code="label.history"/>
                </a>
            </li>
        </sec:authorize>

        <li <c:if test="${mainMenu == 'status_summary'}">class="active"</c:if>>
                <a href="/report/status">
                    <i class="fa fa-flag fa-fw fa-lg"></i>
                    &nbsp; <spring:message code="label.status.summary"/>
            </a>
        </li>
        <li>
            <a href="javascript:void(0);" data-toggle="modal" data-target="#help">
                <i class="fa fa-info fa-fw fa-lg"></i>
                &nbsp; <spring:message code="label.help"/>
            </a>
            <div class="modal fade" id="help" tabindex="-1" role="dialog" aria-labelledby="help">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel"><spring:message code="label.help"/></h4>
                </div>
                <div class="modal-body">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th class="text-center"><spring:message code="label.status"/></th>
                                <th class="text-center"><spring:message code="label.help.details"/></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="text-center"><span class="label label-success"><spring:message code="label.status.completed"/></span>
                                </td>
                                <td><spring:message code="label.status.completed.desc"/></td>
                            </tr>
                            <tr>
                                <td class="text-center"><span class="label label-warning"><spring:message
                                            code="label.status.cannot_give_information"/></span></td>
                                <td><spring:message code="label.status.cannot_give_information.desc"/></td>
                            </tr>
                            <tr>
                                <td class="text-center"><span class="label label-warning"><spring:message
                                            code="label.status.rejected"/></span></td>
                                <td><spring:message code="label.status.rejected.desc"/></td>
                            </tr>
                            <tr>
                                <td class="text-center"><span class="label label-primary"><spring:message
                                            code="label.status.not_yet_interviewed"/></span></td>
                                <td><spring:message code="label.status.not_yet_interviewed.desc"/></td>
                            </tr>
                            <tr>
                                <td class="text-center"><span class="label label-warning"><spring:message
                                            code="label.status.not_picked_up"/></span></td>
                                <td><spring:message code="label.status.not_picked_up.desc"/></td>
                            </tr>
                            <tr>
                                <td class="text-center"><span class="label label-warning"><spring:message
                                            code="label.status.wait_for_calling_back"/></span></td>
                                <td><spring:message code="label.status.wait_for_calling_back.desc"/></td>
                            </tr>
                            <tr>
                                <td class="text-center"><span class="label label-warning"><spring:message
                                            code="label.status.turn_off"/></span></td>
                                <td><spring:message code="label.status.turn_off.desc"/></td>
                            </tr>
                            <tr>
                                <td class="text-center"><span class="label label-warning"><spring:message
                                            code="label.status.wrong_number"/></span></td>
                                <td><spring:message code="label.status.wrong_number.desc"/></td>
                            </tr>
                            <tr>
                                <td class="text-center"><span class="label label-warning"><spring:message
                                            code="label.status.cannot_connect"/></span></td>
                                <td><spring:message code="label.status.cannot_connect.desc"/></td>
                            </tr>
                            <tr>
                                <td class="text-center"><span class="label label-warning"><spring:message
                                            code="label.status.terminate_interviews"/></span></td>
                                <td><spring:message code="label.status.terminate_interviews.desc"/></td>
                            </tr>
                            <tr>
                                <td class="text-center"><span class="label label-warning"><spring:message
                                            code="label.status.not_customer_telephone_number"/></span></td>
                                <td><spring:message code="label.status.not_customer_telephone_number.desc"/></td>
                            </tr>
                            <tr>
                                <td class="text-center"><span class="label label-warning"><spring:message
                                            code="label.status.wait_for_investigation"/></span></td>
                                <td><spring:message code="label.status.wait_for_investigation.desc"/></td>
                            </tr>
                            <tr>
                                <td class="text-center"><span class="label label-warning"><spring:message
                                            code="label.status.already_exist"/></span></td>
                                <td><spring:message code="label.status.already_exist.desc"/></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal"><spring:message
                            code="label.close"/></button>
                </div>
            </div>
        </li>
    </ul>
</div>
