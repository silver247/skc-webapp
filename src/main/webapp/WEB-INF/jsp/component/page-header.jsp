<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<div class="row">
    <div class="col-sm-12">
        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li>
                <i class="clip-home-3"></i>
                <a href="/">
                    <spring:message code="label.home"/>
                </a>
            </li>
            <li class="active">
                ${pageHeaderBig}
            </li>

        </ol>
        <div class="page-header">
            <h1>${pageHeaderBig} <small>${pageHeaderSmall}</small></h1>
        </div>
        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>