<!-- start: FOOTER -->
<div class="footer clearfix">
    <div class="footer-inner">
        2016 &copy; SKC Input System by vBit Connection.
    </div>
    <div class="footer-items">
        <span class="go-top"><i class="clip-chevron-up"></i></span>
    </div>
</div>
<!-- end: FOOTER -->