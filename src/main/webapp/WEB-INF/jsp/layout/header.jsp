<%@ page contentType="text/html; charset=utf-8"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<sec:authentication var="user" property="principal" />

<div class="navbar navbar-inverse navbar-fixed-top">
    <!-- start: TOP NAVIGATION CONTAINER -->
    <div class="container">
        <div class="navbar-header">
            <!-- start: RESPONSIVE MENU TOGGLER -->
            <button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle pull-left" type="button" style="font-size: 25px; padding-bottom: 0px; padding-top: 0px;margin-left: 9px;margin-right: 5px;">
                <span class="clip-list-2"></span>
            </button>
            <!-- end: RESPONSIVE MENU TOGGLER -->
            <!-- start: LOGO -->
            <a class="navbar-brand" href="/">
                <b>SKC</b> INPUT SYSTEM
            </a>
            <!-- end: LOGO -->
        </div>
        <div class="navbar-tools">
            <!-- start: TOP NAVIGATION MENU -->
            <ul class="nav navbar-right">

                <li class="dropdown current-user">
                    <a data-toggle="dropdown" data-hover="dropdown" class="dropdown-toggle" data-close-others="true" href="#">
                        <i class="clip-world" style="color: black;"></i>
                        <spring:message code="language.language"/>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="<c:url value='?lang=th' />">
                                <i class="fa fa-flag fa-fw"></i>
                                &nbsp; ไทย
                            </a>
                        </li>
                        <li>
                            <a href="<c:url value='?lang=${thridLangLang}' />">
                                <i class="fa fa-flag fa-fw"></i>
                                &nbsp; ${thridLangLanguage}
                            </a>
                        </li>
                        <li>
                            <a href="<c:url value='?lang=en' />">
                                <i class="fa fa-flag fa-fw"></i>
                                &nbsp; English
                            </a>
                        </li>
                    </ul>
                </li>



                <!-- start: USER DROPDOWN -->
                <sec:authorize access="isAuthenticated()">
                    <li class="dropdown current-user">
                        <a data-toggle="dropdown" data-hover="dropdown" class="dropdown-toggle" data-close-others="true" href="#">
                            <!--<img src="/assets/clip-one/images/avatar-1-small.jpg" class="circle-img" alt="">-->
                            <i class="clip-user-5"></i>
                            <span class="username">
                                ${guserProfile.firstname} ${guserProfile.lastname}
                            </span>
                            <i class="clip-chevron-down"></i>
                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="/user/me/profile">
                                    <i class="fa fa-user fa-fw"></i>
                                    &nbsp;<spring:message code="label.myProfile"/>
                                </a>
                            </li>
                            <sec:authorize access="hasRole('ROLE_ADMIN') or hasRole('ROLE_PREVIOUS_ADMINISTRATOR')">
                                <li class="divider"></li>
                            </sec:authorize>
                            <sec:authorize access="hasRole('ROLE_ADMIN') and !hasRole('ROLE_PREVIOUS_ADMINISTRATOR')">
                                <li>
                                    <a href="<c:url value='/user/list' />">
                                        <i class="fa fa-child fa-fw"></i>
                                        &nbsp; Switch user
                                    </a>
                                </li>
                            </sec:authorize>
                            <sec:authorize access="hasRole('ROLE_PREVIOUS_ADMINISTRATOR')">
                                <li>
                                    <a href="<c:url value='/switchuserlogout' />">
                                        <i class="fa fa-reply fa-fw"></i>
                                        &nbsp; Exit this user
                                    </a>
                                </li>
                            </sec:authorize>
                            <li class="divider"></li>
                            <li>
                                <a href="<c:url value='/logout' />">
                                    <i class="fa fa-sign-out fa-fw"></i>
                                    &nbsp; <spring:message code="label.logOut"/>
                                </a>
                            </li>
                        </ul>
                    </li>
                </sec:authorize>
                <!-- end: USER DROPDOWN -->
            </ul>
            <!-- end: TOP NAVIGATION MENU -->
        </div>
        <!-- start: HORIZONTAL MENU -->
        <tiles:insertAttribute name="menu" />
        <!-- end: HORIZONTAL MENU -->
    </div>
    <!-- end: TOP NAVIGATION CONTAINER -->
</div>