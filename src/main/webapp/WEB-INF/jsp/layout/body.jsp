<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>

<div class="main-container">
    <!-- start: PAGE -->
    <div class="main-content">
        <div class="container">
            <!-- start: PAGE HEADER -->
            <tiles:insertAttribute name="page-header" />
            <!-- end: PAGE HEADER -->
            <tiles:insertAttribute name="content" />
        </div>
    </div>
    <!-- end: PAGE -->
</div>