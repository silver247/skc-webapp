<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<tiles:importAttribute name="javascripts"/>
<tiles:importAttribute name="stylesheets"/>

<!DOCTYPE html>
<html>
    <head>
        <title>SKC Input System</title>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"></meta>
        <!-- start: META -->
        <meta charset="utf-8" />
        <!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <meta content="" name="description" />
        <meta content="" name="author" />


        <link rel="stylesheet" href="/assets/clip-one/plugins/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="/assets/clip-one/fonts/font-awesome-4.5.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="/assets/clip-one/fonts/style.css">
        <link rel="stylesheet" href="/assets/clip-one/css/main.css">
        <link rel="stylesheet" href="/assets/clip-one/css/main-responsive.css">
        <link rel="stylesheet" href="/assets/clip-one/plugins/iCheck/skins/all.css">
        <link rel="stylesheet" href="/assets/clip-one/plugins/bootstrap-colorpalette/css/bootstrap-colorpalette.css">
        <link rel="stylesheet" href="/assets/clip-one/plugins/perfect-scrollbar/src/perfect-scrollbar.css">
        <link rel="stylesheet" href="/assets/clip-one/css/theme_light.css" type="text/css" id="skin_color">
        <link rel="stylesheet" href="/assets/clip-one/css/print.css" type="text/css" media="print"/>

        <link rel="stylesheet" type="text/css" href='/static/fonts/thsarabunnew.css'></link>
        <link rel="stylesheet" type="text/css" href='/static/css/app.css'></link>

        <c:forEach var="stylesheet" items="${stylesheets}">
            <link rel="stylesheet" type="text/css" href='<c:url value="${stylesheet}"/>'></link>
        </c:forEach>

        <!--[if gte IE 9]><!-->
        <script src="/assets/clip-one/plugins/jQuery-lib/2.0.3/jquery.min.js"></script>
        <!--<![endif]-->
    </head>
    <body class="<tiles:insertAttribute name="bodyclass" />">
        <tiles:insertAttribute name="header" />
        <tiles:insertAttribute name="body" />
        <tiles:insertAttribute name="footer" />


        <!-- start: MAIN JAVASCRIPTS -->
        <!--[if lt IE 9]>
        <script src="static/assets/clip-one/plugins/respond.min.js"></script>
        <script src="static/assets/clip-one/plugins/excanvas.min.js"></script>
        <script type="text/javascript" src="static/assets/clip-one/plugins/jQuery-lib/1.10.2/jquery.min.js"></script>
        <![endif]-->

        <script src="/assets/clip-one/plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>
        <script src="/assets/clip-one/plugins/bootstrap/js/bootstrap.min.js"></script>
        <script src="/assets/clip-one/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"></script>
        <script src="/assets/clip-one/plugins/blockUI/jquery.blockUI.js"></script>
        <script src="/assets/clip-one/plugins/iCheck/jquery.icheck.min.js"></script>
        <script src="/assets/clip-one/plugins/perfect-scrollbar/src/jquery.mousewheel.js"></script>
        <script src="/assets/clip-one/plugins/perfect-scrollbar/src/perfect-scrollbar.js"></script>
        <script src="/assets/clip-one/plugins/less/less-1.5.0.min.js"></script>
        <script src="/assets/clip-one/plugins/jquery-cookie/jquery.cookie.js"></script>
        <script src="/assets/clip-one/plugins/bootstrap-colorpalette/js/bootstrap-colorpalette.js"></script>
        <script src="/assets/clip-one/js/main.js"></script>
        <!-- scripts -->
        <c:forEach var="script" items="${javascripts}">
            <script src="<c:url value="${script}"/>"></script>
        </c:forEach>
    </body>
</html>