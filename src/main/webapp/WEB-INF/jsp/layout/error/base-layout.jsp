<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<!DOCTYPE html>
<html lang="en" class="no-js">
    <head>
        <title>Spring</title>
        <meta charset="utf-8" />
        <!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <meta content="" name="description" />
        <meta content="" name="author" />
        <link rel="stylesheet" href="/assets/clip-one/plugins/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="/assets/clip-one/fonts/font-awesome-4.5.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="/assets/clip-one/fonts/style.css">
        <link rel="stylesheet" href="/assets/clip-one/css/main.css">
        <link rel="stylesheet" href="/assets/clip-one/css/main-responsive.css">
        <link rel="stylesheet" href="/assets/clip-one/css/theme_light.css" type="text/css" id="skin_color">
        <link rel="stylesheet" type="text/css" href='/static/css/app.css'></link>
        <link rel="shortcut icon" href="/favicon.ico" />
        <script src="/assets/clip-one/plugins/bootstrap/js/bootstrap.min.js"></script>
        <script src="/assets/clip-one/js/main.js"></script>
    </head>
    <body class="error-full-page">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 page-error">
                    <div class="error-number teal"  style="padding-bottom: 90px;">${status} (╥﹏╥)</div>
                    <div class="error-details col-sm-6 col-sm-offset-3">
                        <h3>Oops! You are stuck at ${status}</h3>
                        <p>
                            Unfortunately the page you were looking for could not be found.<br/>
                            It may be temporarily unavailable, moved or no longer exist.<br/>
                            Check the URL you entered for any mistakes and try again.<br/>
                            <a href="/" class="btn btn-teal btn-return">Return home</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>