<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div class="row">
    <div class="col-md-12 report">
<table class="table table-striped table-bordered table-hover" id="history">
<thead>
    <tr>
        <th>Deleted Date</th>
        <th>Deleted by</th>
        <th><spring:message code="label.dateOfInput"/></th>
        <th><spring:message code="label.adId"/></th>
        <th><spring:message code="label.adName"/></th>
        <th><spring:message code="label.inputOrder"/></th>
        <th><spring:message code="label.notificationNumber"/></th>
        <th><spring:message code="label.productCategory"/></th>
        <th><spring:message code="label.model"/></th>
        <th><spring:message code="label.serialNumber"/></th>
        <th><spring:message code="label.ownerName"/></th>
        <th><spring:message code="label.customerName"/></th>
        <th><spring:message code="label.customerPhoneNo"/>-1</th>
        <th><spring:message code="label.customerPhoneNo"/>-2</th>
        <th><spring:message code="label.customerPhoneNo"/>-3</th>
        <th><spring:message code="label.checkingCount"/></th>
        <th><spring:message code="label.checkingDate"/></th>
        <th><spring:message code="label.mechanic"/></th>
    </tr>
</thead>
<tbody>
<c:forEach items="${activities}" var="activity">
<tr>
<td><fmt:formatDate value="${activity.deletedDate}" pattern="YYYY-MM-dd" /></td>
<td>${activity.deletedBy.fullname}</td>
<td><fmt:formatDate value="${activity.report.serviceDate}" pattern="YYYY-MM-dd" /></td>
<td>${activity.report.company.displayId}</td>
<td>${activity.report.company.name}</td>
<td>${activity.report.orderNo}</td>
<td>${activity.report.informedCode}</td>
<td>
<c:if test="${activity.report.productCategory == 0}">
<spring:message code="label.tractor"/>
</c:if>
<c:if test="${activity.report.productCategory == 1}">
<spring:message code="label.harvester"/>
</c:if>
<c:if test="${activity.report.productCategory == 2}">
<spring:message code="label.booties"/>
</c:if>
</td>
<td>${activity.report.vehicleModelName}</td>
<td>${activity.report.vehicleBodyId}</td>
<td>${activity.report.name}</td>
<td>${activity.report.customerName}</td>
<td>${activity.report.customerPhoneNo}</td>
<td>${activity.report.customerPhoneNo2}</td>
<td>${activity.report.customerPhoneNo3}</td>
<td>${activity.report.maintenanceNumber}</td>
<td><fmt:formatDate value="${activity.report.maintenanceDate}" pattern="YYYY-MM-dd" /></td>
<td>${activity.report.mechanic.name}</td>
</tr>
</c:forEach>
</tbody>
</table>

    </div>
</div>

<script>
    $(document).ready(function () {
        Main.init();
        var oTable = $('#history').dataTable({
            dom: '<"top"lf>rt<"bottom"ip><"clear">',
            scrollX: true,
            autoWidth: true,
            deferRender: true,
            columnDefs: [
                {
                    "render": function (data, type, row) {
                        switch (type) {
                            case 'sort':
                                return data;
                            default:
                                return moment(data, 'YYYY-MM-DD').format('DD/MM/YYYY'); 
                        }
                    },
                    "targets": [0, 2, 16]
                }
            ],
            language: {
                lengthMenu: "Show _MENU_",
                search: "",
                paginate: {
                    "sPrevious": "",
                    "sNext": ""
                }
            },
            sorting: [
                [0, 'desc']
            ],
            lengthMenu: [
                [5, 10, 15, 20, -1],
                [5, 10, 15, 20, "All"] // change per page values here
            ],
            // set the initial value
            pageLength: 10
        });

    });
</script>
