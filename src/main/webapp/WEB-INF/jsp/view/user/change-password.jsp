<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div class="col-md-offset-3 col-md-6">
    <form method="post" action="/user/${user.id}/change_password" role="form" id="form">
        <!--div class="col-md-12">
            <div class="successHandler alert alert-danger no-display">
                <i class="fa fa-times-sign"></i> You have some form errors. Please check below.
            </div>
        </div-->
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label">
                        <spring:message code="label.password"/> <span class="symbol required"></span>
                    </label>
                    <input type="password" class="form-control" name="password" id="password" required>
                </div>
                <div class="form-group">
                    <label class="control-label">
                        <spring:message code="label.confirmPassword"/> <span class="symbol required"></span>
                    </label>
                    <input type="password" class="form-control" id="password_again" name="password_again" required>
                </div>
                <input type="hidden" id="csrf" name="${_csrf.parameterName}" value="${_csrf.token}" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div>
                    <span class="symbol required"></span><spring:message code="label.requiredFields"/>
                    <hr>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <button id="changep" class="btn btn-yellow btn-block" type="submit" onclick="">
                    <spring:message code="label.changePassword"/> <i class="fa fa-arrow-circle-right"></i>
                </button>
            </div>
        </div>
    </form>
</div>

<script>
    $(document).ready(function () {
        Main.init();
        FormValidator.init();
        $("#changep").click(function () {
            if ($('#form').valid()) {
                $.post("/user/${user.id}/change_password", {password: $('#password').val(), "_csrf": $("#csrf").val()})
                        .done(function (data) {
                            window.location.href = '/report';
                        });
            }
        });
    });
</script>
