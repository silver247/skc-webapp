<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<div class="row">
    <div class="col-md-12">
        <a href="/user/${user.id}/change_password" class="btn btn-warning"><i class="fa fa-key fa-fw"></i>&nbsp; <spring:message code="label.changePassword"/></a>
        <c:if test="${profile}">
          <a href="/user/me/profile/edit" class="btn btn-teal"><i class="fa fa-edit fa-fw"></i>&nbsp; <spring:message code="label.edit"/></a>
        </c:if>
        <c:if test="${!profile}">
          <a href="/user/${user.id}/edit" class="btn btn-teal"><i class="fa fa-edit fa-fw"></i>&nbsp; <spring:message code="label.edit"/></a>
        </c:if>
        <sec:authorize access="hasRole('ROLE_ADMIN')">
          <a href="<c:url value='/switchuserto?username=${userProfile.user.username}' />" class="btn btn-danger tooltips pull-right" data-placement="top" data-original-title="Switch to ${userProfile.firstname} ${userProfile.lastname}"><i class="fa fa-child fa-fw"></i>&nbsp; Switch to this</a>
        </sec:authorize>
    </div>
</div>
<br/>
<div class="row">
    <div class="col-md-12">
        <table id="user" class="table table-bordered table-striped" style="clear: both">
            <tbody>
                <tr>
                    <td class="column-left"><spring:message code="label.firstname"/></td>
                    <td class="column-right">
                        ${userProfile.firstname}
                    </td>
                </tr>
                <tr>
                    <td class="column-left"><spring:message code="label.lastname"/></td>
                    <td class="column-right">
                        ${userProfile.lastname}
                    </td>
                </tr>
                <tr>
                    <td class="column-left">Username</td>
                    <td class="column-right">
                        ${user.username}
                    </td>
                </tr>
                <tr>
                    <td class="column-left">Email</td>
                    <td class="column-right">
                        ${user.email}
                    </td>
                </tr>
                <tr>
                    <td class="column-left"><spring:message code="label.company"/></td>
                    <td class="column-right">
                        ${company.name}
                    </td>
                </tr>
                <tr>
                    <td class="column-left"><spring:message code="label.role"/></td>
                    <td class="column-right">
                        ${authority.name}
                    </td>
                </tr>

            </tbody>
        </table>
    </div>
</div>

<script>
    $(document).ready(function () {
        Main.init();
    });
</script>
