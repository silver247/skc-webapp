<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<div class="col-md-offset-3 col-md-6">
    <form method="post" action="/user/${user.getId()}/edit" role="form" id="form">
        <div class="row">
            <div class="col-md-12">
                <div class="errorHandler alert alert-danger no-display">
                    <i class="fa fa-times-sign"></i> You have some form errors. Please check below.
                </div>
                <div class="successHandler alert alert-success no-display">
                    <i class="fa fa-ok"></i> Your form validation is successful!
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label">
                        <spring:message code="label.firstname"/> <span class="symbol required"></span>
                    </label>
                    <input type="text" placeholder="Insert your First Name" class="form-control" id="firstname" name="firstname" value="${userProfile.getFirstname()}" required>
                </div>
                <div class="form-group">
                    <label class="control-label">
                        <spring:message code="label.lastname"/> <span class="symbol required"></span>
                    </label>
                    <input type="text" placeholder="Insert your Last Name" class="form-control" id="lastname" name="lastname" value="${userProfile.getLastname()}" required>
                </div>
                <div class="form-group">
                    <label class="control-label">
                        Username <span class="symbol required"></span>
                    </label>
                    <input type="text" placeholder="username" class="form-control" id="username" name="username" value="${user.getUsername()}" required>
                </div>
                <div class="form-group">
                    <label class="control-label">
                        Email Address <span class="symbol required"></span>
                    </label>
                    <input type="email" placeholder="Email address" class="form-control" id="email" name="email" value="${user.getEmail()}" required>
                </div>
                <sec:authorize access="hasRole('ROLE_ADMIN')">
                  <div class="form-group">
                      <label class="control-label">
                          <spring:message code="label.company"/> <span class="symbol required"></span>
                      </label>
                      <select name="companyId" id="company-select2" class="form-control search-select">
                          <c:forEach items="${companies}" var="comp">
                              <option value="${comp.id}" <c:if test="${comp.id == company.id}">selected</c:if>>${comp.name}</option>
                          </c:forEach>
                      </select>
                  </div>

                  <div class="form-group">
                      <label class="control-label">
                          <spring:message code="label.role"/> <span class="symbol required"></span>
                      </label>
                      <select name="authorityId" id="role-select2" class="form-control search-select">
                          <c:forEach items="${authorities}" var="authority">
                              <option value="${authority.id}" <c:if test="${authority.id == userAuthority.authority.id}">selected</c:if>>${authority.name}</option>
                          </c:forEach>
                      </select>
                  </div>
                </sec:authorize>

                <input type="hidden" name="${_csrf.parameterName}" 	value="${_csrf.token}" />
            </div
        </div>
        <div class="row">
            <div class="col-md-12">
                <div>
                    <span class="symbol required"></span><spring:message code="label.requiredFields"/>
                    <hr>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <button class="btn btn-yellow btn-block" type="submit">
                    <spring:message code="label.update"/> <i class="fa fa-arrow-circle-right"></i>
                </button>
            </div>
        </div>
    </form>
</div>

<script>
    $(document).ready(function () {
        Main.init();

        $("#company-select2").select2();
        $("#role-select2").select2();
    });
</script>
