<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div class="col-md-offset-3 col-md-6">
    <form method="post" action="/user/create" role="form" id="form">
        <div class="row">
            <div class="col-md-12">
                <div class="errorHandler alert alert-danger no-display">
                    <i class="fa fa-times-sign"></i> You have some form errors. Please check below.
                </div>
                <div class="successHandler alert alert-success no-display">
                    <i class="fa fa-ok"></i> Your form validation is successful!
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label">
                        <spring:message code="label.firstname"/> <span class="symbol required"></span>
                    </label>
                    <input type="text" placeholder="Insert your First Name" class="form-control" id="firstname" name="firstname" required>
                </div>
                <div class="form-group">
                    <label class="control-label">
                        <spring:message code="label.lastname"/> <span class="symbol required"></span>
                    </label>
                    <input type="text" placeholder="Insert your Last Name" class="form-control" id="lastname" name="lastname" required>
                </div>
                <div class="form-group">
                    <label class="control-label">
                        Username <span class="symbol required"></span>
                    </label>
                    <input type="text" placeholder="username" class="form-control" id="username" name="username" required>
                </div>
                <div class="form-group">
                    <label class="control-label">
                        Email Address <span class="symbol required"></span>
                    </label>
                    <input type="email" placeholder="Email address" class="form-control" id="email" name="email" required>
                </div>
                <div class="form-group">
                    <label class="control-label">
                        <spring:message code="label.password"/> <span class="symbol required"></span>
                    </label>
                    <input type="password" class="form-control" name="password" id="password" required>
                </div>
                <div class="form-group">
                    <label class="control-label">
                        <spring:message code="label.confirmPassword"/> <span class="symbol required"></span>
                    </label>
                    <input type="password" class="form-control" id="password_again" name="password_again" required>
                </div>
                <div class="form-group">
                    <label class="control-label">
                        <spring:message code="label.company"/> <span class="symbol required"></span>
                    </label>
                    <select name="companyId" id="company-select2" class="form-control search-select">
                        <c:forEach items="${companies}" var="company">
                            <option value="${company.getId()}">${company.getName()}</option>
                        </c:forEach>
                    </select>
                </div>

                <div class="form-group">
                    <label class="control-label">
                        <spring:message code="label.role"/> <span class="symbol required"></span>
                    </label>
                    <select name="authorityId" id="role-select2" class="form-control search-select">
                        <c:forEach items="${authorities}" var="authority">
                            <option value="${authority.id}">${authority.name}</option>
                        </c:forEach>
                    </select>
                </div>

                <input type="hidden" name="${_csrf.parameterName}" 	value="${_csrf.token}" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div>
                    <span class="symbol required"></span><spring:message code="label.requiredFields"/>
                    <hr>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <button class="btn btn-yellow btn-block" type="submit">
                    <spring:message code="label.register"/> <i class="fa fa-arrow-circle-right"></i>
                </button>
            </div>
        </div>
    </form>
</div>

<script>
    $(document).ready(function () {
        Main.init();

        $("#company-select2").select2();
        $("#role-select2").select2();
    });
</script>