<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div class="row">
    <div class="col-md-12">
        <a href="/user/new" class="btn btn-primary">
            <i class="fa fa-plus-circle"></i>
            <spring:message code="label.newUser"/>
        </a>
    </div>
</div>
<br/>
<div class="row">
    <div class="col-md-12">
        <table class="table table-striped table-bordered table-hover table-full-width" id="sample_1">
            <thead>
                <tr>
                    <th><spring:message code="label.firstname"/></th>
                    <th><spring:message code="label.lastname"/></th>
                    <th>Email</th>
                    <th>Username</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach items="${userProfiles}" var="userprofile">
                    <tr>
                        <td>${userprofile.firstname}</td>
                        <td>${userprofile.lastname}</td>
                        <td>${userprofile.user.email}</td>
                        <td>${userprofile.user.username}</td>
                        <td>
                            <a href="<c:url value='/user/${userprofile.getUser().getId()}' />" class="btn btn-info tooltips" data-placement="top" data-original-title="Detail"><i class="fa fa-share-square"></i></a>
                            <a href="<c:url value='/user/${userprofile.getUser().getId()}/edit' />" class="btn btn-teal tooltips" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>
                        </td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
    </div>

</div>



<script>
    $(document).ready(function () {
        Main.init();
        TableData.init();
    });
</script>