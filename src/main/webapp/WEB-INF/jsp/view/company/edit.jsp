<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<div class="col-md-offset-3 col-md-6">
    <form method="post" action="/company/${company.getId()}/edit" role="form" id="form">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label">
                        <spring:message code="label.name"/> <span class="symbol required"></span>
                    </label>
                    <input type="text" placeholder="Insert company name" class="form-control" id="name" name="name" value="${company.name}" required>
                </div>
                <div class="form-group">
                    <label class="control-label">
                        <spring:message code="label.adId"/> <span class="symbol required"></span>
                    </label>
                    <input type="text" placeholder='<spring:message code="label.adId"/>' class="form-control" id="displayId" name="displayId" value="${company.displayId}" required>
                </div>
                
                <input type="hidden" name="${_csrf.parameterName}" 	value="${_csrf.token}" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div>
                    <span class="symbol required"></span><spring:message code="label.requiredFields"/>
                    <hr>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <button class="btn btn-yellow btn-block" type="submit">
                    <spring:message code="label.updateCompany"/> <i class="fa fa-arrow-circle-right"></i>
                </button>
            </div>
        </div>
    </form>
</div>