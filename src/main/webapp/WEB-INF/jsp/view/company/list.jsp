<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<sec:authorize access="hasRole('ROLE_ADMIN')">
    <div class="row">
        <div class="col-md-2">
            <a href="/company/new" class="btn btn-primary">
                <i class="fa clip-plus-circle-2"></i>
                <spring:message code="label.addNew"/>
            </a>
        </div>
    </div>
</sec:authorize>
<br/>

<div class="row">
    <div class="col-md-12">
        <table class="table table-striped table-bordered table-hover table-full-width" id="sample_1">
            <thead>
                <tr>
                    <th><spring:message code="label.name"/></th>
                    <th><spring:message code="label.adId"/></th>
                    <th style="width: 10%">Action</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach items="${companies}" var="company">
                    <tr>
                        <td>${company.name}</td>
                        <td>${company.displayId}</td>
                        <td>
                            <a href="<c:url value='/company/${company.id}' />" class="btn btn-info tooltips" data-placement="top" data-original-title='<spring:message code="label.detail"/>'><i class="fa fa-share-square"></i></a>
                            <sec:authorize access="hasRole('ROLE_ADMIN')">
                                <a href="<c:url value='/company/${company.id}/edit' />" class="btn btn-teal tooltips" data-placement="top" data-original-title='<spring:message code="label.edit"/>'><i class="fa fa-edit"></i></a>
                            </sec:authorize>
                </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
</div>


<script>
    $(document).ready(function () {
        Main.init();
        TableData.init();
    });
</script>