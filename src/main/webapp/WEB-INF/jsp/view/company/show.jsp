<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<sec:authorize access="hasRole('ROLE_ADMIN') and !hasRole('ROLE_PREVIOUS_ADMINISTRATOR')">
 <div class="row space20">
    <div class="col-md-12">
        <a href="/company/${company.getId()}/edit" class="btn btn-default">Edit</a>
    </div>
</div>
</sec:authorize>

<div class="row">
    <div class="col-md-12 space20">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i>
                <spring:message code="label.mechanic"/>
                <div class="panel-tools">
                    <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
                    </a>
                </div>
            </div>
            <div class="panel-body">
                <div class="col-md-12 space20" href="#responsive" data-toggle="modal">
                    <button class="btn btn-green add-row">
                        <i class="fa fa-plus"></i> <spring:message code="label.addMechanic"/>
                    </button>
                </div>
                <table class="table table-hover" id="sample-table-1">
                    <thead>
                        <tr>
                            <th><spring:message code="label.name"/></th>
                            <th><spring:message code="label.status"/></th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${mechanics}" var="mechanic">
                            <tr>
                                <td>${mechanic.name}</td>
                                <td id="${mechanic.id}-active">
                                    <c:if test="${mechanic.active}">
                                        <span class="btn btn-xs btn-success">Active</span>
                                    </c:if>
                                    <c:if test="${!mechanic.active}">
                                        <span class="btn btn-xs btn-bricky">Inactive</span>
                                    </c:if>
                                </td>
                                <td>
                                    <a href="#update" data-toggle="modal" data-id="${mechanic.getId()}" data-name="${mechanic.getName()}" class="btn btn-teal tooltips updateBtn" ><i class="fa fa-edit"></i></a>
                                    <a data-id="${mechanic.getId()}" class="btn btn-info tooltips toggleActive" ><i class="fa fa-check"></i></a>
                                </td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!--Add new mechanic modal-->
<div id="responsive" class="modal fade" tabindex="-1" data-width="760" style="display: none;">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            &times;
        </button>
        <h4 class="modal-title"><spring:message code="label.addMechanic"/></h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label">
                        <spring:message code="label.name"/> <span class="symbol required"></span>
                    </label>
                    <input type="text" placeholder='<spring:message code="label.name"/>' class="form-control" id="name" name="name" required>
                </div>
                <input type="hidden" id="companyid" value="${company.id}" />
                <input type="hidden" id="csrf" value="${_csrf.token}" />
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" id="closemodal" data-dismiss="modal" class="btn btn-light-grey">
            <spring:message code="label.close"/>
        </button>
        <button type="button" id="createmechanic"class="btn btn-blue">
            <spring:message code="label.addMechanic"/>
        </button>
    </div>
</div>

<!--update mechanic modal-->
<div id="update" class="modal fade" tabindex="-1" data-width="760" style="display: none;">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            &times;
        </button>
        <h4 class="modal-title"><spring:message code="label.updateMechanic"/></h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label">
                        <spring:message code="label.name"/> <span class="symbol required"></span>
                    </label>
                    <input type="text"  class="form-control" id="mechanicname" name="name" required>
                </div>
                <input type="hidden" id="mechanicid" value="" />
                <input type="hidden" id="updatecsrf" value="${_csrf.token}" />
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" id="closemodal" data-dismiss="modal" class="btn btn-light-grey">
            <spring:message code="label.close"/>
        </button>
        <button type="button" id="updatemechanic"class="btn btn-warning">
            <spring:message code="label.update"/>
        </button>
    </div>
</div>

<script>
    $(document).ready(function () {
        Main.init();

        $("#createmechanic").click(function () {
            $.post("/company/"+$('#companyid').val()+"/mechanic/create", {name: $('#name').val(), "_csrf": $("#csrf").val()})
                .done(function (data) {
                    location.reload();
                });
        })
        
        $(".updateBtn").click(function(){
            var self = this;
            $('#mechanicname').val($(self).data("name"));
            $('#mechanicid').val($(self).data('id'));
        })
        
        $("#updatemechanic").click(function(){
            $.post("/mechanic/" + $("#mechanicid").val() + "/edit", {name: $('#mechanicname').val(), "_csrf": $("#updatecsrf").val()})
                .done(function (data) {
                    location.reload();
                });
        })
        
        $(".toggleActive").click(function(){
            var self = this;
            $.post("/mechanic/" +  $(self).data('id') + "/toggle_active", {"_csrf": $("#updatecsrf").val()})
                    .done(function(data){
                        location.reload();
                    });
            
        })
    });
</script>