<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div class="row">
    <div class="col-md-12" style="margin-bottom: 1em;">
        <div id="exec-title" class="alert alert-info">-
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12" style="margin-bottom: 1em;">
        <div class="row">
            <div class="col-md-3" style="margin-bottom: 1em;">
                <div class="table-wrapper">
                    <table class="clickable-table" data-href="/executive/positive">
                        <tr>
                            <td class="eh-table-title" colspan="2">เสียงด้านบวกของลูกค้า</td>
                        </tr>
                        <tr>
                            <td class="eh-positive-number eh-number" id="positive-voices">-</td>
                            <td class="eh-units"><span class="eh-units-title">Positive</span><br>voices</td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="col-md-3" style="margin-bottom: 1em;">
                <div class="table-wrapper">
                    <table class="clickable-table" data-href="/executive/negative">
                        <tr>
                            <td class="eh-table-title" colspan="2">เสียงด้านลบของลูกค้า</td>
                        </tr>
                        <tr>
                            <td class="eh-negative-number eh-number" id="negative-voices">-</td>
                            <td class="eh-units"><span class="eh-units-title">Negative</span><br>voices</td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="col-md-3" style="margin-bottom: 1em;">
                <div class="table-wrapper">
                    <table class="clickable-table" data-href="/executive/suggestion">
                        <tr>
                            <td class="eh-table-title" colspan="2">ข้อเสนอแนะของลูกค้า</td>
                        </tr>
                        <tr>
                            <td class="eh-suggestion-number eh-number" id="suggestion-voices">-</td>
                            <td class="eh-units"><span class="eh-units-title">Suggestion</span><br>voices</td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="col-md-3" style="margin-bottom: 1em;">
                <div class="table-wrapper">
                    <table class="clickable-table" data-href="/executive/complaint">
                        <tr>
                            <td class="eh-table-title" colspan="2">ข้อร้องเรียนของลูกค้า</td>
                        </tr>
                        <tr>
                            <td class="eh-complaint-number eh-number" id="complaint-voices">-</td>
                            <td class="eh-units"><span class="eh-units-title">Complaint</span><br>voices</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        Main.init();
        FormValidator.init();
        var digits = function (num) {
            return (num.toString()).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
        };
        var updateCsiHome = function () {
            $.ajax({
                url: "/executive/csi_home",
                success: function (data) {
                    if (data.success) {
                        $("#exec-title").html(data.title);
                        $("#positive-voices").html(digits(data.positive));
                        $("#negative-voices").html(digits(data.negative));
                        $("#suggestion-voices").html(digits(data.suggestion));
                        $("#complaint-voices").html(digits(data.complaint));
                    }
                }
            });
        };
        updateCsiHome();

        $("table.clickable-table").on('click', function () {
            var $this = $(this);
            window.location.href = $this.data("href");
        });
    });
</script>
