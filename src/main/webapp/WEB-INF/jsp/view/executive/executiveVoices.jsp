<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div class="bg-gray">
    <div class="row voice">
        <div class="col-md-2">
            <select id="s2year" name="year" class="form-control search-select" placeholder="ค้นหาจากปี">
                <option></option>
                <c:forEach var="year" items="${years}">
                    <option value="${year}">${year}</option>
                </c:forEach>
            </select>
        </div>
        <div class="col-md-2">
            <select id="s2shopName" name="shopName" class="form-control search-select" placeholder="ค้นหาจากร้านตัวแทนจำหน่าย" multiple="multiple">
                <c:forEach var="company" items="${companies}">
                    <option value="${company.id}">${company.name}</option>
                </c:forEach>
            </select>
        </div>
        <div class="col-md-2">
            <select id="s2checkNumber" name="checkNumber" class="form-control search-select" placeholder="ค้นหาจากครั้งที่ตรวจ" multiple="multiple">
                <c:forEach var="chkNumber" items="${chkNumbers}">
                    <option value="${chkNumber}">${chkNumber}</option>
                </c:forEach>
            </select>
        </div>
        <div class="col-md-2">
            <select id="s2technicianName" name="technicianName" class="form-control search-select" placeholder="ค้นหาจากช่าง" multiple="multiple">
                <c:forEach var="mechanic" items="${mechanics}">
                    <option value="${mechanic.id}" data-company-id="${mechanic.company.id}">${mechanic.name} [${mechanic.company.name}]</option>
                </c:forEach>
            </select>
        </div>
        <div class="col-md-2">
            <select id="s2customerVoice" name="customerVoice" class="form-control search-select" placeholder="ค้นหาจากเสียงลูกค้า" multiple="multiple">
                <c:forEach var="voice" items="${voices}">
                    <option value="${voice['vid']}">${voice['title']}</option>
                </c:forEach>
            </select>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <h2 class="voice-header">เสียงของลูกค้าทั้งหมด <span id="voice-total-count">0</span></h2>
        </div>
    </div>

    <div class="row">
        <div class="col-md-7">
            <br>
            <canvas id="voice-chart"></canvas>
        </div>
        <div class="col-md-5">
            <div class="row">
                <div class="col-md-6">
                    <div class="voice-div" style="height: 200px;">
                        <table id="topMentionedRemark" class="voice-table">
                            <tr>
                                <td class="" colspan="2">เสียงที่ลูกค้าพูดถึงมากที่สุด</td>
                            </tr>
                            <tr>
                                <td class="voice-counter"></td>
                                <td class="${voiceType}-color"><i class="fa fa-comments-o fa-4x"></i></td>
                            </tr>
                            <tr>
                                <td class="voice-title" colspan="2"></td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="voice-div" style="height: 200px;">
                        <table id="topMentionedMonth" class="voice-table">
                            <tr>
                                <td class="" colspan="2">เดือนที่ลูกค้าพูดถึงมากที่สุด</td>
                            </tr>
                            <tr>
                                <td class="voice-counter"></td>
                                <td class="${voiceType}-color"><i class="fa fa-calendar fa-4x"></i></td>
                            </tr>
                            <tr>
                                <td colspan="2">เดือน <span class="voice-title"></span></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="voice-div" style="height: 200px;">
                        <table id="topMentionedShop" class="voice-table">
                            <tr>
                                <td class="" colspan="2">ร้านที่ลูกค้าพูดถึงมากที่สุด</td>
                            </tr>
                            <tr>
                                <td class="voice-counter"></td>
                                <td class="${voiceType}-color"><i class="fa fa-home fa-4x"></i></td>
                            </tr>
                            <tr>
                                <td class="voice-title" colspan="2"></td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="voice-div" style="height: 200px;">
                        <table id="topMentionedMechanic" class="voice-table">
                            <tr>
                                <td class="" colspan="2">ช่างที่ลูกค้าพูดถึงมากที่สุด</td>
                            </tr>
                            <tr>
                                <td class="voice-counter"></td>
                                <td class="${voiceType}-color"><span class="fa-stack fa-2x">
                                        <i class="fa fa-user fa-stack-2x"></i>
                                        <i class="fa fa-wrench fa-stack-1x fa-inverse" style="margin-left: 8px; margin-top: 8px;"></i>
                                    </span></td>
                            </tr>
                            <tr>
                                <td class="voice-title" colspan="2"></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <table id="topAnnualMentionedRemarks" class="top-voice-table">
                <thead>
                    <tr class="${voiceType}-bg">
                        <th colspan="2">TOP 3 เสียงที่ลูกค้าพูดถึงมากที่สุดทั้งปี</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="vt0-title"></td>
                        <td class="vt0-count pull-right"></td>
                    </tr>
                    <tr>
                        <td class="vt1-title"></td>
                        <td class="vt1-count pull-right"></td>
                    </tr>
                    <tr>
                        <td class="vt2-title"></td>
                        <td class="vt2-count pull-right"></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="col-md-4">
            <table id="topAnnualMentionedShops" class="top-voice-table">
                <thead>
                    <tr class="${voiceType}-bg">
                        <th colspan="2">TOP 3 ร้านที่ลูกค้าพูดถึงมากที่สุดทั้งปี</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="vt0-title"></td>
                        <td class="vt0-count pull-right"></td>
                    </tr>
                    <tr>
                        <td class="vt1-title"></td>
                        <td class="vt1-count pull-right"></td>
                    </tr>
                    <tr>
                        <td class="vt2-title"></td>
                        <td class="vt2-count pull-right"></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="col-md-4">
            <table id="topAnnualMentionedMechanics" class="top-voice-table">
                <thead>
                    <tr class="${voiceType}-bg">
                        <th colspan="2">TOP 3 ช่างที่ลูกค้าพูดถึงมากที่สุดทั้งปี</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="vt0-title"></td>
                        <td class="vt0-count pull-right"></td>
                    </tr>
                    <tr>
                        <td class="vt1-title"></td>
                        <td class="vt1-count pull-right"></td>
                    </tr>
                    <tr>
                        <td class="vt2-title"></td>
                        <td class="vt2-count pull-right"></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <table class="table table-hover voice-month-table" id="all-voices-table">
                <thead>
                    <tr>
                        <th class="center">
                            <c:if test="${voiceType == 'positive'}">เสียงด้านบวก</c:if>
                            <c:if test="${voiceType == 'negative'}">เสียงด้านลบ</c:if>
                            <c:if test="${voiceType == 'suggestion'}">ข้อเสนอแนะ</c:if>
                            <c:if test="${voiceType == 'complaint'}">ข้อร้องเรียน</c:if>ของลูกค้า</th>
                            <th>ม.ค.</th>
                            <th>ก.พ.</th>
                            <th>มี.ค.</th>
                            <th>เม.ย.</th>
                            <th>พ.ค.</th>
                            <th>มิ.ย.</th>
                            <th>ก.ค.</th>
                            <th>ส.ค.</th>
                            <th>ก.ย.</th>
                            <th>ต.ค.</th>
                            <th>พ.ย.</th>
                            <th>ธ.ค.</th>
                            <th>รวม</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <script>
        $('.container').addClass('bg-gray');
        $(document).ready(function () {
            Main.init();
            FormValidator.init();
            var digits = function (num) {
                return (num.toString()).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
            };

            var s2year = $("#s2year").select2({
            }).on("change", function (e) {
                updateCsi();
            });
            var s2shopName = $("#s2shopName").multiselect({
                placeholder: "ค้นหาจากร้านตัวแทนจำหน่าย",
                selectAll: true
            }).on("change", function (e) {
                updateTechnicianSelector();
                updateCsi();
            });
            var s2checkNumber = $("#s2checkNumber").multiselect({
                placeholder: "ค้นหาจากครั้งที่ตรวจ",
                selectAll: true
            }).on("change", function (e) {
                updateCsi();
            });
            var orgTechnicians = $("#s2technicianName").clone();
            var s2technicianName = $("#s2technicianName").multiselect({
                placeholder: "ค้นหาจากช่าง",
                selectAll: true
            }).on("change", function (e) {
                updateCsi();
            });
            var s2customerVoice = $("#s2customerVoice").multiselect({
                placeholder: "ค้นหาจากเสียงลูกค้า",
                selectAll: true
            }).on("change", function (e) {
                updateCsi();
            });
            var tupdating = false;
            // this code remember previously selected technicians and filter technician by company
            var updateTechnicianSelector = function () {
                if (tupdating) {
                    return;
                }
                tupdating = true;
                setTimeout(function () {
                    var oarray = [];
                    // remember selected mechanics
                    var selectedOptions = {};
                    s2technicianName.find('option:selected').each(function (i, elx) {
                        var $elx = $(elx);
                        selectedOptions[$elx.attr('value')] = true;
                    });
                    orgTechnicians.find('option').each(function (i, el) {
                        var $el = $(el);
                        var comId = $el.data('company-id');
                        if (!s2shopName.val() || (comId && s2shopName.val().indexOf(comId) > -1)) {
                            var itel = $el.clone();
                            var itselected = selectedOptions.hasOwnProperty(itel.attr('value'));
                            if (itselected) {
                                itel.prop('selected', true);
                            } else {
                                itel.removeProp('selected');
                            }
                            oarray.push(itel);
                        }
                    });

                    s2technicianName.html('');
                    s2technicianName.append(oarray);
                    s2technicianName.multiselect('reload');
                    tupdating = false;
                }, 400);
            };
            var updateTop = function (tname, data) {
                var $e = $('#' + tname);
                $e.find('.voice-counter').html(digits(data.count));
                $e.find('.voice-title').html(data.title);
            };
            var updateTop3 = function (tname, data) {
                var $e = $('#' + tname);
                for (var i = 0; i < 3; i++) {
                    var vtCount = '&nbsp;';
                    var vtTitle = '&nbsp;';
                    if (i < data.length) {
                        var vtCount = data[i].count;
                        var vtTitle = data[i].title;
                    }
                    $e.find('.vt' + i + '-count').html(digits(vtCount));
                    $e.find('.vt' + i + '-title').html(vtTitle);
                }
            };
            var updateAllVoiceTable = function (data) {
                var $e = $('#all-voices-table');
                var $tb = $e.find('tbody');
                $tb.html('');
                var total = 0;
                for (var i = 0; i < data.length; i++) {
                    var dd = data[i];
                    var $tr = $('<tr>');
                    $tr.append($('<td>', {"class": dd.class}).html(dd.title));
                    for (var j = 0; j < dd.values.length; j++) {
                        var $acount = $('<span>')
                        .data('month', j + 1)
                        .data('voiceValue', dd.voiceValue)
                        .data('count', dd.values[j])
                        .html(digits(dd.values[j]));
                        if (dd.values[j]) {
                            $acount.addClass("voice-cmodal");
                            $acount.on('click', function () {
                                var $t = $(this);
                                $.ajax({
                                    url: "/executive/csi/people/${voiceType}",
                                    method: 'post',
                                    data: {
                                        year: s2year.val(),
                                        companies: s2shopName.val(),
                                        chkNumbers: s2checkNumber.val(),
                                        mechanics: s2technicianName.val(),
                                        month: $t.data('month'),
                                        voiceValue: $t.data('voiceValue')
                                    },
                                    success: function (data) {
                                        if (data.success) {
                                            var $tbody = $('#voices-people').find('.modal-body tbody');
                                            $tbody.html('');
                                            $.each(data.voices, function(ind, v) {
                                                var $trm = $('<tr>').appendTo($tbody);
                                                $trm.append($('<td>').html(v.interviewee));
                                                $trm.append($('<td>', {"class":"text-center"}).html(v.contactPhone));
                                                $trm.append($('<td>', {"class":"text-center"}).html(v.model));
                                                $trm.append($('<td>', {"class":"text-center"}).html(v.chkNumber));
                                                $trm.append($('<td>', {"class":"text-center"}).html(v.chkDate));
                                                $trm.append($('<td>').html(v.mechanicName));
                                                $trm.append($('<td>').html(v.companyName));
                                                $trm.append($('<td>').html(v.voiceTitle));
                                            });
                                            $('#voices-people').modal("show");
                                        }
                                    }
                                });
                            });
                        }

                        $tr.append($('<td>', {"class": dd.class}).html($acount));
                        total = dd.values[j];
                    }
                    $tb.append($tr);
                }
                $('#voice-total-count').html(digits(total));
            };
            var csiupdating = false;
            var updateCsi = function () {
                if (csiupdating) {
                    return;
                }
                csiupdating = true;
                setTimeout(function () {
                    $.ajax({
                        url: "/executive/csi_${voiceType}",
                        method: 'post',
                        data: {
                            year: s2year.val(),
                            companies: s2shopName.val(),
                            chkNumbers: s2checkNumber.val(),
                            mechanics: s2technicianName.val(),
                            voices: s2customerVoice.val()
                        },
                        success: function (data) {
                            if (data.success) {
                                updateTop('topMentionedRemark', data.topMentionedRemark);
                                updateTop('topMentionedMonth', data.topMentionedMonth);
                                updateTop('topMentionedShop', data.topMentionedShop);
                                updateTop('topMentionedMechanic', data.topMentionedMechanic);
                                updateTop3('topAnnualMentionedRemarks', data.topAnnualMentionedRemarks);
                                updateTop3('topAnnualMentionedShops', data.topAnnualMentionedShops);
                                updateTop3('topAnnualMentionedMechanics', data.topAnnualMentionedMechanics);
                                updateAllVoiceTable(data.voices);
                                updateChart(data.datasets);
                            }
                        }
                    });
                    csiupdating = false;
                }, 400);
            };
            updateCsi();

            var ctx = $("#voice-chart");
            var MONTHS = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
            var config = {
                type: 'line',
                data: {
                    labels: MONTHS,
                    datasets: []
                },
                options: {
                    lineTension: 0,
                    legend: {
                        display: false
                    },
                    responsive: true,
                    title: {
                        display: false,
                        text: "Chart.js Line Chart - Stacked Area",
                        fontSize: 40
                    },
                    tooltips: {
                        mode: 'index'
                    },
                    hover: {
                        mode: 'index'
                    },
                    scales: {
                        xAxes: [{
                                scaleLabel: {
                                    display: false,
                                    labelString: 'Month'
                                },
                                gridLines: {
                                    display: false
                                }
                            }],
                        yAxes: [{
                                scaleLabel: {
                                    display: false,
                                    labelString: 'Value'
                                },
                                ticks: {
                                    suggestedMax: 40,
                                    beginAtZero: true
                                },
                                gridLines: {
                                    display: false
                                }
                            }]
                    }
                }
            };

            var voiceChart = new Chart(ctx, config);

            var updateChart = function (datasets) {
                for (var i = 0; i < datasets.length; i++) {
                    datasets[i]['lineTension'] = 0;
                    if ('${voiceType}' === "positive") {
                        datasets[i]['backgroundColor'] = "rgba(75,192,192,0.2)";
                        datasets[i]['borderColor'] = "rgba(75,192,192,0.5)";
                        datasets[i]['pointBorderColor'] = "rgba(75,192,192,1)";
                        datasets[i]['pointHoverBackgroundColor'] = "rgba(75,192,192,1)";
                    } else if ('${voiceType}' === "negative") {
                        datasets[i]['backgroundColor'] = "rgba(243, 144, 141,0.2)";
                        datasets[i]['borderColor'] = "rgba(243, 144, 141,0.5)";
                        datasets[i]['pointBorderColor'] = "rgba(243, 144, 141,1)";
                        datasets[i]['pointHoverBackgroundColor'] = "rgba(243, 144, 141,1)";
                    } else if ('${voiceType}' === "suggestion") {
                        datasets[i]['backgroundColor'] = "rgba(109, 161, 201,0.2)";
                        datasets[i]['borderColor'] = "rgba(109, 161, 201,0.5)";
                        datasets[i]['pointBorderColor'] = "rgba(109, 161, 201,1)";
                        datasets[i]['pointHoverBackgroundColor'] = "rgba(109, 161, 201,1)";
                    } else {
                        datasets[i]['backgroundColor'] = "rgba(250, 208, 88,0.2)";
                        datasets[i]['borderColor'] = "rgba(250, 208, 88,0.5)";
                        datasets[i]['pointBorderColor'] = "rgba(250, 208, 88,1)";
                        datasets[i]['pointHoverBackgroundColor'] = "rgba(250, 208, 88,1)";
                    }
                    datasets[i]['borderCapStyle'] = 'butt';
                    datasets[i]['borderDash'] = [];
                    datasets[i]['borderDashOffset'] = 0.0;
                    datasets[i]['borderJoinStyle'] = 'miter';
                    datasets[i]['pointBackgroundColor'] = "#fff";
                    datasets[i]['pointBorderWidth'] = 1;
                    datasets[i]['pointHoverRadius'] = 5;
                    datasets[i]['pointHoverBorderColor'] = "rgba(220,220,220,1)";
                    datasets[i]['pointHoverBorderWidth'] = 2;
                    datasets[i]['pointRadius'] = 1;
                    datasets[i]['pointHitRadius'] = 10;
                }
                voiceChart.data.datasets = datasets;
                voiceChart.update();
            };

        });

        setInterval(function () {
            $.ajax({url: "/executive/csi_home", success: function (data) {
                }, dataType: "json"});
        }, 28 * 60 * 1000);

</script>
<div id="voices-people" class="fade modal container" tabindex="-1" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            ×
        </button>
        <h4 class="modal-title">รายชื่อลูกค้า</h4>
    </div>
    <div class="modal-body">
        <table class="table table-hover" id="">
            <thead>
                <tr>
                    <th class="center">ชื่อลูกค้า</th>
                    <th class="center">เบอร์โทร</th>
                    <th class="center">รุ่นรถ</th>
                    <th class="center">ครั้งที่ตรวจเช็ค</th>
                    <th class="center">วันที่ตรวจเช็ค</th>
                    <th class="center">ช่างที่เข้าไปบริการ</th>
                    <th class="center">ร้านตัวแทนจำหน่าย</th>
                    <th class="center">เสียงของลูกค้า</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-default">
            Close
        </button>
    </div>
</div>