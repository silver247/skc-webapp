<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@ page import="java.util.Calendar"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div class="row">
    <div class="col-md-2">
        <select id="s2month" name="month" class="form-control search-select">
            <option value="1" ${params['month'] == "1" ? 'selected' : ''}>Jan</option>
            <option value="2" ${params['month'] == "2" ? 'selected' : ''}>Feb</option>
            <option value="3" ${params['month'] == "3" ? 'selected' : ''}>Mar</option>
            <option value="4" ${params['month'] == "4" ? 'selected' : ''}>Apr</option>
            <option value="5" ${params['month'] == "5" ? 'selected' : ''}>May</option>
            <option value="6" ${params['month'] == "6" ? 'selected' : ''}>Jun</option>
            <option value="7" ${params['month'] == "7" ? 'selected' : ''}>Jul</option>
            <option value="8" ${params['month'] == "8" ? 'selected' : ''}>Aug</option>
            <option value="9" ${params['month'] == "9" ? 'selected' : ''}>Sep</option>
            <option value="10" ${params['month'] == "10" ? 'selected' : ''}>Oct</option>
            <option value="11" ${params['month'] == "11" ? 'selected' : ''}>Nov</option>
            <option value="12" ${params['month'] == "12" ? 'selected' : ''}>Dec</option>
        </select>
    </div>
    <div class="col-md-2">
        <select id="s2year" name="year" class="form-control search-select">
            <c:forEach var="year" items="${years}">
                <option value="${year}" ${year == params['year'] ? 'selected' : ''}>${year}</option>
            </c:forEach>
        </select>
    </div>
    <div class="col-md-2">
        <select id="s2company" name="year" class="form-control search-select" >
            <option value="0">All Companies</option>
            <c:forEach var="company" items="${companies}">
                <option value="${company.id}" ${company.id == params['companyId'] ? 'selected' : ''}>${company.name}</option>
            </c:forEach>
        </select>
    </div>
</div>

<br/>
<div class="row">
    <div class="col-md-12">
        <h3>${title}</h3>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <table class="table table-bordered">
            <thead style="background-color: #ccc;font-weight: bold;text-align: center;">
            <th></th>
                <c:forEach var="hd" items="${dateHeaders}">
                <th>${hd}</th>
                </c:forEach>
            </thead>
            <tbody>
                <c:forEach var="row" items="${rows}">
                    <tr style="${row == 'Total' ? 'background-color: #ccc;font-weight: bold;' : ''}">
                        <td><spring:message code="label.status.${row}"/></td>
                        <c:forEach var="h" items="${headers}">
                            <td><c:out value="${data.get(h).get(row)}"/></td>
                        </c:forEach>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
    </div>
</div>

<script>
    $(document).ready(function () {
        Main.init();
        FormValidator.init();
        var s2month = $("#s2month").select2();
        var s2year = $("#s2year").select2();
        var s2company = $("#s2company").select2();
        var visit = function (month, year, companyId) {
            location.href = '/report/status?month=' + month + '&year=' + year + '&companyId=' + companyId;
        };
        s2month.on("change", function (e) {
            console.log(e.val, s2year.val(), s2company.val());
            visit(e.val, s2year.val(), s2company.val());
        });
        s2year.on("change", function (e) {
            console.log(s2month.val(), e.val, s2company.val());
            visit(s2month.val(), e.val, s2company.val());
        });
        s2company.on("change", function (e) {
            console.log(s2month.val(), s2year.val(), e.val);
            visit(s2month.val(), s2year.val(), e.val);
        });
    });
</script>
