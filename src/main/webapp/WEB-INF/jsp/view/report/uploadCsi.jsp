<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div class="col-md-offset-3 col-md-6" style="margin-bottom: 1em;">
    <form id="upload-csi-form" method="post" action="/report/upload_csi" role="form" enctype="multipart/form-data">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label">Select XLSX Status File:</label>
                    <input type="file" class="form-control" name="file">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <button class="btn btn-yellow pull-right" type="submit" form="upload-csi-form"><i class="fa fa-arrow-circle-up"></i> Upload Csi</button>
            </div>
        </div>
    </form>
    <c:if test="${not empty errors}"><h2 style="color: red;">Errors</h2></c:if>
    <c:forEach items="${errors}" var="error">
        <div class="row">
            <div class="col-md-12">
                ${error}
            </div>
        </div>
    </c:forEach>
    <c:if test="${success}">
        <div class="row">
            <div class="col-md-12">
                <h2 style="color: darkgreen">File uploaded successfully. Added ${successCount}. ID not found ${failureCount}</h2>
            </div>
        </div>
    </c:if>
</div>

<script>
    $(document).ready(function () {
        Main.init();
        FormValidator.init();
    });
</script>
