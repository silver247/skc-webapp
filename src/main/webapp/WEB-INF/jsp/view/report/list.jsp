<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<div class="row">
    <sec:authorize access="hasRole('ROLE_USER')">
        <div class="col-md-10">
            <a href="/report/new" class="btn btn-primary">
                <i class="fa clip-plus-circle-2"></i>
                <spring:message code="label.addNew"/>
            </a>
        </div>
    </sec:authorize>
</div>
<br/>
<div class="row">
    <sec:authorize access="hasRole('ROLE_ADMIN') or hasRole('ROLE_EXECUTIVE')">
        <div class="col-md-5">
            <select id="company-filter" class="form-control search-select" multiple="multiple">
                <c:forEach items="${companies}" var="comp">
                    <option value="${comp.id}">${comp.getName()}</option>
                </c:forEach>
            </select>
        </div>
    </sec:authorize>
    <div class="col-md-2">
        <select name="dateType" id="dateType" class="form-control search-select">
            <option value="serviceDate"><spring:message code="label.dateTypeByCheckingDate"/></option>
            <option value="createdDate"><spring:message code="label.dateTypeByCreatedDate"/></option>
        </select>
    </div>
    <div class="col-md-2">
        <input id="daterange" placeholder="Pick a date" type="text" name="datefilter" class="form-control" value="" />
    </div>
    <div class="col-md-3" style="padding-left: 0px !important;">
        <a href="#" id="searchSubmit" class="btn btn-success" style="margin-right: 10px;">
            <spring:message code="label.search"/>
        </a>
        <a href="/report" class="btn btn-default">
            <spring:message code="label.clear"/>
        </a>
        <sec:authorize access="hasRole('ROLE_ADMIN')">
            <a href="/report/upload_status" class="btn btn-teal pull-right">Upload Status</a>
            <a href="/report/upload_csi" class="btn btn-primary pull-right" style="margin-right: 10px;">Upload CSI</a>
        </sec:authorize>
    </div>
</div>

<br/>

<div class="row">
    <div class="col-md-12 report">
        <table class="table table-striped table-bordered table-hover" id="sample_1">
            <thead>
                <tr>
                    <th>ID</th>
                    <th><spring:message code="label.dateOfInput"/></th>
                    <th><spring:message code="label.adId"/></th>
                    <th><spring:message code="label.adName"/></th>
                    <th><spring:message code="label.inputOrder"/></th>
                    <th><spring:message code="label.notificationNumber"/></th>
                    <th><spring:message code="label.productCategory"/></th>
                    <th><spring:message code="label.model"/></th>
                    <th><spring:message code="label.serialNumber"/></th>
                    <th><spring:message code="label.ownerName"/></th>
                    <th><spring:message code="label.customerName"/></th>
                    <th><spring:message code="label.customerPhoneNo"/>-1</th>
                    <th><spring:message code="label.customerPhoneNo"/>-2</th>
                    <th><spring:message code="label.customerPhoneNo"/>-3</th>
                    <th><spring:message code="label.checkingCount"/></th>
                    <th><spring:message code="label.checkingDate"/></th>
                    <th><spring:message code="label.mechanic"/></th>
                    <sec:authorize access="!hasRole('ROLE_ADMIN')"><th><spring:message code="label.status"/></th></sec:authorize>
                    <sec:authorize access="hasRole('ROLE_ADMIN')"><th>Status</th></sec:authorize>
                        <th>Date of calling</th>
                        <th>Note</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                <c:forEach items="${reports}" var="report">
                    <tr>
                        <td>${report.id}</td>
                        <td><fmt:formatDate value="${report.serviceDate}" pattern="YYYY-MM-dd" /></td>
                        <td>${report.company.displayId}</td>
                        <td>${report.company.name}</td>
                        <td>${report.orderNo}</td>
                        <td>${report.informedCode}</td>
                        <td>
                            <c:if test="${report.productCategory == 0}">
                                <spring:message code="label.tractor"/>
                            </c:if>
                            <c:if test="${report.productCategory == 1}">
                                <spring:message code="label.harvester"/>
                            </c:if>
                            <c:if test="${report.productCategory == 2}">
                                <spring:message code="label.booties"/>
                            </c:if>
                        </td>
                        <td>${report.vehicleModelName}</td>
                        <td>${report.vehicleBodyId}</td>
                        <td>${report.name}</td>
                        <td>${report.customerName}</td>
                        <td>${report.customerPhoneNo}</td>
                        <td>${report.customerPhoneNo2}</td>
                        <td>${report.customerPhoneNo3}</td>
                        <td>${report.maintenanceNumber}</td>
                        <td><fmt:formatDate value="${report.maintenanceDate}" pattern="YYYY-MM-dd" /></td>
                        <td>${report.mechanic.name}</td>
                        <td class="text-center">
                            <a href="#" class="report-status" data-type="select" data-pk="${report.id}" data-value="${report.status}" data-url="/report/update_status" data-title="Select status"></a>
                        </td>
                        <td><fmt:formatDate value="${report.callingDate}" pattern="YYYY-MM-dd" /></td>
                        <td>${report.note}</td>
                        <td>
                            <a href="#report" data-toggle="modal" data-id="${report.id}" class="btn btn-xs btn-warning reportEdit" >Edit</a>
                            <sec:authorize access="hasRole('ROLE_ADMIN') or hasRole('ROLE_EXECUTIVE')">
                                <a href="#removereportmodal" data-toggle="modal" data-id="${report.id}" class="btn btn-xs btn-danger reportRemove" >Delete</a>
                            </sec:authorize>
                        </td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
    </div>
</div>


<!--update report modal-->
<div id="report" class="modal fade" tabindex="-1" data-width="760" data-replace="true" style="display: none;">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            &times;
        </button>
        <h4 class="modal-title">Edit report</h4>
    </div>
    <div class="modal-body">
        <form method="post"  role="form" id="form">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label">
                            <spring:message code="label.dateOfInput"/><span class="symbol required"></span>
                        </label>
                        <input type="date" class="form-control" id="serviceDate" disabled>
                    </div>
                    <div class="form-group">
                        <label class="control-label">
                            <spring:message code="label.adId"/>
                        </label>
                        <input type="text" class="form-control"  id="shopId" disabled>
                    </div>
                    <div class="form-group">
                        <label class="control-label">
                            <spring:message code="label.adName"/>
                        </label>
                        <input type="text" class="form-control" id="shopName" disabled>
                    </div>
                    <div class="form-group">
                        <label class="control-label">
                            <spring:message code="label.inputOrder"/><span class="symbol required"></span>
                        </label>
                        <input type="number" class="form-control" id="orderNo" disabled>
                    </div>
                    <div class="form-group">
                        <label class="control-label">
                            <spring:message code="label.notificationNumber"/><span class="symbol required"></span>
                        </label>
                        <input type="text" class="form-control" name="informedCode" id="informedCode" required>
                    </div>
                    <div class="form-group">
                        <label class="control-label">
                            <spring:message code="label.productCategory"/><span class="symbol required"></span>
                        </label>
                        <select name="productCategory" id="productCategory" class="form-control search-select">
                            <option value="0"><spring:message code="label.tractor"/></option>
                            <option value="1"><spring:message code="label.harvester"/></option>
                            <option value="2"><spring:message code="label.booties"/></option>
                            <option value="3"><spring:message code="label.excavator"/></option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="control-label">
                            <spring:message code="label.model"/><span class="symbol required"></span>
                        </label>
                        <input type="text" class="form-control" name="vehicleModelName" id="vehicleModelName" required>
                    </div>
                    <div class="form-group">
                        <label class="control-label">
                            <spring:message code="label.serialNumber"/><span class="symbol required"></span>
                        </label>
                        <input type="text" class="form-control" name="vehicleBodyId" id="vehicleBodyId" required>
                    </div>
                    <div class="form-group">
                        <label class="control-label">
                            <spring:message code="label.ownerName"/><span class="symbol required"></span>
                        </label>
                        <input type="text" class="form-control" name="name" id="name" required>
                    </div>
                    <div class="form-group">
                        <label class="control-label">
                            <spring:message code="label.customerName"/><span class="symbol required"></span>
                        </label>
                        <input type="text" class="form-control" name="customerName" id="customerName" required>
                    </div>
                    <div class="form-group">
                        <label class="control-label">
                            <spring:message code="label.customerPhoneNo"/>-1<span class="symbol required"></span>
                        </label>
                        <input type="number" class="form-control" name="customerPhoneNo" id="customerPhoneNo" required>
                    </div>
                    <div class="form-group">
                        <label class="control-label">
                            <spring:message code="label.customerPhoneNo"/>-2<span class="symbol"></span>
                        </label>
                        <input type="number" class="form-control" name="customerPhoneNo2" id="customerPhoneNo2">
                    </div>
                    <div class="form-group">
                        <label class="control-label">
                            <spring:message code="label.customerPhoneNo"/>-3<span class="symbol"></span>
                        </label>
                        <input type="number" class="form-control" name="customerPhoneNo3" id="customerPhoneNo3">
                    </div>
                    <div class="form-group">
                        <%-- <label class="control-label">
                            <spring:message code="label.checkingCount"/><span class="symbol required"></span>
                        </label>
                        <input type="number" class="form-control" name="maintenanceNumber" id="maintenanceNumber" required> --%>
                        <label class="control-label">
                            <spring:message code="label.checkingCount"/><span class="symbol required"></span>
                        </label>
                        <select name="maintenanceNumber" id="maintenanceNumber" class="form-control search-select">
                            <option value="1"><spring:message code="message.maintenanceNumber" arguments="1"/></option>
                            <option value="2"><spring:message code="message.maintenanceNumber" arguments="2"/></option>
                            <option value="3"><spring:message code="message.maintenanceNumber" arguments="3"/></option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="control-label">
                            <spring:message code="label.checkingDate"/><span class="symbol required"></span>
                        </label>
                        <div class="input-group">
                            <input id="maintenanceDate" name="maintenanceDate" type="text" data-date-format="yyyy-mm-dd" data-date-viewmode="years" class="form-control date-picker" required>
                            <span class="input-group-addon"> <i class="fa fa-calendar"></i> </span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">
                            <spring:message code="label.mechanic"/><span class="symbol required"></span>
                        </label>
                        <input type="text" class="form-control" id="mechanic" value="" disabled>
                        <%-- <select name="mechanicId" id="mechanic-select2" class="form-control search-select">
                            <c:forEach items="${mechanics}" var="mechanic">
                                <option value="${mechanic.getId()}">${mechanic.getName()}</option>
                            </c:forEach>
                            <option value="0"><spring:message code="label.other"/></option>
                        </select>
                        <input type="hidden" id="mechanicName" name="mechanicName" class="form-control"	style="margin-top: 1em;" placeholder="New mechanic" value=""> --%>
                    </div>
                    <input type="hidden" id="reportid"/>
                    <input type="hidden" id="csrf" name="${_csrf.parameterName}" 	value="${_csrf.token}" />
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div>
                        <span class="symbol required"></span><spring:message code="label.requiredFields"/>
                        <hr>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="modal-footer">
        <button type="button" id="closemodal" data-dismiss="modal" class="btn btn-light-grey">
            <spring:message code="label.close"/>
        </button>
        <button type="button" id="updatereport"class="btn btn-warning">
            <spring:message code="label.update"/>
        </button>
    </div>

</div>

<div id="removereportmodal" class="modal fade" tabindex="-1" data-width="760" data-replace="true" style="display: none;">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            &times;
        </button>
        <h4 class="modal-title">Are you sure?</h4>
    </div>
    <div class="modal-footer">
        <input type="hidden" id="removereportid"/>
        <button type="button" id="closemodal" data-dismiss="modal" class="btn btn-light-grey">
            <spring:message code="label.close"/>
        </button>
        <button type="button" id="removereport"class="btn btn-danger">
            <spring:message code="label.confirm"/>
        </button>
    </div>

</div>

<script id="status-json" type="application/json">
    [
    {"value": "completed", "text": "<spring:message code="label.status.completed"/>", "class": "label-success"},
    {"value": "cannot_give_information", "text": "<spring:message code="label.status.cannot_give_information"/>", "class": "label-warning"},
    {"value": "rejected", "text": "<spring:message code="label.status.rejected"/>", "class": "label-warning"},
    {"value": "not_yet_interviewed", "text": "<spring:message code="label.status.not_yet_interviewed"/>", "class": "label-primary"},
    {"value": "not_picked_up", "text": "<spring:message code="label.status.not_picked_up"/>", "class": "label-warning"},
    {"value": "wait_for_calling_back", "text": "<spring:message code="label.status.wait_for_calling_back"/>", "class": "label-warning"},
    {"value": "turn_off", "text": "<spring:message code="label.status.turn_off"/>", "class": "label-warning"},
    {"value": "wrong_number", "text": "<spring:message code="label.status.wrong_number"/>", "class": "label-warning"},
    {"value": "cannot_connect", "text": "<spring:message code="label.status.cannot_connect"/>", "class": "label-warning"},
    {"value": "terminate_interviews", "text": "<spring:message code="label.status.terminate_interviews"/>", "class": "label-warning"},
    {"value": "not_customer_telephone_number", "text": "<spring:message code="label.status.not_customer_telephone_number"/>", "class": "label-warning"},
    {"value": "wait_for_investigation", "text": "<spring:message code="label.status.wait_for_investigation"/>", "class": "label-warning"},
    {"value": "already_exist", "text": "<spring:message code="label.status.already_exist"/>", "class": "label-warning"}
    ]
</script>

<script>
    $(document).ready(function () {
        Main.init();
        FormValidator.init();
        var statusJson = JSON.parse($("script#status-json").text());
        var isKnownStatus = function (status) {
            for (var i = 0; i < statusJson.length; i++) {
                if (statusJson[i]['value'] === status) {
                    return true;
                }
            }
            return false;
        };
        $('.report-status').editable({
            placement: 'bottom',
            showbuttons: false,
            params: function (params) {
                params["_csrf"] = $("#csrf").val();
                return params;
            },
            display: function (value, sourceData) {
                var $self = $(this);
                var xval = value;
                if (!isKnownStatus(value)) {
                    if (value === 'incorrect_mobile') {
                        xval = "wrong_number";
                    } else {
                        xval = "not_yet_interviewed";
                    }
                }

                for (var i = 0; i < statusJson.length; i++) {
                    var sj = statusJson[i];
                    if (sj['value'] === xval) {
                        var $span = $('<span>', {class: "label"}).html(sj['text']);
                        $span.addClass(sj['class']);
                        $self.html($span);
                    }
                }

            },
            source: statusJson
        });

        $("#company-filter").select2({
            placeholder: "Select a company",
            allowClear: true
        });

        $("#maintenanceNumber").select2();
        $("#productCategory").select2();
        $("#mechanic-select2").select2();
        $("#dateType").select2();
        var canDownload = ${showDownload};
        var btn = function () {
            if (canDownload)
                return ['copy', 'csv', 'excel', 'print'];
            return [];
        };
        var _bc = function (data, type, row) {
            return '';
        };

        var oTable = $('#sample_1').dataTable({
            dom: '<"top"lBf>rt<"bottom"ip><"clear">',
            scrollX: true,
            autoWidth: true,
            buttons: btn(),
            deferRender: true,
            columnDefs: [
                {
                    "render": function (data, type, row) {
                        switch (type) {
                            case 'sort':
                                return data;
                            default:
                                return !data ? data : moment(data, 'YYYY-MM-DD').format('DD/MM/YYYY');
                        }
                    },
                    "targets": [1, 15, 18]
                }
            ],
            language: {
                lengthMenu: "Show _MENU_",
                search: "",
                paginate: {
                    "sPrevious": "",
                    "sNext": ""
                }
            },
            sorting: [
                [1, 'desc']
            ],
            lengthMenu: [
                [5, 10, 15, 20, -1],
                [5, 10, 15, 20, "All"] // change per page values here
            ],
            // set the initial value
            pageLength: 10
        });

        $("#searchSubmit").click(function () {
            var companyids = $("#company-filter").val() || [];
            var params = "?";
            params = params + "companiesId=" + companyids;
            if ($('#daterange').val() !== "") {
                var startDate = $('#daterange').data('daterangepicker').startDate.format('YYYY-MM-DD');
                var endDate = $('#daterange').data('daterangepicker').endDate.format('YYYY-MM-DD');
                params = params + "&startDate=" + startDate;
                params = params + "&endDate=" + endDate;
            }
            if ($('#dateType').val() !== "") {
                params = params + "&dateType=" + $('#dateType').val();
            }

            window.location.href = '/report' + params;

        });

        $('input[name="datefilter"]').daterangepicker({
            autoUpdateInput: false,
            locale: {
                cancelLabel: 'Clear'
            },
            alwaysShowCalendars: true,
            startDate: moment().subtract(1, 'month'),
            endDate: moment(),
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            "linkedCalendars": false
        }, cb);

        $('input[name="datefilter"]').on('apply.daterangepicker', function (ev, picker) {
            $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
        });

        $('input[name="datefilter"]').on('cancel.daterangepicker', function (ev, picker) {
            $(this).val('');
        });

        function cb(start, end) {
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }
        cb(moment().subtract(29, 'days'), moment());

        $("#removereport").click(function () {
            $.post("/report/" + $("#removereportid").val() + "/delete", {"_csrf": $("#csrf").val()})
            .done(function (data) {
                window.location.href = '/report';
            });
        });

        $(".reportRemove").click(function () {
            var $self = $(this);
            var id = $self.data('id');
            $("#removereportid").val(id);
        });

        $(".reportEdit").click(function () {
            var $self = $(this);
            var id = $self.data('id');

            $.ajax({
                url: '/report/order/' + id,
                success: function (data) {
                    if (!data.success) {
                        return;
                    }
                    var o = data.order;
                    $("#reportid").val(id);
                    $('#serviceDate').val(moment(o.serviceDate).format("YYYY-MM-DD"));
                    $('#shopId').val(o.company.id);
                    $('#shopName').val(o.company.name);
                    $('#orderNo').val(o.orderNo);
                    $('#informedCode').val(o.informedCode);
                    $('#vehicleModelName').val(o.vehicleModelName);
                    $('#vehicleBodyId').val(o.vehicleBodyId);
                    $('#name').val(o.name);
                    $('#customerName').val(o.customerName);
                    $('#customerPhoneNo').val(o.customerPhoneNo);
                    $('#customerPhoneNo2').val(o.customerPhoneNo2);
                    $('#customerPhoneNo3').val(o.customerPhoneNo3);
                    $("#maintenanceNumber").val(o.maintenanceNumber).change();
                    $("#productCategory").val(o.productCategory).change();
                    $('#maintenanceDate').val(moment(o.maintenanceDate).format("YYYY-MM-DD"));
                    $("#mechanic").val(o.mechanic.name);
                    $('.date-picker').datepicker({
                        autoclose: true
                    });
                },
                dataType: "json"
            });
        });
    });

    $("#updatereport").click(function () {
        if ($('#form').valid()) {
            var params = {};
            params['_csrf'] = $("#csrf").val();
            params.serviceDate = $("#serviceDate").val();
            params.shopId = $("#shopId").val();
            params.shopName = $("#shopName").val();
            params.informedCode = $("#informedCode").val();
            params.productCategory = $("#productCategory").val();
            params.vehicleModelName = $("#vehicleModelName").val();
            params.vehicleBodyId = $("#vehicleBodyId").val();
            params.customerName = $("#customerName").val();
            params.customerPhoneNo = $("#customerPhoneNo").val();
            params.customerPhoneNo2 = $("#customerPhoneNo2").val();
            params.customerPhoneNo3 = $("#customerPhoneNo3").val();
            params.maintenanceNumber = $("#maintenanceNumber").val();
            params.maintenanceDate = $("#maintenanceDate").val();
            params.name = $("#name").val();
            $.post("/report/" + $("#reportid").val() + "/edit", params)
            .done(function (data) {
                window.location.href = '/report';
            });
        }
    });
</script>
