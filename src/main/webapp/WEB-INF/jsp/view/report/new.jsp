<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div class="col-md-offset-3 col-md-6" style="margin-bottom: 1em;">
    <form method="post" action="/report/create" role="form" id="form">
        <div class="col-md-12">
            <div class="errorHandler alert alert-danger no-display">
                <i class="fa fa-times-sign"></i> You have some form errors. Please check below.
            </div>
            <div class="successHandler alert alert-success no-display">
                <i class="fa fa-ok"></i> Report is successfully created!!
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label">
                        <spring:message code="label.dateOfInput"/><span class="symbol required"></span>
                    </label>
                    <input type="date" class="form-control" name="serviceDate" id="serviceDate" required disabled>
                </div>
                <div class="form-group">
                    <label class="control-label">
                        <spring:message code="label.adId"/>
                    </label>
                    <input type="text" class="form-control" id="shopId" value="${company.displayId}" disabled>
                </div>
                <div class="form-group">
                    <label class="control-label">
                        <spring:message code="label.adName"/>
                    </label>
                    <input type="text" class="form-control"  id="shopName" value="${company.name}" disabled>
                </div>
                <div class="form-group">
                    <label class="control-label">
                        <spring:message code="label.notificationNumber"/><span class="symbol required"></span>
                    </label>
                    <input type="text" class="form-control" name="informedCode" id="informedCode" required autofocus>
                </div>
                <div class="form-group">
                    <label class="control-label">
                        <spring:message code="label.productCategory"/><span class="symbol required"></span>
                    </label>
                    <select name="productCategory" id="productCategory" class="form-control search-select">
                        <option value="0"><spring:message code="label.tractor"/></option>
                        <option value="1"><spring:message code="label.harvester"/></option>
                        <option value="2"><spring:message code="label.booties"/></option>
                        <option value="3"><spring:message code="label.excavator"/></option>
                    </select>
                </div>
                <div class="form-group">
                    <label class="control-label">
                        <spring:message code="label.model"/><span class="symbol required"></span>
                    </label>
                    <input type="text" class="form-control" name="vehicleModelName" id="vehicleModelName" required>
                </div>
                <div class="form-group">
                    <label class="control-label">
                        <spring:message code="label.serialNumber"/><span class="symbol required"></span>
                    </label>
                    <input type="text" class="form-control" name="vehicleBodyId" id="vehicleBodyId" required>
                </div>
                <div class="form-group">
                    <label class="control-label">
                        <spring:message code="label.ownerName"/><span class="symbol required"></span>
                    </label>
                    <input type="text" class="form-control" name="name" id="name" required>
                </div>
                <div class="form-group">
                    <label class="control-label">
                        <spring:message code="label.customerName"/><span class="symbol required"></span>
                    </label>
                    <input type="text" class="form-control" name="customerName" id="customerName" required>
                </div>
                <div class="form-group">
                    <label class="control-label">
                        <spring:message code="label.customerPhoneNo"/>-1<span class="symbol required"></span>
                    </label>
                    <input type="number" class="form-control" name="customerPhoneNo" id="customerPhoneNo" required>
                </div>
                <div class="form-group">
                    <label class="control-label">
                        <spring:message code="label.customerPhoneNo"/>-2<span class="symbol"></span>
                    </label>
                    <input type="number" class="form-control" name="customerPhoneNo2" id="customerPhoneNo2">
                </div>
                <div class="form-group">
                    <label class="control-label">
                        <spring:message code="label.customerPhoneNo"/>-3<span class="symbol"></span>
                    </label>
                    <input type="number" class="form-control" name="customerPhoneNo3" id="customerPhoneNo3">
                </div>
                <div class="form-group">
                    <%-- <label class="control-label">
                        <spring:message code="label.checkingCount"/><span class="symbol required"></span>
                    </label>
                    <input type="number" class="form-control" name="maintenanceNumber" id="maintenanceNumber" required> --%>
                    <label class="control-label">
                        <spring:message code="label.checkingCount"/><span class="symbol required"></span>
                    </label>
                    <select name="maintenanceNumber" id="maintenanceNumber" class="form-control search-select">
                        <option value="1"><spring:message code="message.maintenanceNumber" arguments="1"/></option>
                        <option value="2"><spring:message code="message.maintenanceNumber" arguments="2"/></option>
                        <option value="3"><spring:message code="message.maintenanceNumber" arguments="3"/></option>
                    </select>
                </div>
                <div class="form-group">
                    <label class="control-label">
                        <spring:message code="label.checkingDate"/><span class="symbol required"></span>
                    </label>
                    <div class="input-group">
                        <input id="maintenanceDate" name="maintenanceDate" type="text" data-date-format="yyyy-mm-dd" data-date-viewmode="years" class="form-control date-picker" required>
                        <span class="input-group-addon"> <i class="fa fa-calendar"></i> </span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label">
                        <spring:message code="label.mechanic"/><span class="symbol required"></span>
                    </label>
                    <select name="mechanicId" id="mechanic-select2" class="form-control search-select">
                        <c:forEach items="${mechanics}" var="mechanic">
                            <option value="${mechanic.getId()}">${mechanic.getName()}</option>
                        </c:forEach>
                        <option value="0"><spring:message code="label.other"/></option>
                    </select>
                    <input type="hidden" id="mechanicName" name="mechanicName" class="form-control"	style="margin-top: 1em;" placeholder="New mechanic" value="">
                </div>
                <input type="hidden" id="csrf" name="${_csrf.parameterName}" 	value="${_csrf.token}" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div>
                    <span class="symbol required"></span><spring:message code="label.requiredFields"/>
                    <hr>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <button id="createreport" class="btn btn-yellow btn-block" type="submit">
                    <spring:message code="label.createOrder"/> <i class="fa fa-arrow-circle-right"></i>
                </button>
            </div>
        </div>
    </form>
</div>

<script>
    $(document).ready(function () {
        Main.init();
        FormValidator.init();

        $("#serviceDate").val(moment().format("YYYY-MM-DD"));
        $("#documentCode").focus();
        $("#mechanic-select2").select2();
        $("#maintenanceNumber").select2();
        $("#productCategory").select2();
        $("#createreport").click(function () {
            if ($('#form').valid()) {
                var params = {};
                params['_csrf'] = $("#csrf").val();
                params.documentCode = $("#documentCode").val();
                params.informedCode = $("#informedCode").val();
                params.productCategory = $("#productCategory").val();
                params.vehicleModelName = $("#vehicleModelName").val();
                params.vehicleBodyId = $("#vehicleBodyId").val();
                params.customerName = $("#customerName").val();
                params.customerPhoneNo = $("#customerPhoneNo").val();
                params.customerPhoneNo2 = $("#customerPhoneNo2").val();
                params.customerPhoneNo3 = $("#customerPhoneNo3").val();
                params.maintenanceNumber = $("#maintenanceNumber").val();
                params.maintenanceDate = $("#maintenanceDate").val();
                params.serviceDate = $("#serviceDate").val();
                params.name = $("#name").val();
                var mechanicVal = $("#mechanic-select2").val();
                if (mechanicVal == 0) {
                    // new mechanic
                    params.mechanicName = $("#mechanicName").val();
                } else {
                    // existed mechanic
                    params.mechanicId = $("#mechanic-select2").val();
                }

                console.log(params);
                $.post("/report/create", params)
                .done(function (data) {
                    window.location.href = '/report';
                });
            }
        });

        $("#mechanic-select2").change(function () {
            var $self = $(this);
            if ($self.val() == 0) {
                $("#mechanicName").prop("type", "text");
                $("#mechanicName").prop('required', true);
            } else {
                $("#mechanicName").prop("type", "hidden");
                $("#mechanicName").prop('required', false);
            }
        });

        if ($("#mechanic-select2").val() == 0) {
            $("#mechanicName").prop("type", "text");
            $("#mechanicName").prop('required', true);
        } else {
            $("#mechanicName").prop("type", "hidden");
            $("#mechanicName").prop('required', false);
        }
        $('.date-picker').datepicker({
            autoclose: true
        });
    });
</script>
