<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="main-login col-sm-4 col-sm-offset-4">
    <div class="logo">SKC Input System
    </div>
    <!-- start: LOGIN BOX -->
    <div class="box-login" style="display: inherit;">
        <h3>Sign in to your account</h3>
        <p>
            Please enter your name and password to log in.
        </p>
        <c:url var="loginUrl" value="/login" />
        <form class="form-login" method="post" action="${loginUrl}">
            <c:if test="${param.error != null}">
                <div class="alert alert-danger">
                    <i class="fa fa-remove-sign"></i> You have some form errors. Please check below. (${param.error})
                </div>
            </c:if>
            <c:if test="${param.logout != null}">
                <div class="alert alert-success">
                    <p>You have been logged out successfully.</p>
                </div>
            </c:if>
            <fieldset>
                <div class="form-group">
                    <span class="input-icon">
                        <input type="text" class="form-control" name="username" placeholder="Username" required>
                        <i class="fa fa-user"></i> </span>
                </div>
                <div class="form-group form-actions">
                    <span class="input-icon">
                        <input type="password" class="form-control password" name="password" placeholder="Password" required>
                        <i class="fa fa-lock"></i>
                </div>
                <input type="hidden" name="${_csrf.parameterName}" 	value="${_csrf.token}" />
                <div class="form-actions">
                    <label for="remember-me" class="checkbox-inline">
                        <input type="checkbox" class="grey remember" id="remember-me" name="remember-me">
                        Keep me signed in
                    </label>
                    <button type="submit" class="btn btn-bricky pull-right">
                        Log in <i class="fa fa-arrow-circle-right"></i>
                    </button>
                </div>
            </fieldset>
        </form>
    </div>
    <!-- end: LOGIN BOX -->
</div>
