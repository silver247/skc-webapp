<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <!--table class="table table-striped table-bordered table-hover table-full-width" id="item-list">
            <thead>
                <tr>
                    <th></th>
                    <th style="width: 10%">Action</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table-->
        <p><sec:authorize access="hasRole('ROLE_PREVIOUS_ADMINISTRATOR')">has ROLE_PREVIOUS_ADMINISTRATOR</sec:authorize></p>
        <p><sec:authorize access="hasRole('ROLE_ADMIN')">has ROLE_ADMIN</sec:authorize></p>
        <p><sec:authorize access="hasRole('ROLE_USER')">has ROLE_USER</sec:authorize></p>
    </div>
</div>

<script>
    $(document).ready(function () {
        Main.init();
    });
</script>