<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<!--<p><spring:message code="label.firstname"/> <--- This is an i18n example.</p>-->

<sec:authentication var="user" property="principal" />
<!--<p><spring:message code="message.greeting" arguments="${user.username}"/> <--- This is parameterized message.</p>-->

<div class="row">
    <div class="col-md-8 col-md-offset-2 col-sm-12">
        <div class="row">
            <div class="col-sm-4">
                <button onclick="location.href = '/report';" class="btn btn-icon btn-block">
                    <i class="clip-stack-empty"></i>
                    <p><spring:message code="label.report"/>
                </button>
            </div>
            <sec:authorize access="hasRole('ROLE_ADMIN')">
                <div class="col-sm-4">
                    <button onclick="location.href = '/company';" class="btn btn-icon btn-block">
                        <i class="clip-users"></i>
                        <p><spring:message code="label.company"/>
                    </button>
                </div>
                <div class="col-sm-4">
                    <button onclick="location.href = '/user';" class="btn btn-icon btn-block">
                        <i class="clip-user"></i>
                        <p><spring:message code="label.user"/>
                    </button>
                </div>
            </sec:authorize>
        </div>
    </div>
</div>
                
       