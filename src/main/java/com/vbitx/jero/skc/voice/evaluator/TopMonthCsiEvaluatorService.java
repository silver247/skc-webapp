/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vbitx.jero.skc.voice.evaluator;

import com.vbitx.jero.api.company.Company;
import com.vbitx.jero.api.company.CompanyRepository;
import com.vbitx.jero.skc.voice.CustomerVoice;
import com.vbitx.jero.skc.voice.CustomerVoiceRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import static org.springframework.data.jpa.domain.Specifications.*;
import static com.vbitx.jero.skc.voice.CustomerVoiceSpecs.*;
import com.vbitx.jero.skc.voice.MessyCustomerVoiceIndexes;
import static com.vbitx.jero.skc.voice.MessyCustomerVoiceIndexes.IndexTitleMap;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.transaction.annotation.Transactional;
import java.util.Optional;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author gigadot
 */
@Service
public class TopMonthCsiEvaluatorService {

    @Autowired
    private CustomerVoiceRepository customerVoiceRepository;

    @Autowired
    private CompanyRepository companyRepository;

    private final String MAGIC_SPLITOR = "-####-";

    private Integer getVoiceValue(CustomerVoice customerVoice, String voiceType) {
        switch (voiceType) {
            case "positive":
                return customerVoice.getN50();
            case "negative":
                return customerVoice.getN51();
            case "complaint":
                return customerVoice.getN31r();
            case "suggestion":
                return customerVoice.getN49();
            default:
                return 0;
        }
    }

    /**
     *
     * @param year
     * @param companyIds
     * @param chkNumbers
     * @param mechanicNames
     * @param voices
     * @param voiceType positive, negative, complaint, suggestion
     * @return
     */
    @Transactional(readOnly = true)
    public Map processTopMonthCsi(
        Integer year,
        List<String> companyIds,
        List<Integer> chkNumbers,
        List<String> mechanicNames,
        List<Integer> voices,
        String voiceType) {

        Map<Integer, Integer> monthlyTotals = new HashMap<>();
        Map<Integer, List<CustomerVoice>> monthlyVoices = new HashMap<>();

        // map f month to (map of remark to voice)
        List<Map<Integer, List<CustomerVoice>>> monthlyGroupedVoices = new ArrayList<>();

        for (int i = 0; i < 12; i++) {
            Specifications<CustomerVoice> spec = where(targetYearMonth(year, i))
                .and(hasMechanics(mechanicNames))
                .and(hasChkNumbers(chkNumbers))
                .and(hasCompanies(companyIds))
                .and(
                    (voices != null && !voices.isEmpty())
                    ? hasVoices(voices, voiceType)
                    : voiceNotNull(voiceType)
                );
            List<CustomerVoice> cvs = customerVoiceRepository.findAll(spec);
            monthlyVoices.put(i, cvs);
            monthlyTotals.put(i, cvs.size());

            try {
                Map<Integer, List<CustomerVoice>> mcv = cvs.stream().collect(
                    Collectors.groupingBy((t) -> {
                        return getVoiceValue(t, voiceType);
                    }, Collectors.toList())
                );
                Set<Integer> remarks = IndexTitleMap.get(voiceType).keySet();
                remarks.forEach((remark) -> {
                    mcv.putIfAbsent(remark, new ArrayList<>());
                });
                monthlyGroupedVoices.add(mcv);
            } catch (Exception ex) {
                monthlyGroupedVoices.add(new HashMap<>());
            }
        }

        Map<String, Object> resmap = new HashMap<>();

        int maxMonthIndex = -1;
        // find top-mentioned month
        try {
            Optional<Map.Entry<Integer, Integer>> max = monthlyTotals.entrySet().stream().max(
                (e1, e2) -> e1.getValue().compareTo(e2.getValue())
            );
            if (max.isPresent()) {
                Map.Entry<Integer, Integer> maxMonthEntrySet = max.get();
                maxMonthIndex = (maxMonthEntrySet != null) ? maxMonthEntrySet.getKey() : -1;
                int maxMonthCount = (maxMonthEntrySet != null) ? maxMonthEntrySet.getValue() : -1;
                resmap.put("topMentionedMonth", ct(maxMonthCount, MessyCustomerVoiceIndexes.getMonthName(maxMonthIndex)));
            } else {
                resmap.put("topMentionedMonth", ct(0, ""));
            }
        } catch (Exception ex) {
            resmap.put("topMentionedMonth", ct(0, ""));
        }

        {
            List<Map<String, Object>> responseVoices = new ArrayList<>();
            Map<Integer, Object> responseDatasets = new HashMap<>();
            Map<Integer, String> remarks = new HashMap<>(IndexTitleMap.get(voiceType));
            if (voices != null && !voices.isEmpty()) {
                for (Iterator<Map.Entry<Integer, String>> it = remarks.entrySet().iterator(); it.hasNext();) {
                    Map.Entry<Integer, String> entry = it.next();
                    if (!voices.contains(entry.getKey())) {
                        it.remove();
                    }
                }
            }
            int[] annualTotalRemarks = new int[13];
            int overallTotal = 0;
            for (Map.Entry<Integer, String> entry : remarks.entrySet()) {
                if (monthlyGroupedVoices.size() != 12) {
                    continue;
                }
                Integer remark = entry.getKey();
                String title = entry.getValue();
                List<Integer> voiceCountList = new ArrayList<>();
                int xCount = 0;
                int[] monthlyRemarks = new int[12];
                for (int i = 0; i < monthlyGroupedVoices.size(); i++) {
                    Map<Integer, List<CustomerVoice>> x = monthlyGroupedVoices.get(i);
                    List<CustomerVoice> lcv = x.get(remark);
                    voiceCountList.add(lcv.size());
                    xCount += lcv.size();
                    annualTotalRemarks[i] += lcv.size();
                    overallTotal += lcv.size();
                    //if (i == maxMonthIndex) {
                    monthlyRemarks[i] += lcv.size();
                    //}
                }
                voiceCountList.add(xCount);
                responseVoices.add(MapUtils.putAll(new HashMap<>(), new Object[]{
                    "class", "",
                    "title", title,
                    "voiceValue", remark,
                    "values", voiceCountList
                }));
                responseDatasets.put(remark, monthlyRemarks);
            }
            annualTotalRemarks[12] = overallTotal;
            responseVoices.add(MapUtils.putAll(new HashMap<>(), new Object[]{
                "class", "total",
                "title", "รวม",
                "voiceValue", 0,
                "values", annualTotalRemarks
            }));
            resmap.put("voices", responseVoices);
            resmap.put("datasets", responseDatasets.entrySet().stream().sorted(Map.Entry.comparingByKey()).map((t) -> {
                return MapUtils.putAll(new HashMap(), new Object[]{
                    "data", t.getValue(),
                    "label", remarks.get(t.getKey())
                });
            }).collect(Collectors.toList()));
        }

        if (maxMonthIndex >= 0) {
            List<CustomerVoice> voicesOfTopMentionedMonth = monthlyVoices.get(maxMonthIndex);

            // find top-mentioned remark of the top-mentioned month
            try {
                Optional<Map.Entry<Integer, Long>> max = voicesOfTopMentionedMonth.stream().collect(
                    Collectors.groupingBy((t) -> {
                        return getVoiceValue(t, voiceType);
                    }, Collectors.counting())
                ).entrySet().stream().max((e1, e2) -> e1.getValue().compareTo(e2.getValue()));
                if (max.isPresent()) {
                    Map.Entry<Integer, Long> topRemarkCount = max.get();
                    Integer count = (int) (long) topRemarkCount.getValue();
                    Integer remark = topRemarkCount.getKey();
                    String title = IndexTitleMap.get(voiceType).get(remark);
                    resmap.put("topMentionedRemark", ct(count, title));
                } else {
                    resmap.put("topMentionedRemark", ct(0, ""));
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                resmap.put("topMentionedRemark", ct(0, ""));
            }

            // find top-mentioned shop of the top-mentioned month
            try {
                Optional<Map.Entry<String, Long>> max = voicesOfTopMentionedMonth.stream().filter((t) -> {
                    return !StringUtils.isBlank(t.getCompanyId());
                }).collect(
                    Collectors.groupingBy(CustomerVoice::getCompanyId, Collectors.counting())
                ).entrySet().stream().max((e1, e2) -> {
                    return e1.getValue().compareTo(e2.getValue());
                });
                if (max.isPresent()) {
                    Map.Entry<String, Long> topShopNameCount = max.get();
                    Integer count = (int) (long) topShopNameCount.getValue();
                    String companyId = topShopNameCount.getKey();
                    Company company = companyRepository.findOne(companyId);
                    resmap.put("topMentionedShop", ct(count, company.getName()));
                } else {
                    resmap.put("topMentionedShop", ct(0, ""));
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                resmap.put("topMentionedShop", ct(0, ""));
            }

            // find top-mentioned mechanic of the top-mentioned month
            try {

                Optional<Map.Entry<String, Long>> max = voicesOfTopMentionedMonth.stream().filter((t) -> {
                    return !StringUtils.isBlank(t.getMechanicName()) && !StringUtils.isBlank(t.getCompanyId());
                }).collect(
                    Collectors.groupingBy((t) -> {
                        String companyId = t.getCompanyId() != null ? t.getCompanyId() : "";
                        String mechanicName = t.getMechanicName() != null ? t.getMechanicName() : "";
                        return companyId + MAGIC_SPLITOR + mechanicName;
                    }, Collectors.counting())
                ).entrySet().stream().max((e1, e2) -> e1.getValue().compareTo(e2.getValue()));
                if (max.isPresent()) {
                    Map.Entry<String, Long> topMechanicNameCount = max.get();
                    Integer count = (int) (long) topMechanicNameCount.getValue();
                    String companyId = topMechanicNameCount.getKey().split(MAGIC_SPLITOR)[0];
                    String mechanicName = topMechanicNameCount.getKey().split(MAGIC_SPLITOR)[1];
                    Company company = companyRepository.findOne(companyId);
                    if (company != null) {
                        mechanicName += " (" + company.getName() + ")";
                    }
                    resmap.put("topMentionedMechanic", ct(count, mechanicName));
                } else {
                    resmap.put("topMentionedMechanic", ct(0, ""));
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                resmap.put("topMentionedMechanic", ct(0, ""));
            }
        }

        //do annual statistics
        Specifications<CustomerVoice> annualSpec = where(targetYear(year))
            .and(hasMechanics(mechanicNames))
            .and(hasChkNumbers(chkNumbers))
            .and(hasCompanies(companyIds))
            .and(
                (voices != null && !voices.isEmpty())
                ? hasVoices(voices, voiceType)
                : voiceNotNull(voiceType)
            );
        List<CustomerVoice> cvs = customerVoiceRepository.findAll(annualSpec);

        // find top annual-mentioned remarks
        try {
            Map<String, Long> annualMentionedRemarkCount = cvs.stream().collect(
                Collectors.groupingBy((t) -> {
                    return getVoiceValue(t, voiceType);
                }, Collectors.counting())
            ).entrySet().stream().collect(
                Collectors.toMap(
                    t -> IndexTitleMap.get(voiceType).get(t.getKey()),
                    v -> v.getValue()
                )
            );
            List<Map> tops = tops(annualMentionedRemarkCount, 3);
            resmap.put("topAnnualMentionedRemarks", tops);
        } catch (Exception ex) {
            ex.printStackTrace();
            resmap.put("topAnnualMentionedRemarks", new ArrayList());
        }

        // find top annual-mentioned shops
        try {
            Map<String, Long> annualMentionedShopCount = cvs.stream().filter((t) -> {
                return !StringUtils.isBlank(t.getCompanyId());
            }).collect(
                Collectors.groupingBy(CustomerVoice::getCompanyId, Collectors.counting())
            ).entrySet().stream().collect(Collectors.toMap((t) -> {
                Company company = companyRepository.findOne(t.getKey());
                return company.getName();
            }, (t) -> t.getValue()));
            List<Map> tops = tops(annualMentionedShopCount, 3);
            resmap.put("topAnnualMentionedShops", tops);
        } catch (Exception ex) {
            ex.printStackTrace();
            resmap.put("topAnnualMentionedShops", new ArrayList());
        }

        // find top annual-mentioned mechanics
        try {

            Map<String, Long> annualMentionedMechanicCount = cvs.stream().filter((t) -> {
                return !StringUtils.isBlank(t.getMechanicName()) && !StringUtils.isBlank(t.getCompanyId());
            }).collect(
                Collectors.groupingBy((t) -> {
                    String companyId = t.getCompanyId();
                    String mechanicName = t.getMechanicName() != null ? t.getMechanicName() : "";
                    return companyId + MAGIC_SPLITOR + mechanicName;
                }, Collectors.counting())
            ).entrySet().stream().collect(Collectors.toMap(
                t -> {
                    String[] comme = t.getKey().split(MAGIC_SPLITOR);
                    Company company = companyRepository.findOne(comme[0]);
                    return comme[1] + " (" + company.getName() + ")";
                },
                v -> v.getValue())
            );
            List<Map> tops = tops(annualMentionedMechanicCount, 3);
            resmap.put("topAnnualMentionedMechanics", tops);
        } catch (Exception ex) {
            ex.printStackTrace();
            resmap.put("topAnnualMentionedMechanics", new ArrayList());
        }

        return resmap;
    }

    public Map csi(Integer year, List<String> companyNames, List<Integer> chkNumbers, List<String> mechanicNames, List<Integer> voices, String voiceType) {
        try {
            Map resmap = processTopMonthCsi(year, companyNames, chkNumbers, mechanicNames, voices, voiceType);
            resmap.put("success", true);
            return resmap;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return MapUtils.putAll(new HashMap<>(), new Object[]{"success", false});
    }

    private static Map ct(int count, String title) {
        return MapUtils.putAll(new HashMap<>(), ArrayUtils.toArray(
            "count", count,
            "title", title
        ));
    }

    private static List<Map> tops(Map<String, Long> map, int max) {
        List<Map.Entry<String, Long>> entries = new ArrayList<>(map.entrySet());
        Collections.sort(entries, (e1, e2) -> e2.getValue().compareTo(e1.getValue()));

        List<Map> collect = entries.stream().map((t) -> {
            return ct((int) (long) t.getValue(), t.getKey());
        }).collect(Collectors.toList());

        return collect.subList(0, Math.min(max, collect.size()));
    }

}
