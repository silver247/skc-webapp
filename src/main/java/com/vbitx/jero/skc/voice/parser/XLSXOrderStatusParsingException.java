/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vbitx.jero.skc.voice.parser;

/**
 *
 * @author gigadot
 */
public class XLSXOrderStatusParsingException extends Exception {

    public XLSXOrderStatusParsingException(String message) {
        super(message);
    }

    public XLSXOrderStatusParsingException(String message, Throwable cause) {
        super(message, cause);
    }

}
