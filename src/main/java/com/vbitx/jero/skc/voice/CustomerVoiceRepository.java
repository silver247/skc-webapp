/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vbitx.jero.skc.voice;

import java.util.List;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

/**
 *
 * @author gigadot
 */
@Repository
public interface CustomerVoiceRepository extends JpaRepository<CustomerVoice, Long>, JpaSpecificationExecutor<CustomerVoice> {

    public CustomerVoice findByRefId(String refId);
    
    @Override
    public List<CustomerVoice> findAll(Specification<CustomerVoice> specification);
    
}
