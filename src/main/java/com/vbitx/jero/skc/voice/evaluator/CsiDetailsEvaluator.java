/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vbitx.jero.skc.voice.evaluator;

import com.vbitx.jero.api.company.Company;
import com.vbitx.jero.api.company.CompanyRepository;
import com.vbitx.jero.skc.voice.CustomerVoice;
import com.vbitx.jero.skc.voice.CustomerVoiceRepository;
import static com.vbitx.jero.skc.voice.CustomerVoiceSpecs.hasChkNumbers;
import static com.vbitx.jero.skc.voice.CustomerVoiceSpecs.hasCompanies;
import static com.vbitx.jero.skc.voice.CustomerVoiceSpecs.hasMechanics;
import static com.vbitx.jero.skc.voice.CustomerVoiceSpecs.hasVoice;
import static com.vbitx.jero.skc.voice.CustomerVoiceSpecs.targetYear;
import static com.vbitx.jero.skc.voice.CustomerVoiceSpecs.targetYearMonth;
import static com.vbitx.jero.skc.voice.CustomerVoiceSpecs.voiceNotNull;
import com.vbitx.jero.skc.voice.MessyCustomerVoiceIndexes;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specifications;
import static org.springframework.data.jpa.domain.Specifications.where;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author gigadot
 */
@Service
public class CsiDetailsEvaluator {

    @Autowired
    private CustomerVoiceRepository customerVoiceRepository;

    @Autowired
    private CompanyRepository companyRepository;

    private Integer getVoiceValue(CustomerVoice customerVoice, String voiceType) {
        switch (voiceType) {
            case "positive":
                return customerVoice.getN50();
            case "negative":
                return customerVoice.getN51();
            case "complaint":
                return customerVoice.getN31r();
            case "suggestion":
                return customerVoice.getN49();
            default:
                return 0;
        }
    }

    /**
     *
     * @param year
     * @param companyIds
     * @param chkNumbers
     * @param mechanicNames
     * @param month
     * @param voiceValue
     * @param voiceType positive, negative, complaint, suggestion
     * @return
     */
    @Transactional(readOnly = true)
    public Map processTopMonthCsiPeople(
        Integer year,
        List<String> companyIds,
        List<Integer> chkNumbers,
        List<String> mechanicNames,
        Integer month,
        Integer voiceValue,
        String voiceType) {

        Specifications<CustomerVoice> spec = where((1 <= month && month <= 12) ? targetYearMonth(year, month - 1) : targetYear(year))
            .and(hasMechanics(mechanicNames))
            .and(hasChkNumbers(chkNumbers))
            .and(hasCompanies(companyIds))
            .and((voiceValue != 0) ? hasVoice(voiceValue, voiceType) : voiceNotNull(voiceType));
        List<CustomerVoice> cvs = customerVoiceRepository.findAll(spec);
        List<Map<String, Object>> voices = new ArrayList<>();
        if (cvs != null) {
            for (CustomerVoice cv : cvs) {
                Map<String, Object> data = new HashMap<>();
                data.put("interviewee", cv.getInterviewee());
                data.put("contactPhone", cv.getContactPhone());
                data.put("model", cv.getModel());
                data.put("chkNumber", cv.getChkNumber());
                try {
                    data.put("chkDate", DateFormatUtils.format(cv.getChkDate(), "yyyy-MM-dd"));
                } catch (Exception ex) {
                    data.put("chkDate", "");
                }
                data.put("mechanicName", cv.getMechanicName());
                Company company = companyRepository.findOne(cv.getCompanyId());
                data.put("companyName", (company != null) ? company.getName() : "");
                Integer vv = getVoiceValue(cv, voiceType);
                data.put("voiceTitle", MessyCustomerVoiceIndexes.IndexTitleMap.get(voiceType).get(vv));
                voices.add(data);
            }
        }
        voices = voices.stream().sorted((o1, o2) -> {
            return o1.get("companyName").toString().compareTo(o2.get("companyName").toString()); //To change body of generated lambdas, choose Tools | Templates.
        }).collect(Collectors.toList());
        Map<String, Object> resmap = new HashMap<>();
        resmap.put("voices", voices);
        return resmap;
    }

    public Map people(
        Integer year,
        List<String> companyNames,
        List<Integer> chkNumbers,
        List<String> mechanicNames,
        Integer month,
        Integer voiceValue,
        String voiceType
    ) {
        try {
            Map resmap = processTopMonthCsiPeople(year, companyNames, chkNumbers, mechanicNames, month, voiceValue, voiceType);
            resmap.put("success", true);
            return resmap;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return MapUtils.putAll(new HashMap<>(), new Object[]{"success", false});
    }

}
