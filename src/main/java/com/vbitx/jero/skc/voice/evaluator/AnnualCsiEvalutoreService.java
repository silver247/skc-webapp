/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vbitx.jero.skc.voice.evaluator;

import com.vbitx.jero.skc.voice.CustomerVoice;
import com.vbitx.jero.skc.voice.CustomerVoiceRepository;
import com.vbitx.jero.skc.voice.MessyCustomerVoiceIndexes;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import static org.springframework.data.jpa.domain.Specifications.*;
import static com.vbitx.jero.skc.voice.CustomerVoiceSpecs.*;
import java.lang.reflect.InvocationTargetException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.reflect.MethodUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author gigadot
 */
@Service
public class AnnualCsiEvalutoreService {

    @Autowired
    private CustomerVoiceRepository customerVoiceRepository;

    private int getNCount(CustomerVoice voice, List<String> ns) {
        int count = 0;
        for (String n : ns) {
            try {
                Object result = MethodUtils.invokeExactMethod(voice, n);
                if (result instanceof Integer && result != null) {
                    count++;
                }
            } catch (IllegalAccessException | NoSuchMethodException | InvocationTargetException ex) {
                System.out.println(ex.getMessage());
            }
        }
        return count;
    }

    @Transactional(readOnly = true)
    public Map currentMonthMiniCsiByYear() {
        int year = Calendar.getInstance().get(Calendar.YEAR);
        int month = Calendar.getInstance().get(Calendar.MONTH);
        List<CustomerVoice> voices = customerVoiceRepository.findAll(
            where(targetYearMonth(year, month))
        );
        int positiveVoiceCount = 0;
        int negativeVoiceCount = 0;
        int suggestionVoiceCount = 0;
        int complaintVoiceCount = 0;
        for (CustomerVoice voice : voices) {
            positiveVoiceCount += getNCount(voice, MessyCustomerVoiceIndexes.positiveMethods);
            negativeVoiceCount += getNCount(voice, MessyCustomerVoiceIndexes.negativeMethods);
            suggestionVoiceCount += getNCount(voice, MessyCustomerVoiceIndexes.suggestionMethods);
            complaintVoiceCount += getNCount(voice, MessyCustomerVoiceIndexes.complaintMethods);
            //complaintVoiceCount += getNCount(voice, MessyCustomerVoiceIndexes.complaintDeliveryMethods);
            //complaintVoiceCount += getNCount(voice, MessyCustomerVoiceIndexes.complaintServiceMethods);
        }
        return MapUtils.putAll(new HashMap<>(), ArrayUtils.toArray(
            "success", true,
            "title", DateFormatUtils.format(new Date(), "MMM yyyy"),
            "positive", positiveVoiceCount,
            "negative", negativeVoiceCount,
            "suggestion", suggestionVoiceCount,
            "complaint", complaintVoiceCount
        ));
    }

}
