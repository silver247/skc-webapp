/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vbitx.jero.skc.voice;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author gigadot
 */
public class MessyCustomerVoiceIndexes {

    public static final List<String> COMPLAINT_DELIVERY = new ArrayList<>(
        Arrays.asList("n2r", "n9r", "n10r", "n11r", "n12r", "n47r"));

    public static final List<String> COMPLAINT_SERVICE = new ArrayList<>(
        Arrays.asList("n21r", "n31r", "n32r", "n33r", "n36r", "n37r", "n38r", "n39r", "n40r", "n46r", "n48r"));

    public static final List<String> SUGGESTION = new ArrayList<>(
        Arrays.asList("n49"));

    public static final List<String> POSITIVE = new ArrayList<>(
        Arrays.asList("n50"));

    public static final List<String> NEGATIVE = new ArrayList<>(
        Arrays.asList("n51"));

    public static final List<String> COMPLAINT = new ArrayList<>();

    public static final List<String> POSITIVE_REMARKS = new ArrayList<>();

    public static final List<String> NEGATIVE_REMARKS = new ArrayList<>();

    public static final List<String> ALL = new ArrayList<>();

    public static final List<String> complaintMethods = new ArrayList<>();

    public static final List<String> complaintDeliveryMethods = new ArrayList<>();

    public static final List<String> complaintServiceMethods = new ArrayList<>();

    public static final List<String> suggestionMethods = new ArrayList<>();

    public static final List<String> positiveMethods = new ArrayList<>();

    public static final List<String> negativeMethods = new ArrayList<>();

    private static final void addGetN(List<String> from, List<String> to) {
        for (String n : from) {
            to.add("get" + StringUtils.capitalize(n));
        }
    }

    public static final Map TITLE_INDEXES = new HashMap<>();

    static {
        addGetN(NEGATIVE, negativeMethods);
        addGetN(POSITIVE, positiveMethods);
        addGetN(SUGGESTION, suggestionMethods);
        addGetN(COMPLAINT_DELIVERY, complaintDeliveryMethods);
        addGetN(COMPLAINT_SERVICE, complaintServiceMethods);
        complaintMethods.add("getN31r");

        COMPLAINT.addAll(COMPLAINT_DELIVERY);
        COMPLAINT.addAll(COMPLAINT_SERVICE);

        ALL.addAll(NEGATIVE);
        ALL.addAll(POSITIVE);
        ALL.addAll(SUGGESTION);
        ALL.addAll(COMPLAINT);

        POSITIVE_REMARKS.addAll(POSITIVE);

        NEGATIVE_REMARKS.addAll(NEGATIVE);

        InputStream in = MessyCustomerVoiceIndexes.class.getClassLoader().getResourceAsStream("csi-key-title.json");
        ObjectMapper om = new ObjectMapper();
        try {
            Map titleMap = om.readValue(in, Map.class);
            TITLE_INDEXES.putAll(titleMap);
        } catch (Exception e) {
        }
    }

    public static String getIndexTitle(String nName, Integer value) {
        Map nMap = (Map) TITLE_INDEXES.get(nName);
        return (String) ((Map) nMap.get("codes")).get(Integer.toString(value));
    }

    private static Map<Integer, String> monthNames = MapUtils.putAll(new HashMap<>(), ArrayUtils.toArray(
        0, "มกราคม",
        1, "กุมภาพันธ์",
        2, "มีนาคม",
        3, "เมษายน",
        4, "พฤษภาคม",
        5, "มิถุนายน",
        6, "กรกฎาคม",
        7, "สิงหาคม",
        8, "กันยายน",
        9, "ตุลาคม",
        10, "พฤศจิกายน",
        11, "ธันวาคม"
    ));

    public static String getMonthName(int maxMonthIndex) {
        return monthNames.get(maxMonthIndex);
    }

    public static final Map<Integer, String> positiveIndexTitleMap = MapUtils.unmodifiableMap(MapUtils.putAll(new HashMap<Integer, String>(), ArrayUtils.toArray(
        1, "ช่างบริการดีเป็นที่น่าประทับใจ",
        2, "ช่างพูดจาสุภาพ ดูแล้วเป็นมิตร",
        3, "ช่างแต่งกายสุภาพ",
        4, "ช่างมีความรู้ดี เก่ง",
        5, "อื่นๆ"
    )));

    public static final Map<Integer, String> negativeIndexTitleMap = MapUtils.unmodifiableMap(MapUtils.putAll(new HashMap<Integer, String>(), ArrayUtils.toArray(
        1, "ราคาอะไหล่แพง",
        2, "อะไหล่ไม่มีคุณภาพ",
        3, "ของแถมที่ได้รับไม่มีคุณภาพ",
        4, "ช่างออกมาบริการช้ามาก",
        5, "ติดต่อทางร้านยาก พนักงานไม่รับโทรศัพท์",
        6, "อื่นๆ"
    )));

    public static final Map<Integer, String> complaintIndexTitleMap = MapUtils.unmodifiableMap(MapUtils.putAll(new HashMap<Integer, String>(), ArrayUtils.toArray(
        //1, "สอบถามเหตุผลแล้วแต่ลูกค้าพอใจให้คะแนนเท่านี้",
        2, "ช่างตรวจเช็คไม่ละเอียด",
        3, "แก้ไขปัญหารถไม่ได้",
        4, "ช่างไม่ทำการตรวจสอบสภาพทั่วไป (น้ำในหม้อน้ำ น้ำมันต่างๆ น้ำกลั่น กรองต่าง ๆ)",
        5, "ช่างไม่ทำการตรวจสอบการทำงานของอุปกรณ์และชิ้นส่วนต่าง ๆ (คลัตซ์ เพลา พวงมาลัย แขนยก คอนโทรลต่างๆ)",
        6, "ช่างไม่ทำการปรับตั้งส่วนต่าง ๆ (ระยะฟรีคันเหยียบคลัตซ์ ระยะฟรีคันเหยียบเบรกซ้าย-ขวา สายพานพัดลม)",
        7, "ช่างไม่ได้ตรวจสอบการรั่วซึมของน้ำมันตามจุดต่างๆ",
        8, "ช่างไม่ได้ใช้ความระมัดระวังในการทำงานไม่รักษารถของลูกค้า",
        9, "ช่างตรวจเช็ครถให้ผ่านๆเพราะรถยังไม่มีปัญหาอะไร",
        10, "ช่างทำงานไม่เรียบร้อย (น้ำมันหกเลอะเทอะ)",
        11, "ช่างให้เด็กฝึกงานตรวจเช็ครถ ตัวเองยืนดู",
        12, "รถอยู่ในสภาพปกติ ช่างจึงไม่ได้ตรวจเช็คอะไรมาก พอใจให้คะแนนเท่านี้",
        13, "ช่างไม่ได้ทำอะไรเลย มาเดินรอบรถแล้วให้เซ็นชื่อในใบตรวจเช็ค แล้วกลับ",
        14, "ช่างไม่ได้ทำอะไรเลย มาขูดหมายเลขรถแล้วก็กลับ",
        15, "ช่างไม่ได้ชี้แจงการตรวจเช็คให้ว่ากำลังทำอะไรอยู่",
        16, "ช่างไม่มีการทดสอบการใช้งานภายหลังการตรวจเช็ค ( ไม่มีการสตาร์ทรถ/ไม่มีการทดลองขับ )",
        17, "ช่างไม่มีการทดสอบการใช้งานภายหลังการตรวจเช็ค ( มีการสตาร์ทรถแต่ไม่มีการทดลองขับ )",
        18, "ช่างตรวจเช็คครบทุกจุดแต่ไม่มีการปรับตั้งส่วนต่างๆของรถให้เนื่องจากรถอยู่ในสภาพปกติ",
        19, "ช่างมาดู แต่ไม่รู้ว่าทำอะไรบ้าง และช่างไม่ได้สรุปว่าทำอะไร",
        20, "อื่นๆ"
    )));

    public static final Map<Integer, String> suggestionIndexTitleMap = MapUtils.unmodifiableMap(MapUtils.putAll(new HashMap<Integer, String>(), ArrayUtils.toArray(
        1, "อยากให้มีการโทรนัดล่วงหน้าก่อนเข้ามาให้บริการ",
        2, "อยากให้ช่างเข้ามาตรวจเช็ครถตรงเวลาที่นัดหมาย",
        3, "อยากให้มีการโทรบอกล่วงหน้า หากมีการเลื่อนนัดการตรวจเช็ครถออกไป",
        4, "อยากให้แจ้งเรื่องการเปลี่ยนอะไหล่ น้ำมัน และค่าใช้จ่ายก่อนที่จะออกมาให้บริการ",
        5, "อยากให้มีการประสานงานระหว่างพนักงานโทรนัดกับช่างที่ออกปฏิบัติงานให้ดีกว่านี้",
        6, "ควรแนะนำสิทธิประโยชน์ของลูกค้าก่อนเข้ามาบริการทุกครั้ง",
        7, "อยากให้ช่างเข้ามาซ่อมรถทันที เมื่อลูกค้าแจ้งว่ารถเสีย / มีปัญหา (มาบริการเร็วๆ)",
        8, "อยากให้ช่างเตรียมเครื่องมือ อะไหล่ มาให้พร้อมเวลาออกมาให้บริการ",
        9, "อยากให้ช่างเตรียมอะไหล่ น้ำมันมาเพิ่มเติม เผื่อลูกค้าอาจจะซื้อเก็บไว้ใช้ / เปลี่ยน",
        10, "อยากให้ช่างตรวจเช็ครถให้ละเอียดกว่านี้",
        11, "อยากให้ช่างแนะนำเกี่ยวกับการบำรุงรักษารถให้ละเอียด",
        12, "อยากให้ช่างแนะนำเกี่ยวกับเรื่องการใช้รถให้ละเอียด",
        13, "อยากให้ช่างแต่งเครื่องแบบพนักงานคูโบต้าและมีบัตรพนักงาน เพื่อให้ลูกค้าเชื่อมั่นว่าเป็นช่างคูโบต้า",
        14, "อยากให้อบรมช่างให้มีความรู้มากกว่านี้",
        15, "อยากให้เพิ่มจำนวนช่างที่ออกมาให้บริการ",
        16, "อื่นๆ"
    )));

    public static final Map<String, Map<Integer, String>> IndexTitleMap = MapUtils.unmodifiableMap(
        MapUtils.putAll(new HashMap<String, Map<Integer, String>>(), ArrayUtils.toArray(
            "suggestion", suggestionIndexTitleMap,
            "complaint", complaintIndexTitleMap,
            "positive", positiveIndexTitleMap,
            "negative", negativeIndexTitleMap
        ))
    );

}
