/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vbitx.jero.skc.voice;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.data.jpa.domain.Specification;

/**
 *
 * @author gigadot
 */
public class CustomerVoiceSpecs {

    public static Specification<CustomerVoice> targetYear(Integer year) {
        Integer targetYear = year;
        if (year == null || year < 2000) {
            targetYear = Calendar.getInstance().get(Calendar.YEAR);
        }
        String startTargetDateStr = targetYear + "-01-01";
        String endTargetDateStr = (targetYear) + "-12-31";
        return CustomerVoiceSpecs.targetDateBetween(startTargetDateStr, endTargetDateStr);
    }

    /**
     *
     * @param year
     * @param month zero-based month
     * @return
     */
    public static Specification<CustomerVoice> targetYearMonth(Integer year, int month) {
        try {
            Integer targetYear = year;
            if (year == null || year < 2000) {
                targetYear = Calendar.getInstance().get(Calendar.YEAR);
            }
            String startTargetDateStr = targetYear + "-" + StringUtils.leftPad(Integer.toString(month + 1), 2, "0") + "-01";
            Date startTargetDate = DateUtils.parseDate(startTargetDateStr, "yyyy-MM-dd");
            Date endTargetDate = DateUtils.addDays(DateUtils.addMonths(startTargetDate, 1), -1);
            return CustomerVoiceSpecs.targetDateBetween(startTargetDate, endTargetDate);
        } catch (ParseException ex) {
        }
        return null;
    }

    public static Specification<CustomerVoice> targetDateBetween(String startTargetDateStr, String endTargetDateStr) {
        try {
            Date startTargetDate = DateUtils.parseDate(startTargetDateStr, "yyyy-MM-dd");
            Date endTargetDate = DateUtils.parseDate(endTargetDateStr, "yyyy-MM-dd");
            return targetDateBetween(startTargetDate, endTargetDate);
        } catch (ParseException ex) {
        }
        return null;
    }

    public static Specification<CustomerVoice> targetDateBetween(Date startTargetDate, Date endTargetDate) {
        return new Specification<CustomerVoice>() {
            public Predicate toPredicate(
                Root<CustomerVoice> root,
                CriteriaQuery<?> query,
                CriteriaBuilder builder) {
                if (startTargetDate != null && endTargetDate != null && startTargetDate.before(endTargetDate)) {
                    return builder.between(root.get("targetDate"), startTargetDate, endTargetDate);
                } else {
                    return null;
                }
            }
        };
    }

    public static Specification<CustomerVoice> hasMechanics(List<String> mechanicNames) {
        return new Specification<CustomerVoice>() {
            public Predicate toPredicate(
                Root<CustomerVoice> root,
                CriteriaQuery<?> query,
                CriteriaBuilder builder) {
                if (mechanicNames != null && !mechanicNames.isEmpty()) {
                    return root.get("mechanicName").in(mechanicNames);
                }
                return null;
            }
        };
    }

    public static Specification<CustomerVoice> hasChkNumbers(List<Integer> chkNumbers) {
        return new Specification<CustomerVoice>() {
            public Predicate toPredicate(
                Root<CustomerVoice> root,
                CriteriaQuery<?> query,
                CriteriaBuilder builder) {
                if (chkNumbers != null && !chkNumbers.isEmpty()) {
                    return root.get("chkNumber").in(chkNumbers);
                }
                return null;
            }
        };
    }

    public static Specification<CustomerVoice> hasCompanies(List<String> companIds) {
        return new Specification<CustomerVoice>() {
            public Predicate toPredicate(
                Root<CustomerVoice> root,
                CriteriaQuery<?> query,
                CriteriaBuilder builder) {
                if (companIds != null && !companIds.isEmpty()) {
                    return root.get("companyId").in(companIds);
                }
                return null;
            }
        };
    }

    public static Map<String, String> VOICE_TYPE_FIELD_NAME = MapUtils.putAll(new HashMap<>(), new String[]{
        "positive", "n50",
        "negative", "n51",
        "complaint", "n31r",
        "suggestion", "n49"
    });

    public static Specification<CustomerVoice> hasVoices(List<Integer> voices, String voiceType) {
        return new Specification<CustomerVoice>() {
            public Predicate toPredicate(
                Root<CustomerVoice> root,
                CriteriaQuery<?> query,
                CriteriaBuilder builder) {
                if (voices != null && !voices.isEmpty()) {
                    String fieldName = VOICE_TYPE_FIELD_NAME.get(voiceType);
                    if (fieldName == null) {
                        return null;
                    }
                    return root.get(fieldName).in(voices);
                }
                return null;
            }
        };
    }

    public static Specification<CustomerVoice> hasVoice(Integer voiceValue, String voiceType) {
        return new Specification<CustomerVoice>() {
            public Predicate toPredicate(
                Root<CustomerVoice> root,
                CriteriaQuery<?> query,
                CriteriaBuilder builder) {
                if (voiceValue != null && voiceValue != 0) {
                    String fieldName = VOICE_TYPE_FIELD_NAME.get(voiceType);
                    if (fieldName == null) {
                        return null;
                    }
                    return builder.equal(root.get(fieldName), voiceValue);
                }
                return null;
            }
        };
    }

    public static Specification<CustomerVoice> voiceNotNull(String voiceType) {
        return new Specification<CustomerVoice>() {
            public Predicate toPredicate(
                Root<CustomerVoice> root,
                CriteriaQuery<?> query,
                CriteriaBuilder builder) {
                String fieldName = VOICE_TYPE_FIELD_NAME.get(voiceType);
                if (fieldName == null) {
                    return null;
                }
                return root.get(fieldName).isNotNull();
            }
        };
    }

}
