/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vbitx.jero.skc.voice.parser;

import com.vbitx.jero.api.company.Company;
import com.vbitx.jero.api.company.CompanyRepository;
import com.vbitx.jero.skc.voice.CustomerVoice;
import com.vbitx.jero.skc.voice.CustomerVoiceRepository;
import com.vbitx.jero.skc.voice.MessyCustomerVoiceIndexes;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.lang3.reflect.MethodUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author gigadot
 */
public class XlsxCsiRawDataReader {

    private final InputStream xlsxInputStream;
    private boolean isParsed = false;
    private List<String> errors = new ArrayList<>();

    private Map<String, Integer> headers = new HashMap<>();
    private List<String> ids = new ArrayList<>();
    private Map<String, List<String>> rows = new HashMap<>();
    private int idHeaderIndex = -1;
    private final CustomerVoiceRepository customerVoiceRepository;
    private final CompanyRepository companyRepository;

    public XlsxCsiRawDataReader(InputStream xlsxInputStream, CustomerVoiceRepository customerVoiceRepository, CompanyRepository companyRepository) {
        this.customerVoiceRepository = customerVoiceRepository;
        this.companyRepository = companyRepository;
        this.xlsxInputStream = xlsxInputStream;
        parse();
    }

    private void parse() {
        if (isParsed) {
            return;
        }
        try {
            isParsed = true;
            Workbook workbook = new XSSFWorkbook(xlsxInputStream);
            if (workbook.getNumberOfSheets() == 0) {
                errors.add("No sheet found.");
                return;
            } else if (workbook.getNumberOfSheets() > 1) {
                errors.add("Multiple sheets found. Only parse the first sheet.");
            }
            Sheet datatypeSheet = workbook.getSheetAt(0);
            datatypeSheet.getPhysicalNumberOfRows();
            if (datatypeSheet.getPhysicalNumberOfRows() == 0) {
                errors.add("No row found.");
                return;
            } else if (datatypeSheet.getPhysicalNumberOfRows() == 1) {
                errors.add("No data found.");
                return;
            }

            for (int i = 0; i < datatypeSheet.getPhysicalNumberOfRows(); i++) {
                Row currentRow = datatypeSheet.getRow(i);
                if (idHeaderIndex < 0) {
                    parseHeaders(currentRow);
                    if (headers == null) {
                        errors.add("Headers not found");
                        break;
                    }
                } else {
                    convertRowToList(currentRow);
                }
            }
        } catch (IOException e) {
            errors.add("Unable to read file.");
        }
    }

    public int size() {
        return rows.size();
    }

    public void print() {
        for (String error : errors) {
            System.out.println(error);
        }
    }

    private void parseHeaders(Row row) {
        int minColIx = row.getFirstCellNum();
        int maxColIx = row.getLastCellNum();
        for (int colIx = minColIx; colIx < maxColIx; colIx++) {
            Cell cell = row.getCell(colIx);
            String stringCellValue = null;
            if (cell != null) {
                cell.setCellType(CellType.STRING);
                stringCellValue = StringUtils.trim(cell.getStringCellValue());
                if ("id".equals(stringCellValue)) {
                    idHeaderIndex = colIx;
                }
            }
            headers.put(stringCellValue, colIx);
        }
        if (idHeaderIndex < 0) {
            headers = null;
        }
    }

    private Integer getInt(Cell c) {
        c.setCellType(CellType.STRING);
        Integer i = NumberUtils.toInt(c.getStringCellValue(), 0);
        return (i == 0) ? null : i;
    }

    private String getString(Cell c) {
        c.setCellType(CellType.STRING);
        return c.getStringCellValue();
    }

    private Date getDate(Cell c) {
        c.setCellType(CellType.NUMERIC);
        Date dateCellValue = c.getDateCellValue();
        if (dateCellValue != null) {
            LocalDate localDate = dateCellValue.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
            int year = localDate.getYear();
            if (year > 2543) {
                dateCellValue = DateUtils.addYears(dateCellValue, -543);
            }
        }
        return dateCellValue;
    }

    private void setN(CustomerVoice v, Row row, String n) {
        try {
            Integer nIdx = headers.get(n);
            Cell nCell = row.getCell(nIdx);
            Integer nVal = getInt(nCell);
            String mname = "set" + StringUtils.capitalize(n);
            MethodUtils.invokeExactMethod(v, mname, nVal);
        } catch (Exception e) {
        }
    }

    private boolean hasNComplaint(CustomerVoice v, String n) {
        Integer nVal = null;
        try {
            nVal = (Integer) MethodUtils.invokeExactMethod(v, "get" + StringUtils.capitalize(n));
        } catch (Exception e) {
        }
        return nVal != null && nVal != 0;
    }

    private int countNComplaint(CustomerVoice v, List<String> ns) {
        int count = 0;
        for (String n : ns) {
            count += hasNComplaint(v, n) ? 1 : 0;
        }
        return count;
    }

    private void convertRowToList(Row row) {
        Integer refIdIdx = headers.get("id");
        Integer chkIdx = headers.get("chk");
        Integer buy_shopIdx = headers.get("shop_id");
        Integer technicianIdx = headers.get("technician");
        Integer keyin_dateIdx = headers.get("keyin_date");

        Integer chk_dateIdx = headers.get("chk_date");
        Integer chk_monthIdx = headers.get("chk_month");
        Integer chk_yearIdx = headers.get("chk_year");

        Integer modelIdx = headers.get("model");
        Integer intervieweeIdx = headers.get("interviewee");
        Integer contact_phoneIdx = headers.get("contact_phone");

        Cell refIdCell = row.getCell(refIdIdx);
        Cell chkCell = row.getCell(chkIdx);
        Cell buy_shopCell = row.getCell(buy_shopIdx);
        Cell technicianCell = row.getCell(technicianIdx);
        Cell keyin_dateCell = row.getCell(keyin_dateIdx);
        Cell chk_dateCell = row.getCell(chk_dateIdx);
        Cell chk_monthCell = row.getCell(chk_monthIdx);
        Cell chk_yearCell = row.getCell(chk_yearIdx);
        Cell modelCell = row.getCell(modelIdx);
        Cell intervieweeCell = row.getCell(intervieweeIdx);
        Cell contact_phoneCell = row.getCell(contact_phoneIdx);

        Integer chk = getInt(chkCell);
        String refId = getString(refIdCell);
        String shopId = getString(buy_shopCell);
        Company company = companyRepository.findByDisplayId(shopId);
        if (company == null) {
            return;
        }
        String technician = getString(technicianCell);
        Date keyin_date = getDate(keyin_dateCell);

        Integer chk_date = getInt(chk_dateCell);
        Integer chk_month = getInt(chk_monthCell);
        Integer chk_year = getInt(chk_yearCell);
        if (chk_year > 2543) {
            chk_year = chk_year - 543;
        }
        String chkDateStr = chk_year + "-" + StringUtils.leftPad(chk_month.toString(), 2, "0") + "-" + StringUtils.leftPad(chk_date.toString(), 2, "0");
        Date chkDate = null;
        try {
            chkDate = DateUtils.parseDate(chkDateStr, "yyyy-MM-dd");
        } catch (ParseException ex) {
            return;
        }
        String model = getString(modelCell);
        String interviewee = getString(intervieweeCell);
        String contact_phone = getString(contact_phoneCell);

        CustomerVoice v;

        CustomerVoice cv = customerVoiceRepository.findByRefId(refId);

        if (cv == null) {
            v = new CustomerVoice();
        } else {
            v = cv;
        }
        v.setRefId(refId);
        v.setChkNumber(chk);
        v.setCompanyId(company.getId());
        v.setMechanicName(technician);
        v.setTargetDate(keyin_date);
        v.setChkDate(chkDate);
        v.setModel(model);
        v.setInterviewee(interviewee);
        v.setContactPhone(contact_phone);
        for (String persistField : MessyCustomerVoiceIndexes.ALL) {
            setN(v, row, persistField);
        }
        if (v.getN31r() != null && !MessyCustomerVoiceIndexes.complaintIndexTitleMap.containsKey(v.getN31r())) {
            v.setN31r(null);
        }
        if (v.getN51() != null && !MessyCustomerVoiceIndexes.negativeIndexTitleMap.containsKey(v.getN51())) {
            v.setN51(null);
        }
        if (v.getN50() != null && !MessyCustomerVoiceIndexes.positiveIndexTitleMap.containsKey(v.getN50())) {
            v.setN50(null);
        }
        if (v.getN49() != null && !MessyCustomerVoiceIndexes.suggestionIndexTitleMap.containsKey(v.getN49())) {
            v.setN49(null);
        }

        int countNComplaint = countNComplaint(v, MessyCustomerVoiceIndexes.COMPLAINT);
        v.setComplaintCount(countNComplaint);

        customerVoiceRepository.save(v);

    }

}
