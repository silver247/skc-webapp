/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vbitx.jero.skc.voice;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

/**
 *
 * @author gigadot
 */
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(indexes = {
    @Index(columnList = "refId", name = "ref_id_idx"),
    @Index(columnList = "targetDate", name = "target_date_idx"),
    @Index(columnList = "companyId", name = "company_id_idx"),
    @Index(columnList = "chkNumber", name = "chk_number_idx"),
    @Index(columnList = "mechanicName", name = "mechanic_name_idx")
})
public class CustomerVoice {

    @Id
    @GeneratedValue
    private Long id;

    @CreatedDate
    @Temporal(TemporalType.TIMESTAMP)
    private Date created;

    @LastModifiedDate
    @Temporal(TemporalType.TIMESTAMP)
    private Date modified;

    private String refId;

    @Temporal(TemporalType.DATE)
    private Date targetDate;

    private String companyId;

    // chk
    private Integer chkNumber;

    @Temporal(TemporalType.DATE)
    private Date chkDate;

    private String model;
    
    private String interviewee;
    
    private String contactPhone;

    // technician
    private String mechanicName;
    // complaint delivery
    private Integer n2r;
    private Integer n9r;
    private Integer n10r;
    private Integer n11r;
    private Integer n12r;
    private Integer n47r;

    // complaint service
    private Integer n21r;
    private Integer n31r;
    private Integer n32r;
    private Integer n33r;
    private Integer n36r;
    private Integer n37r;
    private Integer n38r;
    private Integer n39r;
    private Integer n40r;
    private Integer n46r;
    private Integer n48r;
    
    private Integer complaintCount;

    // n49
    private Integer n49;

    // n50
    private Integer n50;

    // n51
    private Integer n51;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRefId() {
        return refId;
    }

    public void setRefId(String refId) {
        this.refId = refId;
    }

    public Date getTargetDate() {
        return targetDate;
    }

    public void setTargetDate(Date targetDate) {
        this.targetDate = targetDate;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getModified() {
        return modified;
    }

    public void setModified(Date modified) {
        this.modified = modified;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public Integer getChkNumber() {
        return chkNumber;
    }

    public void setChkNumber(Integer chkNumber) {
        this.chkNumber = chkNumber;
    }

    public String getMechanicName() {
        return mechanicName;
    }

    public void setMechanicName(String mechanicName) {
        this.mechanicName = mechanicName;
    }

    public Integer getN2r() {
        return n2r;
    }

    public void setN2r(Integer n2r) {
        this.n2r = n2r;
    }

    public Integer getN9r() {
        return n9r;
    }

    public void setN9r(Integer n9r) {
        this.n9r = n9r;
    }

    public Integer getN10r() {
        return n10r;
    }

    public void setN10r(Integer n10r) {
        this.n10r = n10r;
    }

    public Integer getN11r() {
        return n11r;
    }

    public void setN11r(Integer n11r) {
        this.n11r = n11r;
    }

    public Integer getN12r() {
        return n12r;
    }

    public void setN12r(Integer n12r) {
        this.n12r = n12r;
    }

    public Integer getN47r() {
        return n47r;
    }

    public void setN47r(Integer n47r) {
        this.n47r = n47r;
    }

    public Integer getN21r() {
        return n21r;
    }

    public void setN21r(Integer n21r) {
        this.n21r = n21r;
    }

    public Integer getN31r() {
        return n31r;
    }

    public void setN31r(Integer n31r) {
        this.n31r = n31r;
    }

    public Integer getN32r() {
        return n32r;
    }

    public void setN32r(Integer n32r) {
        this.n32r = n32r;
    }

    public Integer getN33r() {
        return n33r;
    }

    public void setN33r(Integer n33r) {
        this.n33r = n33r;
    }

    public Integer getN36r() {
        return n36r;
    }

    public void setN36r(Integer n36r) {
        this.n36r = n36r;
    }

    public Integer getN37r() {
        return n37r;
    }

    public void setN37r(Integer n37r) {
        this.n37r = n37r;
    }

    public Integer getN38r() {
        return n38r;
    }

    public void setN38r(Integer n38r) {
        this.n38r = n38r;
    }

    public Integer getN39r() {
        return n39r;
    }

    public void setN39r(Integer n39r) {
        this.n39r = n39r;
    }

    public Integer getN40r() {
        return n40r;
    }

    public void setN40r(Integer n40r) {
        this.n40r = n40r;
    }

    public Integer getN46r() {
        return n46r;
    }

    public void setN46r(Integer n46r) {
        this.n46r = n46r;
    }

    public Integer getComplaintCount() {
        return complaintCount;
    }

    public void setComplaintCount(Integer complaintCount) {
        this.complaintCount = complaintCount;
    }

    public Integer getN48r() {
        return n48r;
    }

    public void setN48r(Integer n48r) {
        this.n48r = n48r;
    }

    public Integer getN49() {
        return n49;
    }

    public void setN49(Integer n49) {
        this.n49 = n49;
    }

    public Integer getN50() {
        return n50;
    }

    public void setN50(Integer n50) {
        this.n50 = n50;
    }

    public Integer getN51() {
        return n51;
    }

    public void setN51(Integer n51) {
        this.n51 = n51;
    }

    public Date getChkDate() {
        return chkDate;
    }

    public void setChkDate(Date chkDate) {
        this.chkDate = chkDate;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getInterviewee() {
        return interviewee;
    }

    public void setInterviewee(String interviewee) {
        this.interviewee = interviewee;
    }

    public String getContactPhone() {
        return contactPhone;
    }

    public void setContactPhone(String contactPhone) {
        this.contactPhone = contactPhone;
    }

}
