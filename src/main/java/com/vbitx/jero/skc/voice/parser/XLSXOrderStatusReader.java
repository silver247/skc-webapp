/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vbitx.jero.skc.voice.parser;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author gigadot
 */
public class XLSXOrderStatusReader {

    private final InputStream xlsxInputStream;

    private boolean isParsed = false;

    private List<String> errors = new ArrayList<>();

    private List<String> warnings = new ArrayList<>();

    private List<StatusRowValues> statusRowValuesList = new ArrayList<>();

    public XLSXOrderStatusReader(InputStream xlsxInputStream) {
        this.xlsxInputStream = xlsxInputStream;
        parse();
    }

    private void parse() {
        if (isParsed) {
            return;
        }
        try {
            isParsed = true;
            Workbook workbook = new XSSFWorkbook(xlsxInputStream);
            if (workbook.getNumberOfSheets() == 0) {
                errors.add("No sheet found.");
                return;
            } else if (workbook.getNumberOfSheets() > 1) {
                warnings.add("Multiple sheets found. Only parse the first sheet.");
            }
            Sheet datatypeSheet = workbook.getSheetAt(0);
            datatypeSheet.getPhysicalNumberOfRows();
            if (datatypeSheet.getPhysicalNumberOfRows() == 0) {
                errors.add("No row found.");
                return;
            } else if (datatypeSheet.getPhysicalNumberOfRows() == 1) {
                errors.add("No data found.");
                return;
            }
            boolean hasParsedHeader = false;
            StatusHeaderIndexes statusHeaderIndexes = null;

            for (int i = 0; i < datatypeSheet.getPhysicalNumberOfRows(); i++) {
                Row currentRow = datatypeSheet.getRow(i);
                if (!hasParsedHeader) {
                    statusHeaderIndexes = new StatusHeaderIndexes(currentRow);
                    if (!statusHeaderIndexes.isValid()) {
                        errors.addAll(statusHeaderIndexes.getErrors());
                        return;
                    }
                    hasParsedHeader = true;
                } else if (null != statusHeaderIndexes) {
                    StatusRowValues statusRowValues = new StatusRowValues(currentRow, statusHeaderIndexes);
                    statusRowValuesList.add(statusRowValues);
                    errors.addAll(statusRowValues.getErrors());
                }
            }
        } catch (IOException e) {
            errors.add("Unable to read file.");
        }
    }

    public boolean hasErrors() {
        return !errors.isEmpty();
    }

    public List<String> getErrors() {
        return errors;
    }

    public String getId(int index) {
        return statusRowValuesList.get(index).getId();
    }

    public Date getCallingDate(int index) {
        return statusRowValuesList.get(index).getCallingDate();
    }

    public String getStatus(int index) {
        return statusRowValuesList.get(index).getStatus();
    }

    public String getNote(int index) {
        return statusRowValuesList.get(index).getNote();
    }

    public int size() {
        return statusRowValuesList.size();
    }

    public void print() {
        for (String error : errors) {
            System.out.println(error);
        }
        for (StatusRowValues statusRowValues : statusRowValuesList) {
            System.out.println(statusRowValues);
        }
    }

    private static final class StatusHeaderIndexes {

        private final static String ID = "Document ID";
        private final static String CALLING_DATE = "Date of calling";
        private final static String STATUS = "Status";
        private final static String NOTE = "Note";

        private int idIndex = -1;
        private int callingDateIndex = -1;
        private int statusIndex = -1;
        private int noteIndex = -1;

        public StatusHeaderIndexes(Row row) {
            int minColIx = row.getFirstCellNum();
            int maxColIx = row.getLastCellNum();
            for (int colIx = minColIx; colIx < maxColIx; colIx++) {
                Cell cell = row.getCell(colIx);
                if (cell != null) {
                    cell.setCellType(CellType.STRING);
                    String stringCellValue = StringUtils.trim(cell.getStringCellValue());
                    switch (stringCellValue) {
                        case ID:
                            idIndex = colIx;
                            break;
                        case CALLING_DATE:
                            callingDateIndex = colIx;
                            break;
                        case STATUS:
                            statusIndex = colIx;
                            break;
                        case NOTE:
                            noteIndex = colIx;
                            break;
                        default:
                    }
                }
            }
        }

        public int getIdIndex() {
            return idIndex;
        }

        public int getCallingDateIndex() {
            return callingDateIndex;
        }

        public int getStatusIndex() {
            return statusIndex;
        }

        public int getNoteIndex() {
            return noteIndex;
        }

        public boolean isValid() {
            return !((idIndex == -1) || (callingDateIndex == -1)
                    || (statusIndex == -1) || (noteIndex == -1));
        }

        private List<String> errors = null;

        public List<String> getErrors() {
            if (null != errors) {
                return errors;
            }
            errors = new ArrayList<>();
            if (idIndex == -1) {
                errors.add("Column \"" + ID + "\" not found.");
            }
            if (callingDateIndex == -1) {
                errors.add("Column \"" + CALLING_DATE + "\" not found.");
            }
            if (statusIndex == -1) {
                errors.add("Column \"" + STATUS + "\" not found.");
            }
            if (noteIndex == -1) {
                errors.add("Column \"" + NOTE + "\" not found.");
            }
            return errors;
        }
    }

    public final static Set<String> KNOWN_STATUSES = Collections.unmodifiableSet(new LinkedHashSet<String>() {
        {
            add("completed");
            add("cannot_give_information");
            add("rejected");
            add("not_yet_interviewed");
            add("not_picked_up");
            add("wait_for_calling_back");
            add("turn_off");
            add("wrong_number");
            add("cannot_connect");
            add("terminate_interviews");
            add("not_customer_telephone_number");
            add("wait_for_investigation");
            add("already_exist");
        }
    });

    private static final class StatusRowValues {

        private String id = null;
        private Date callingDate = null;
        private String status = null;
        private String note = null;
        private int rowIndex = -1;

        private String orgStatusValue;

        public StatusRowValues(Row row, StatusHeaderIndexes statusHeaderIndexes) {
            Cell idCell = row.getCell(statusHeaderIndexes.idIndex);
            if (idCell != null) {
                try {
                    idCell.setCellType(CellType.STRING);
                    id = idCell.getStringCellValue();
                } catch (Exception e) {
                }
            }
            Cell callingDateCell = row.getCell(statusHeaderIndexes.callingDateIndex);
            if (callingDateCell != null) {
                try {
                    callingDateCell.setCellType(CellType.STRING);
                    String callingDateStr = callingDateCell.getStringCellValue();
                    callingDate = DateUtils.parseDate(callingDateStr, "dd/MM/yyyy");
                } catch (Exception e) {
                }
            }
            Cell statusCell = row.getCell(statusHeaderIndexes.statusIndex);
            if (statusCell != null) {
                try {
                    statusCell.setCellType(CellType.STRING);
                    status = statusCell.getStringCellValue();
                    orgStatusValue = status;
                    if (status != null) {
                        status = status.trim().toLowerCase().replaceAll("\\s+", "_");
                    }
                    if (StringUtils.isBlank(status)) {
                        status = null;
                    }
                } catch (Exception e) {
                }
            }
            Cell noteCell = row.getCell(statusHeaderIndexes.noteIndex);
            if (noteCell != null) {
                try {
                    noteCell.setCellType(CellType.STRING);
                    note = noteCell.getStringCellValue();
                } catch (Exception e) {
                }
            }
            rowIndex = row.getRowNum();
        }

        public String getId() {
            return id;
        }

        public Date getCallingDate() {
            return callingDate;
        }

        public String getStatus() {
            return status;
        }

        public String getNote() {
            return note;
        }

        public int getRowIndex() {
            return rowIndex;
        }

        public boolean isValid() {
            return !((id == null) || (callingDate == null) || (status == null));
        }

        private List<String> errors = null;

        public List<String> getErrors() {
            if (null != errors) {
                return errors;
            }
            errors = new ArrayList<>();
            if (id == null) {
                errors.add("Error found at row " + (rowIndex + 1) + ", value for column \"" + StatusHeaderIndexes.ID + "\" not found.");
            }
            if (callingDate == null) {
                errors.add("Error found at row " + (rowIndex + 1) + ", value for column \"" + StatusHeaderIndexes.CALLING_DATE + "\" not found.");
            }
            if (status == null) {
                errors.add("Error found at row " + (rowIndex + 1) + ", value for column \"" + StatusHeaderIndexes.STATUS + "\" not found.");
            } else if (!KNOWN_STATUSES.contains(status)) {
                errors.add("Error found at row " + (rowIndex + 1) + ", value for column \"" + StatusHeaderIndexes.STATUS + "\" is not recognized [" + orgStatusValue + "].");
            }
            return errors;
        }

        @Override
        public String toString() {
            return (rowIndex + 1) + "," + id + ", " + callingDate + ", " + status + ", " + note;
        }

    }

}
