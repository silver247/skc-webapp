/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vbitx.jero.skc.voice.parser;

import com.vbitx.jero.skc.voice.parser.XlsxCsiRawDataReader;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vbitx.jero.api.company.CompanyRepository;
import com.vbitx.jero.skc.voice.CustomerVoiceRepository;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author gigadot
 */
@Service
public class CustomerVoiceImporterService {

    @Autowired
    private CustomerVoiceRepository customerVoiceRepository;

    @Autowired
    private CompanyRepository companyRepository;

    public Map<String, Object> importRawData(InputStream xlsxInputStream) {
        XlsxCsiRawDataReader xlsxCsiRawDataReader = new XlsxCsiRawDataReader(xlsxInputStream, customerVoiceRepository, companyRepository);
        return MapUtils.putAll(new HashMap<>(), ArrayUtils.toArray(
                "success", true
        ));
    }
}
