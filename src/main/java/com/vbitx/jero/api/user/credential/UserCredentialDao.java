package com.vbitx.jero.api.user.credential;

import com.vbitx.jero.api.user.User;

import org.springframework.data.repository.CrudRepository;

public interface UserCredentialDao extends CrudRepository<UserCredential, String> {

    public UserCredential findByUser(User user);


}
