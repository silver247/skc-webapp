package com.vbitx.jero.api.user;

import java.util.List;
import org.springframework.data.repository.CrudRepository;

public interface UserDao extends CrudRepository<User, String> {

    /**
     * Return the user having the passed email or null if no user is found.
     *
     * @param email the user email.
     * @return
     */
    public User findByEmail(String email);

    public User findByUsername(String username);

    public User findById(String id);
    
    public List<User> findAllByOrderByCreated();

}
