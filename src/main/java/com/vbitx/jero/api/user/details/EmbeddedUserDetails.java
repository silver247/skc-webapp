package com.vbitx.jero.api.user.details;

import com.vbitx.jero.api.authority.Authority;
import com.vbitx.jero.api.user.User;
import com.vbitx.jero.api.user.credential.UserCredential;
import java.util.ArrayList;
import java.util.List;
import org.springframework.security.core.GrantedAuthority;

import org.springframework.security.core.userdetails.UserDetails;

public class EmbeddedUserDetails implements UserDetails {

    private String userId;
    private String username;
    private String email;
    private String password;

    /* Spring Security related fields*/
    private List<GrantedAuthority> authorities;
    private boolean accountNonExpired = true;
    private boolean accountNonLocked = true;
    private boolean credentialsNonExpired = true;
    private boolean enabled = true;

    public EmbeddedUserDetails(User u, UserCredential uc, List<Authority> authorities) {
        userId = u.getId();
        username = u.getUsername();
        email = u.getEmail();
        password = uc.getHashedPassword();
        this.authorities = new ArrayList<>();
        for (Authority authority : authorities) {
            this.authorities.add(new GrantedAuthorityImpl(authority));
        }
    }

    public String getUserId() {
        return userId;
    }

    @Override
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public List<GrantedAuthority> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(List<GrantedAuthority> authorities) {
        this.authorities = authorities;
    }

    @Override
    public boolean isAccountNonExpired() {
        return accountNonExpired;
    }

    public void setAccountNonExpired(boolean accountNonExpired) {
        this.accountNonExpired = accountNonExpired;
    }

    @Override
    public boolean isAccountNonLocked() {
        return accountNonLocked;
    }

    public void setAccountNonLocked(boolean accountNonLocked) {
        this.accountNonLocked = accountNonLocked;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return credentialsNonExpired;
    }

    public void setCredentialsNonExpired(boolean credentialsNonExpired) {
        this.credentialsNonExpired = credentialsNonExpired;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    class GrantedAuthorityImpl implements GrantedAuthority {

        private String name;

        GrantedAuthorityImpl(Authority authority) {
            this.name = "ROLE_" + authority.getName().toUpperCase();
        }

        @Override
        public String getAuthority() {
            return name;
        }

    }

}
