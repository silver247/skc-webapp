/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vbitx.jero.api.user.rememberme;

import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author gigadot
 */
public interface UserRememberMeTokenDao extends CrudRepository<UserRememberMeToken, Long> {

    public UserRememberMeToken findByUsername(String username);

    public UserRememberMeToken findBySeries(String series);

    public UserRememberMeToken findByTokenValue(String tokenValue);
}
