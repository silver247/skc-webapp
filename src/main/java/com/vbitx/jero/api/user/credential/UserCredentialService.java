package com.vbitx.jero.api.user.credential;

import java.security.SecureRandom;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.stereotype.Service;

/**
 *
 * @author gigadot
 */
@Service
public class UserCredentialService {
    
    public String generateSalt() {
        // use secure random generator for harder prediction of the output
        return RandomStringUtils.random(128, 0, 0, true, true, null, new SecureRandom());
    }

    public String encryptPassword(String password, String salt) {
        return DigestUtils.sha512Hex(password + ":" + salt);
    }

}
