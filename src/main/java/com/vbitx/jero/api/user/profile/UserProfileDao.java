/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vbitx.jero.api.user.profile;

import com.vbitx.jero.api.user.User;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author gigadot
 */
public interface UserProfileDao extends CrudRepository<UserProfile, String> {

    public UserProfile findByUser(User user);

    public UserProfile findByUserUsername(String username);
}
