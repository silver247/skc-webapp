/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vbitx.jero.api.user.authority;

import com.vbitx.jero.api.user.User;
import java.util.List;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author gigadot
 */
public interface UserAuthorityDao extends CrudRepository<UserAuthority, String> {

    public List<UserAuthority> findByUser(User user);
    
    public List<UserAuthority> findByUserUsername(String username);
    
}
