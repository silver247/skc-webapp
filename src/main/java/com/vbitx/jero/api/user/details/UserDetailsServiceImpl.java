package com.vbitx.jero.api.user.details;

import com.vbitx.jero.api.authority.Authority;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import com.vbitx.jero.api.user.User;
import com.vbitx.jero.api.user.UserDao;
import com.vbitx.jero.api.user.authority.UserAuthority;
import com.vbitx.jero.api.user.authority.UserAuthorityDao;
import com.vbitx.jero.api.user.credential.UserCredential;
import com.vbitx.jero.api.user.credential.UserCredentialDao;
import java.util.ArrayList;
import java.util.List;
import org.springframework.security.core.userdetails.UserDetails;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserDao userDao;
    @Autowired
    private UserCredentialDao userCredentialDao;
    @Autowired
    private UserAuthorityDao userAuthorityDao;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User u = userDao.findByUsername(username);
        if (u == null) {
            throw new UsernameNotFoundException(username + " is not found in the database.");
        }
        UserCredential uc = userCredentialDao.findByUser(u);
        List<UserAuthority> userAuthorities = userAuthorityDao.findByUser(u);
        List<Authority> authorities = new ArrayList<>();
        for (UserAuthority userAuthority : userAuthorities) {
            authorities.add(userAuthority.getAuthority());
        }
        return new EmbeddedUserDetails(u, uc, authorities);
    }

}
