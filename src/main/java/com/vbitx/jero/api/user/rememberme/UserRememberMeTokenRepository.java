/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vbitx.jero.api.user.rememberme;

import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.web.authentication.rememberme.PersistentRememberMeToken;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author gigadot
 */
@Repository
public class UserRememberMeTokenRepository implements PersistentTokenRepository {

    @Autowired
    private UserRememberMeTokenDao rememberMeTokenDao;

    @Override
    public void createNewToken(PersistentRememberMeToken token) {
        UserRememberMeToken rememberMeToken = new UserRememberMeToken(token);
        rememberMeTokenDao.save(rememberMeToken);
    }

    @Override
    public void updateToken(String series, String tokenValue, Date lastUsed) {
        UserRememberMeToken rememberMeToken = rememberMeTokenDao.findBySeries(series);
        rememberMeToken.setTokenValue(tokenValue);
        rememberMeToken.setDate(lastUsed);
        rememberMeTokenDao.save(rememberMeToken);
    }

    @Override
    public PersistentRememberMeToken getTokenForSeries(String seriesId) {
        UserRememberMeToken rememberMeToken = rememberMeTokenDao.findBySeries(seriesId);
        if (rememberMeToken != null) {
            return new PersistentRememberMeToken(
                    rememberMeToken.getUsername(),
                    rememberMeToken.getSeries(),
                    rememberMeToken.getTokenValue(),
                    rememberMeToken.getDate()
            );
        } else {
            return null;
        }
    }

    @Override
    public void removeUserTokens(String username) {
        UserRememberMeToken rememberMeToken = rememberMeTokenDao.findByUsername(username);
        if (rememberMeToken != null) {
            rememberMeTokenDao.delete(rememberMeToken);
        }
    }
}
