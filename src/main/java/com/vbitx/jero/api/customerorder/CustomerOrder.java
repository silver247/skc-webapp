/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vbitx.jero.api.customerorder;

import com.vbitx.jero.api.company.Company;
import com.vbitx.jero.api.mechanic.Mechanic;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

/**
 *
 * @author gigadot
 */
@Entity
@EntityListeners(AuditingEntityListener.class)
public class CustomerOrder {

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid2")
    @Column(name = "id", unique = true, nullable = false, length = 36)
    private String id;

    private String customerName;

    private String documentCode;

    private int orderNo;

    private String informedCode;

    private String vehicleModelName;

    private int productCategory;

    private String vehicleBodyId;

    @Column(nullable = true)
    private String vehicleEngineId;

    private String name;

    private String customerPhoneNo;

    private String customerPhoneNo2;

    private String customerPhoneNo3;

    private int maintenanceNumber;

    @Column(nullable = false)
    private String status = "not_yet_interviewed";

    private boolean deleted = false;

    @Temporal(TemporalType.TIMESTAMP)
    private Date maintenanceDate;

    @CreatedDate
    @Temporal(TemporalType.TIMESTAMP)
    private Date created;

    @LastModifiedDate
    @Temporal(TemporalType.TIMESTAMP)
    private Date modified;

    @Temporal(TemporalType.TIMESTAMP)
    private Date serviceDate;

    @Temporal(TemporalType.DATE)
    private Date callingDate;
    
    @Column(length = 1024*1024)
    private String note;

    @ManyToOne(cascade = {CascadeType.REMOVE}, optional = false)
    @JoinColumn(name = "mechanic_id")
    private Mechanic mechanic;

    @CreatedBy
    private String createdBy;

    @LastModifiedBy
    private String modifiedBy;

    @ManyToOne
    @JoinColumn(name = "company_id")
    private Company company;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getModified() {
        return modified;
    }

    public void setModified(Date modified) {
        this.modified = modified;
    }

    public Date getServiceDate() {
        return serviceDate;
    }

    public void setServiceDate(Date serviceDate) {
        this.serviceDate = serviceDate;
    }

    public Date getCallingDate() {
        return callingDate;
    }

    public void setCallingDate(Date callingDate) {
        this.callingDate = callingDate;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Mechanic getMechanic() {
        return mechanic;
    }

    public void setMechanic(Mechanic mechanic) {
        this.mechanic = mechanic;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public String getDocumentCode() {
        return documentCode;
    }

    public void setDocumentCode(String documentCode) {
        this.documentCode = documentCode;
    }

    public int getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(int orderNo) {
        this.orderNo = orderNo;
    }

    public String getInformedCode() {
        return informedCode;
    }

    public void setInformedCode(String informedCode) {
        this.informedCode = informedCode;
    }

    public String getVehicleModelName() {
        return vehicleModelName;
    }

    public void setVehicleModelName(String vehicleModelName) {
        this.vehicleModelName = vehicleModelName;
    }

    public String getVehicleBodyId() {
        return vehicleBodyId;
    }

    public void setVehicleBodyId(String vehicleBodyId) {
        this.vehicleBodyId = vehicleBodyId;
    }

    public String getVehicleEngineId() {
        return vehicleEngineId;
    }

    public void setVehicleEngineId(String vehicleEngineId) {
        this.vehicleEngineId = vehicleEngineId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCustomerPhoneNo() {
        return customerPhoneNo;
    }

    public void setCustomerPhoneNo(String customerPhoneNo) {
        this.customerPhoneNo = customerPhoneNo;
    }

    public String getCustomerPhoneNo2() {
        return customerPhoneNo2;
    }

    public void setCustomerPhoneNo2(String customerPhoneNo2) {
        this.customerPhoneNo2 = customerPhoneNo2;
    }

    public String getCustomerPhoneNo3() {
        return customerPhoneNo3;
    }

    public void setCustomerPhoneNo3(String customerPhoneNo3) {
        this.customerPhoneNo3 = customerPhoneNo3;
    }

    public int getMaintenanceNumber() {
        return maintenanceNumber;
    }

    public void setMaintenanceNumber(int maintenanceNumber) {
        this.maintenanceNumber = maintenanceNumber;
    }

    public Date getMaintenanceDate() {
        return maintenanceDate;
    }

    public void setMaintenanceDate(Date maintenanceDate) {
        this.maintenanceDate = maintenanceDate;
    }

    public int getProductCategory() {
        return productCategory;
    }

    public void setProductCategory(int productCategory) {
        this.productCategory = productCategory;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
