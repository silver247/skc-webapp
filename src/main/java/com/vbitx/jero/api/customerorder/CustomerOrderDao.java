/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vbitx.jero.api.customerorder;

import com.vbitx.jero.api.company.Company;
import java.util.Date;
import java.util.List;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author gigadot
 */
public interface CustomerOrderDao extends CrudRepository<CustomerOrder, String> {

    public List<CustomerOrder> findAll();

    public List<CustomerOrder> findAllByOrderByCreated();

    public CustomerOrder findById(String id);

    public List<CustomerOrder> findByCompany(Company company);

    public List<CustomerOrder> findByCompanyIn(List<Company> companies);

    public List<CustomerOrder> findByServiceDateBetween(Date startDate, Date endDate);

    public List<CustomerOrder> findByCompanyInAndServiceDateBetween(List<Company> companies, Date startDate, Date endDate);

    public List<CustomerOrder> findByCompanyAndDeletedFalse(Company company);

    public List<CustomerOrder> findByCompanyInAndServiceDateBetweenAndDeletedFalse(List<Company> companies, Date cstartDate, Date cendDate);
    
    public List<CustomerOrder> findByCompanyInAndMaintenanceDateBetweenAndDeletedFalse(List<Company> companies, Date cstartDate, Date cendDate);

    public List<CustomerOrder> findByCompanyInAndDeletedFalse(List<Company> companies);

    public List<CustomerOrder> findByServiceDateBetweenAndDeletedFalse(Date cstartDate, Date cendDate);
    
    public List<CustomerOrder> findByMaintenanceDateBetweenAndDeletedFalse(Date cstartDate, Date cendDate);

    public List<CustomerOrder> findByDeleted(boolean b);

    public Long countByServiceDate(Date date);

    public List<CustomerOrder> findByCompanyInAndCreatedBetween(List<Company> companies, Date startDate, Date endDate);

    public List<CustomerOrder> findByCompanyInAndCreatedBetweenAndDeletedFalse(List<Company> companies, Date cstartDate, Date cendDate);

    public List<CustomerOrder> findByCreatedBetweenAndDeletedFalse(Date cstartDate, Date cendDate);
}
