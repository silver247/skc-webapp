/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vbitx.jero.api.auditor;

import com.vbitx.jero.api.user.User;
import com.vbitx.jero.api.user.UserDao;
import com.vbitx.jero.api.user.details.EmbeddedUserDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

/**
 *
 * @author gigadot
 */
@Component
public class UserAuditorAware implements AuditorAware<String> {

    @Override
    public String getCurrentAuditor() {
        try {
            Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            EmbeddedUserDetails ep = (EmbeddedUserDetails) principal;
            return ep.getUserId();
            //return auditor.getId();
//            User user = new User();
//            user.setId(auditor.getId());
//            System.out.println(auditor.getUsername());
//            return user;
        } catch (NullPointerException npe) {
        } catch (ClassCastException cce) {
        }
        return null;
    }

}
