/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vbitx.jero.api.locale;

import com.vbitx.jero.api.user.profile.UserProfile;
import com.vbitx.jero.api.user.profile.UserProfileDao;
import java.security.Principal;
import java.util.Locale;
import java.util.TimeZone;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.context.i18n.LocaleContext;
import org.springframework.context.i18n.TimeZoneAwareLocaleContext;
import org.springframework.web.servlet.i18n.AbstractLocaleContextResolver;
import org.springframework.web.util.WebUtils;

/**
 *
 * @author gigadot
 */
public class DatabaseSessionLocaleResolver extends AbstractLocaleContextResolver {

    private UserProfileDao userProfileDao;

    public static final String LOCALE_SESSION_ATTRIBUTE_NAME = DatabaseSessionLocaleResolver.class.getName() + ".LOCALE";

    public static final String TIME_ZONE_SESSION_ATTRIBUTE_NAME = DatabaseSessionLocaleResolver.class.getName() + ".TIME_ZONE";

    public void setUserProfileDao(UserProfileDao userProfileDao) {
        this.userProfileDao = userProfileDao;
    }

    @Override
    public Locale resolveLocale(HttpServletRequest request) {
        // try getting from user preference
        Locale locale = getUserLocale(request);
        if (locale != null) {
            return locale;
        }
        // else get from session or then default value
        Locale sessionLocale = (Locale) WebUtils.getSessionAttribute(request, LOCALE_SESSION_ATTRIBUTE_NAME);
        if (sessionLocale == null) {
            sessionLocale = determineDefaultLocale(request);
        }
        return sessionLocale;
    }

    @Override
    public LocaleContext resolveLocaleContext(final HttpServletRequest request) {
        return new TimeZoneAwareLocaleContext() {
            @Override
            public Locale getLocale() {
                // try getting from user preference
                Locale locale = getUserLocale(request);
                if (locale != null) {
                    return locale;
                }
                // else get from session or then default value
                Locale sessionLocale = (Locale) WebUtils.getSessionAttribute(request, LOCALE_SESSION_ATTRIBUTE_NAME);
                if (sessionLocale == null) {
                    sessionLocale = determineDefaultLocale(request);
                }
                return sessionLocale;
            }

            @Override
            public TimeZone getTimeZone() {
                TimeZone timeZone = (TimeZone) WebUtils.getSessionAttribute(request, TIME_ZONE_SESSION_ATTRIBUTE_NAME);
                if (timeZone == null) {
                    timeZone = determineDefaultTimeZone(request);
                }
                return timeZone;
            }
        };
    }

    @Override
    public void setLocaleContext(HttpServletRequest request, HttpServletResponse response, LocaleContext localeContext) {
        Locale locale = null;
        TimeZone timeZone = null;
        if (localeContext != null) {
            locale = localeContext.getLocale();
            if (localeContext instanceof TimeZoneAwareLocaleContext) {
                timeZone = ((TimeZoneAwareLocaleContext) localeContext).getTimeZone();
            }
        }
        WebUtils.setSessionAttribute(request, LOCALE_SESSION_ATTRIBUTE_NAME, locale);
        WebUtils.setSessionAttribute(request, TIME_ZONE_SESSION_ATTRIBUTE_NAME, timeZone);
        // save locale to database
        saveUserLocale(request, locale);
    }

    protected Locale determineDefaultLocale(HttpServletRequest request) {
        Locale defaultLocale = getDefaultLocale();
        if (defaultLocale == null) {
            defaultLocale = request.getLocale();
        }
        return defaultLocale;
    }

    protected TimeZone determineDefaultTimeZone(HttpServletRequest request) {
        return getDefaultTimeZone();
    }

    protected void saveUserLocale(HttpServletRequest request, Locale locale) {
        Principal principal = request.getUserPrincipal();
        if (principal != null) {
            UserProfile userProfile = userProfileDao.findByUserUsername(principal.getName());
            if (userProfile != null) {
                userProfile.setLocale(locale.getLanguage());
                userProfileDao.save(userProfile);
            }
        }
    }

    protected Locale getUserLocale(HttpServletRequest request) {
        Principal principal = request.getUserPrincipal();
        if (principal != null) {
            UserProfile userProfile = userProfileDao.findByUserUsername(principal.getName());
            if (userProfile != null) {
                return SupportingLocale.getLocale(userProfile.getLocale());
            }
        }
        return null;
    }

}
