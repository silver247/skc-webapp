/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vbitx.jero.api.locale;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 *
 * @author gigadot
 */
public class SupportingLocale {

    public static final Locale EN = Locale.ENGLISH;
    public static final Locale TH = new Locale("th");
    public static final Locale KM = new Locale("km");
    public static final Locale LO = new Locale("lo");
    
    private static final Map<String, Locale> LOCALE_MAP = new HashMap<>();
    

    static {
        LOCALE_MAP.put("en", EN);
        LOCALE_MAP.put("th", TH);
        LOCALE_MAP.put("km", KM);
        LOCALE_MAP.put("lo", LO);
    }

    public static Locale getLocale(String locale) {
        return LOCALE_MAP.get(locale);
    }

}
