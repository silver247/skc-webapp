/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vbitx.jero.api.authority;

import java.util.List;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author gigadot
 */
public interface AuthorityDao extends CrudRepository<Authority, String> {

    public Authority findByNameIgnoreCase(String name);
    @Override
    public List<Authority> findAll();

    public Authority findById(String authorityId);
}
