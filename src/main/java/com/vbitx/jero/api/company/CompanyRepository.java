package com.vbitx.jero.api.company;

import com.vbitx.jero.api.user.User;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CompanyRepository extends CrudRepository<Company, String> {

    /**
     * Return the company having the passed name or null if no company is found.
     *
     * @param name the company name.
     * @return
     */
    public Company findByName(String name);

    public Company findById(String id);
    
    public List<Company> findByIdIn(List<String> ids);
    
//    public List<Company> findByUsers(Collection<User> users);
    
    public Company findByUsers(User user);
    
    public List<Company> findByUsersUsername(String username);

    public List<Company> findAllByOrderByCreated();

    public List<Company> findAllByOrderByName();

    public Company findByDisplayId(String displayId);

}
