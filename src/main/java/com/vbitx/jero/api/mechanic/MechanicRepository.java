package com.vbitx.jero.api.mechanic;

import com.vbitx.jero.api.company.Company;
import java.util.List;
import org.springframework.data.repository.CrudRepository;

public interface MechanicRepository extends CrudRepository<Mechanic, String> {

    /**
     * Return the mechanic having the passed name or null if no mechanic is
     * found.
     *
     * @param name the mechanic name.
     * @return
     */
    public List<Mechanic> findAll();

    public Mechanic findByName(String name);

    public Mechanic findById(String id);

    public List<Mechanic> findByCompany(Company company);

    public List<Mechanic> findByCompanyAndActive(Company company, Boolean active);

    public List<Mechanic> findAllByOrderByCreated();

}
