/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vbitx.jero.api.activity.aware.impl;

import com.vbitx.jero.api.activity.Activity;
import com.vbitx.jero.api.activity.aware.ActivityAware;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 *
 * @author gigadot
 */
@Component
public class ActivityHttpAware implements ActivityAware {

    private static String getFullURL(HttpServletRequest request) {
        StringBuffer requestURL = request.getRequestURL();
        String queryString = request.getQueryString();
        if (queryString == null) {
            return requestURL.toString();
        } else {
            return requestURL.append('?').append(queryString).toString();
        }
    }

    @Override
    public void touchForCreate(Activity activity) {
        try {
            HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
            activity.setIp(request.getRemoteAddr());
            activity.setReferer(request.getHeader("referer"));
            activity.setUri(getFullURL(request));
            activity.setUseragent(request.getHeader("User-Agent"));
        } catch (Exception ex) {

        }
    }

}
