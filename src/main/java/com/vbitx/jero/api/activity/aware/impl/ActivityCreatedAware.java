/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vbitx.jero.api.activity.aware.impl;

import com.vbitx.jero.api.activity.Activity;
import com.vbitx.jero.api.activity.aware.ActivityAware;
import java.util.Date;
import org.springframework.stereotype.Component;

/**
 * There is no replacement for this class. We cannot annotate activity.createdBy
 * with \@CreatedBy because we want the id to be set before the entity is
 * persisted.
 *
 * @author gigadot
 */
@Component
public class ActivityCreatedAware implements ActivityAware {

    @Override
    public void touchForCreate(Activity activity) {
        activity.setCreated(new Date());
    }

}
