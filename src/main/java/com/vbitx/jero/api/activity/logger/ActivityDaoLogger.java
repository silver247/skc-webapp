/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vbitx.jero.api.activity.logger;

import com.vbitx.jero.api.activity.Activity;
import com.vbitx.jero.api.activity.ActivityDao;
import java.util.concurrent.Executor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Currently, cannot rollback if the main transaction fails because this logger
 * use independent transaction inside a separate thread to log to database.
 *
 * The reason for using separate thread is that persisting activity object
 * cannot be done in pre/post transaction events of another entity.
 *
 * @author gigadot
 */
@Component
public class ActivityDaoLogger implements ActivityLogger {

    @Autowired
    private Executor executor;
    @Autowired
    private ActivityDao activityDao;

    @Override
    public void log(Activity activity) {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                if (activity.getJson().length() > Activity.JSON_MAX_LENGTH) {
                    activity.setJson(activity.getJson().substring(0, Activity.JSON_MAX_LENGTH));
                }
                activityDao.save(activity);
            }
        });
    }

}
