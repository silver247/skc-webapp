/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vbitx.jero.api.activity.logger;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vbitx.jero.api.activity.Activity;
import java.util.concurrent.Executor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author gigadot
 */
@Component
public class ActivityConsoleLogger implements ActivityLogger {

    @Autowired
    private Executor executor;

    @Override
    public void log(Activity activity) {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                ObjectMapper mapper = new ObjectMapper();
                try {
                    String jsonInString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(activity);
                    System.out.println(jsonInString);
                } catch (JsonProcessingException ex) {
                }
            }
        });

    }

}
