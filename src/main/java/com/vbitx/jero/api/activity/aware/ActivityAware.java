/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vbitx.jero.api.activity.aware;

import com.vbitx.jero.api.activity.Activity;

/**
 *
 * @author gigadot
 */
public interface ActivityAware {

    public void touchForCreate(Activity activity);
}
