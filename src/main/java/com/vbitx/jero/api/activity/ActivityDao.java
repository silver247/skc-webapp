/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vbitx.jero.api.activity;

import java.util.Date;
import java.util.List;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author gigadot
 */
public interface ActivityDao extends CrudRepository<Activity, String> {

    public List<Activity> findAllByOrderByCreated();
    
    public List<Activity> findByActionAndCreatedGreaterThanEqualOrderByCreatedDesc(String action, Date fromDate);

}
