/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vbitx.jero.api.activity.listener;

import com.vbitx.jero.api.activity.ActivityService;
import com.vbitx.jero.api.context.ApplicationContextUtils;
import javax.persistence.PostPersist;
import javax.persistence.PostRemove;
import javax.persistence.PostUpdate;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.PreUpdate;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author gigadot
 */
public class EntityActivityListener {

    @Autowired
    private ActivityService activityService = null;

    protected void autowireActivityAwares() {
        if (activityService == null) {
            ApplicationContextUtils.getApplicationContext().getAutowireCapableBeanFactory().autowireBean(this);
        }
    }

    @PrePersist
    @PreUpdate
    @PreRemove
    protected void init(Object target) {
        autowireActivityAwares();
    }

    @PostPersist
    public void onCreated(Object target) {
        activityService.logEntity(null, "insert", target);
    }

    @PostUpdate
    public void onUpdated(Object target) {
        activityService.logEntity(null, "update", target);
    }

    @PostRemove
    public void onDeleled(Object target) {
        activityService.logEntity(null, "delete", target);
    }

}
