/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vbitx.jero.api.activity;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vbitx.jero.api.activity.aware.ActivityAware;
import com.vbitx.jero.api.activity.logger.ActivityLogger;
import java.lang.reflect.Field;
import java.util.Collection;
import javax.persistence.Id;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author gigadot
 */
@Service
public class ActivityService {

    @Autowired
    private Collection<ActivityLogger> activityLoggers;
    @Autowired
    private Collection<ActivityAware> activityAwares;

    private void autoaware(Activity activity) {
        for (ActivityAware activityAware : activityAwares) {
            activityAware.touchForCreate(activity);
        }
    }

    private Activity createActivity(String note, String action, String namespace, String objectId, String json) {
        Activity activity = new Activity(note, action, namespace, objectId, json);
        autoaware(activity);
        return activity;
    }

    private String getObjectId(Object target) {
        try {
            Field[] fields = target.getClass().getDeclaredFields();
            for (Field field : fields) {
                if (field.isAnnotationPresent(Id.class)) {
                    field.setAccessible(true);
                    return (String) field.get(target);
                }
            }
        } catch (Exception ex) {
        }
        return null;
    }

    private String json(Object object) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.writeValueAsString(object);
        } catch (JsonProcessingException ex) {
        }
        return null;
    }

    /**
     * log with note
     * @param note 
     */
    public void log(String note) {
        log(note, null, null, null, null);
    }

    /**
     * log with note and action
     * @param note
     * @param action can be one of insert, select, update, delete
     */
    public void log(String note, String action) {
        log(note, action, null, null, null);
    }

    /**
     * log with note, action, namespace, objectId and json. you probably won't need
     * to json because it can be very large.
     * 
     * @param note note
     * @param action can be one of insert, select, update, delete
     * @param namespace canonical name of the entity
     * @param objectId object id
     * @param json serialized object in json format
     */
    public void log(String note, String action, String namespace, String objectId, String json) {
        Activity activity = createActivity(note, action, namespace, objectId, json);
        for (ActivityLogger activityLogger : activityLoggers) {
            activityLogger.log(activity);
        }
    }

    /**
     * same as log(String note, String action, String namespace, String objectId, String json)
     * but auto determine namespace, objectId and json from target object.
     * 
     * Use EntityActivityListener instead. Example:
     * 
     * \@EntityListeners({AuditingEntityListener.class, EntityActivityListener.class})
     * public class Company {
     *    ...
     * }
     * 
     * Add EntityActivityListener.class to EntityListeners annotation at the top of
     * the entity declaration.
     * 
     * DO NOT USE THIS UNLESS IT IS NECCESARY, e.g. logging user sign in because
     * EntityActivityListener only track create, update and remove but not select.
     * 
     * @param note note
     * @param action can be one of insert, select, update, delete
     * @param target targeting object to be logged
     */
    public void logEntity(String note, String action, Object target) {
        log(note, action, target.getClass().getCanonicalName(), getObjectId(target), json(target));
    }

}
