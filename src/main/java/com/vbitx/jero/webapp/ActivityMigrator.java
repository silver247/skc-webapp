/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vbitx.jero.webapp;

import com.vbitx.jero.api.activity.Activity;
import com.vbitx.jero.api.activity.ActivityDao;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author gigadot
 */
@Component
public class ActivityMigrator {
    private static final Logger logger = LoggerFactory.getLogger(ActivityMigrator.class);
    @Autowired
    private ActivityDao activityDao;
    
    public void numberingLongId() {
        List<Activity> activities = activityDao.findAllByOrderByCreated();
        for (int i = 0; i < activities.size(); i++) {
            Activity activity = activities.get(i);
            activity.setLongId((long) i + 1l);
            activityDao.save(activity);
        }
        logger.info("Done renumbering activity long id.");
    }
}
