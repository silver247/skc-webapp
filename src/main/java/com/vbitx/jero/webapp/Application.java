package com.vbitx.jero.webapp;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import org.hibernate.tool.schema.spi.SchemaManagementException;
import org.springframework.boot.ExitCodeGenerator;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextException;

@SpringBootApplication
public class Application {

    public static int attemptSchemaValidation(String[] args) {
        List<String> argList = new ArrayList<>(Arrays.asList(args));
        for (Iterator<String> it = argList.iterator(); it.hasNext();) {
            String arg = it.next();
            if (arg.contains("--spring.jpa.hibernate.ddl-auto=")) {
                it.remove();
            }
        }
        argList.add("--spring.jpa.hibernate.ddl-auto=validate");
        String[] moreArgs = argList.toArray(new String[argList.size()]);
        try {
            ApplicationContext ctx = SpringApplication.run(Application.class, moreArgs);
            return SpringApplication.exit(ctx, new ExitCodeGenerator() {
                @Override
                public int getExitCode() {
                    return 0;
                }
            });
        } catch (ApplicationContextException ex) {
            if (ex.getRootCause() instanceof SchemaManagementException) {
                System.out.println(">>> " + ex.getRootCause());
            }
            return 1;
        }
    }

    public static void attemptSchemaUpdate() {

    }

    public static void backupDatabase() {

    }

    public static void restoreDatabase() {

    }

    public static void main(String[] args) {
        if (false) {
            attemptSchemaValidation(args);
        }

        ApplicationContext ctx = SpringApplication.run(Application.class, args);
        ApplicationInitializer appInit = ctx.getBean(ApplicationInitializer.class);
        appInit.populateDatabase();

        ActivityMigrator activityMigrator = ctx.getBean(ActivityMigrator.class);
        activityMigrator.numberingLongId();
    }
}
