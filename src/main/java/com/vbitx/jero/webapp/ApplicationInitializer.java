/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vbitx.jero.webapp;

import com.vbitx.jero.api.authority.Authority;
import com.vbitx.jero.api.authority.AuthorityDao;
import com.vbitx.jero.api.company.Company;
import com.vbitx.jero.api.user.User;
import com.vbitx.jero.api.user.UserDao;
import com.vbitx.jero.api.user.authority.UserAuthority;
import com.vbitx.jero.api.user.authority.UserAuthorityDao;
import com.vbitx.jero.api.user.credential.UserCredential;
import com.vbitx.jero.api.user.credential.UserCredentialDao;
import com.vbitx.jero.api.user.credential.UserCredentialService;
import com.vbitx.jero.api.user.profile.UserProfile;
import com.vbitx.jero.api.user.profile.UserProfileDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import com.vbitx.jero.api.company.CompanyRepository;
import com.vbitx.jero.api.mechanic.MechanicRepository;

/**
 *
 * @author gigadot
 */
@Component
public class ApplicationInitializer {

    @Autowired
    private UserDao userDao;
    @Autowired
    private UserCredentialDao userCredentialDao;
    @Autowired
    private UserProfileDao userProfileDao;
    @Autowired
    private UserCredentialService userCredentialService;
    @Autowired
    private AuthorityDao authorityDao;
    @Autowired
    private UserAuthorityDao userAuthorityDao;
    @Autowired
    private CompanyRepository companyDao;
    @Autowired
    private MechanicRepository mechanicDao;

    private void addMockNewUser(String username, String firstname, String lastname, String role, Company company) {
        User user = new User(username, username + "@vbitx.com");
        userDao.save(user);

        UserCredential ucredential = new UserCredential();
        ucredential.setUser(user);
        String salt = userCredentialService.generateSalt();
        ucredential.setSalt(salt);
        ucredential.setHashedPassword(userCredentialService.encryptPassword("123456", salt));
        userCredentialDao.save(ucredential);

        UserProfile uprofile = new UserProfile();
        uprofile.setFirstname(firstname);
        uprofile.setLastname(lastname);
        uprofile.setUser(user);
        userProfileDao.save(uprofile);

        company.getUsers().add(user);

        UserAuthority adminAuthority = new UserAuthority(user, authorityDao.findByNameIgnoreCase(role));
        userAuthorityDao.save(adminAuthority);
    }

    private Company createCompany(String name) {
        Company company = new Company();
        company.setName(name);
        companyDao.save(company);
//        Mechanic mechanic = new Mechanic();
//        mechanic.setName("Jason");
//        mechanic.setCompany(company);
//        mechanicDao.save(mechanic);
        return company;
    }

    @Transactional
    public void populateDatabase() {
        if (authorityDao.count() == 0) {
            authorityDao.save(new Authority("admin"));
            authorityDao.save(new Authority("user"));
            authorityDao.save(new Authority("executive"));
        }
        if (userDao.count() == 0) {
            Company ablCompany = createCompany("ABL");
//            Company enterpriseDealerCompany = createCompany("Enterprise Dealer");
//            Company localDealerCompany = createCompany("Local Dealer");
            Company kutotaCompany = createCompany("Kubota");

            addMockNewUser("admin", "ABL", "Admin", "admin", ablCompany);
//            addMockNewUser("uber", "Uber", "Kent", "user", enterpriseDealerCompany);
//            addMockNewUser("uno", "Uno", "Kent", "user", enterpriseDealerCompany);
//            addMockNewUser("ubuntu", "Ubuntu", "Kent", "user", localDealerCompany);
//            addMockNewUser("exon", "Exon", "Kent", "executive", kutotaCompany);
        }
        if (userDao.findByUsername("super") == null) {
            Company vBitCompany = createCompany("vBit");
            addMockNewUser("super", "Super", "vBit", "admin", vBitCompany);
        }
//        List<Company> companies = companyDao.findAll();
//        for (Company company : companies) {
//            company.getUsers();
//        }
    }

}
