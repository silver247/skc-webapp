/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vbitx.jero.webapp.controller;

import com.vbitx.jero.api.company.Company;
import com.vbitx.jero.api.mechanic.Mechanic;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import com.vbitx.jero.api.company.CompanyRepository;
import com.vbitx.jero.api.mechanic.MechanicRepository;

/**
 *
 * @author gigadot
 */
@Controller
@RequestMapping("/company")
public class CompanyController {

    @Autowired
    private CompanyRepository companyDao;

    @Autowired
    private MechanicRepository mechanicDao;

    @RequestMapping(value = {"", "/", "/list"}, method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
    public String list(ModelMap model, HttpServletRequest request, Principal principal) {
        List<Company> companies = new ArrayList<>();
        if (request.isUserInRole("ROLE_USER")) {
            String username = principal.getName();
            List<Company> cs = companyDao.findByUsersUsername(username);
            if (cs != null) {
                companies.addAll(cs);
            }
        } else if (request.isUserInRole("ROLE_ADMIN")) {
            companyDao.findAll().iterator().forEachRemaining(companies::add);
        }
        model.addAttribute("mainMenu", "company");
        model.addAttribute("pageHeaderBig", "Company");
        model.addAttribute("pageHeaderSmall", "list all companies");
        model.addAttribute("companies", companies);
        return "app/company/list";
    }

    @RequestMapping(value = "/new", method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public String addNew(ModelMap model) {
        model.addAttribute("mainMenu", "company");
        model.addAttribute("pageHeaderBig", "Company");
        model.addAttribute("pageHeaderSmall", "new company");
        return "app/company/new";
    }

    @RequestMapping(value = "/{id}/edit", method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public String edit(ModelMap model, @PathVariable String id) {
        Company company = companyDao.findById(id);
        if (company == null) {
            return "redirect:/company";
        }
        model.addAttribute("mainMenu", "company");
        model.addAttribute("pageHeaderBig", company.getName());
        model.addAttribute("pageHeaderSmall", "edit company");

        model.addAttribute("company", company);
        return "app/company/edit";
    }

    @RequestMapping(value = {"/{id}/edit"}, method = RequestMethod.POST)
    @Transactional
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public String update(@PathVariable String id, String name, String displayId) {
        Company company = companyDao.findById(id);
        company.setName(name);
        company.setDisplayId(displayId);
        companyDao.save(company);
        return "redirect:/company/" + id;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
    public String viewId(@PathVariable String id, ModelMap model) {
        Company company = companyDao.findById(id);
        if (company == null) {
            return "redirect:/company";
        }
        model.addAttribute("mainMenu", "company");
        model.addAttribute("pageHeaderBig", company.getName());
        model.addAttribute("pageHeaderSmall", "view company");

        List<Mechanic> mechanics = mechanicDao.findByCompany(company);
        model.addAttribute("company", company);
        model.addAttribute("mechanics", mechanics);
        return "app/company/show";
    }

    @RequestMapping(value = {"/create"}, method = RequestMethod.POST)
    @Transactional
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public String create(String name, String displayId) {
        Company company = new Company();
        company.setName(name);
        company.setDisplayId(displayId);
        companyDao.save(company);
        return "redirect:/company";
    }

    @RequestMapping(value = {"/{id}/mechanic/create"}, method = RequestMethod.POST)
    @Transactional
    @ResponseBody
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
    public String createMechanic(@PathVariable String id, String name, Principal principal) {
        Company company = companyDao.findById(id);
        Mechanic mechanic = new Mechanic();
        mechanic.setActive(true);
        mechanic.setCompany(company);
        mechanic.setName(name);
        mechanicDao.save(mechanic);
        return "success";
    }

}
