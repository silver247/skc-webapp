package com.vbitx.jero.webapp.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vbitx.jero.api.activity.ActivityDao;
import com.vbitx.jero.api.authority.AuthorityDao;
import com.vbitx.jero.api.customerorder.CustomerOrderDao;
import com.vbitx.jero.api.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import com.vbitx.jero.api.user.UserDao;
import com.vbitx.jero.api.user.authority.UserAuthority;
import com.vbitx.jero.api.user.authority.UserAuthorityDao;
import com.vbitx.jero.api.user.credential.UserCredential;
import com.vbitx.jero.api.user.credential.UserCredentialDao;
import com.vbitx.jero.api.user.profile.UserProfile;
import com.vbitx.jero.api.user.profile.UserProfileDao;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RestController;
import com.vbitx.jero.api.company.CompanyRepository;
import com.vbitx.jero.api.mechanic.MechanicRepository;

@RestController
@RequestMapping(value = {"/export"})
public class ExportController {

    @Autowired
    private UserDao userDao;

    @Autowired
    private UserProfileDao userProfileDao;

    @Autowired
    private UserCredentialDao userCredentialDao;

    @Autowired
    private CompanyRepository companyDao;

    @Autowired
    private MechanicRepository mechanicDao;

    @Autowired
    private UserAuthorityDao userAuthorityDao;

    @Autowired
    private AuthorityDao authorityDao;

    @Autowired
    private ActivityDao activityDao;

    @Autowired
    private CustomerOrderDao customerOrderDao;

    @RequestMapping("/users")
    @Transactional(readOnly = true)
    public List users() {
        //Map<String, Object> users = new HashMap<>();
        List<Map> users = new ArrayList<>();

        ObjectMapper m = new ObjectMapper();
        for (User u : userDao.findAllByOrderByCreated()) {

            Map user = m.convertValue(u, Map.class);

            List<UserAuthority> authoritys = userAuthorityDao.findByUser(u);
            List<String> au = new ArrayList<>();
            for (UserAuthority authority : authoritys) {
                au.add(authority.getAuthority().getName());
            }
            user.put("roles", au);

            UserCredential uc = userCredentialDao.findByUser(u);
            Map c = m.convertValue(uc, Map.class);
            c.remove("user");
            user.put("credential", c);

            UserProfile up = userProfileDao.findByUser(u);
            Map p = m.convertValue(up, Map.class);
            p.remove("user");
            user.put("profile", p);

            users.add(user);
        }
        return users;
    }

    @RequestMapping("/companies")
    @Transactional(readOnly = true)
    public List companies() {
        return companyDao.findAllByOrderByCreated();
    }

    @RequestMapping("/mechanics")
    @Transactional(readOnly = true)
    public List mechanics() {
        return mechanicDao.findAllByOrderByCreated();
    }

    @RequestMapping("/activities")
    @Transactional(readOnly = true)
    public List activities() {
        return activityDao.findAllByOrderByCreated();
    }

    @RequestMapping("/orders")
    @Transactional(readOnly = true)
    public List orders() {
        return customerOrderDao.findAllByOrderByCreated();
    }

    @RequestMapping("/all")
    @Transactional(readOnly = true)
    public Map all() {
        Map<String, Object> mmm = new HashMap<>();
        mmm.put("users", users());
        mmm.put("companies", companies());
        mmm.put("mechanics", mechanics());
        mmm.put("orders", orders());
        mmm.put("activities", activities());
        return mmm;
    }

}
