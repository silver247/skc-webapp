/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vbitx.jero.webapp.controller;

import java.util.Collection;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author gigadot
 */
@Controller
@RequestMapping("/acl")
public class AclController {

    @RequestMapping(value = {"", "/", "/list"}, method = RequestMethod.GET)
    public String list(ModelMap model) {
        model.addAttribute("mainMenu", "acl");
        model.addAttribute("pageHeaderBig", "ACL");
        model.addAttribute("pageHeaderSmall", "lab");
        Collection<? extends GrantedAuthority> authorities = SecurityContextHolder.getContext().getAuthentication().getAuthorities();
        for (GrantedAuthority authority : authorities) {
            System.out.println(authority.getAuthority());
        }
        return "app/acl/list-items";
    }

}
