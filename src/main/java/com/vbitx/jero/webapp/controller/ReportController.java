/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vbitx.jero.webapp.controller;

import com.vbitx.jero.api.activity.ActivityService;
import com.vbitx.jero.api.company.Company;
import com.vbitx.jero.api.customerorder.CustomerOrder;
import com.vbitx.jero.api.customerorder.CustomerOrderDao;
import com.vbitx.jero.api.mechanic.Mechanic;
import com.vbitx.jero.api.user.User;
import com.vbitx.jero.api.user.UserDao;
import com.vbitx.jero.skc.voice.parser.XLSXOrderStatusReader;
import java.io.IOException;
import java.io.InputStream;
import java.security.Principal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.collections4.IteratorUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import com.vbitx.jero.api.company.CompanyRepository;
import com.vbitx.jero.api.mechanic.MechanicRepository;
import com.vbitx.jero.skc.voice.parser.CustomerVoiceImporterService;

/**
 *
 * @author sun
 */
@Controller
@RequestMapping("/report")
public class ReportController {

    @Autowired
    private CustomerOrderDao customerOrderDao;

    @Autowired
    private MechanicRepository mechanicDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    private CompanyRepository companyDao;

    @Autowired
    private ActivityService activityService;

    @RequestMapping(value = {"/update_status"}, method = RequestMethod.POST)
    @Transactional
    @ResponseBody
    public ResponseEntity updateStatus(String pk, String value) {
        Map<String, Object> responseMap = new HashMap<>();
        CustomerOrder customerOrder = customerOrderDao.findById(pk);
        customerOrder.setStatus(value);
        customerOrderDao.save(customerOrder);
        responseMap.put("success", true);

        return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.OK);
    }

    @RequestMapping(value = {"", "/", "/list"}, method = RequestMethod.GET)
    public String list(Principal principal, HttpServletRequest request, ModelMap model,
        String[] companiesId,
        @RequestParam(required = false, value = "startDate") @DateTimeFormat(pattern = "yyyy-MM-dd") Date startDate,
        @RequestParam(required = false, value = "endDate") @DateTimeFormat(pattern = "yyyy-MM-dd") Date endDate,
        @RequestParam(required = false, value = "dateType", defaultValue = "") String dateType) throws ParseException {

        model.addAttribute("mainMenu", "report");
        model.addAttribute("pageHeaderBig", "Report");
        model.addAttribute("pageHeaderSmall", "list all order");

        model.addAttribute("showDownload", (request.isUserInRole("ROLE_ADMIN") || request.isUserInRole("ROLE_EXECUTIVE") || request.isUserInRole("ROLE_USER")));

        List<Company> companies = new ArrayList<>();
//      Filter
        if (request.isUserInRole("ROLE_USER")) {
            String username = principal.getName();
            List<Company> cs = companyDao.findByUsersUsername(username);
            if (cs != null) {
                companies.addAll(cs);
            }
//            reports = customerOrderDao.findByCompanyAndDeleted(company, false);
        } else {
            List<Company> cCompanies = new ArrayList<>();
            companyDao.findAll().iterator().forEachRemaining(cCompanies::add);
            model.addAttribute("companies", cCompanies);
            if (companiesId != null) {
                List<String> companyList = Arrays.asList(companiesId);
                companies = companyDao.findByIdIn(companyList);
            }
        }

        endDate = DateUtils.ceiling((endDate == null) ? new Date() : endDate, Calendar.DATE);
        startDate = DateUtils.truncate((startDate == null) ? DateUtils.addMonths(endDate, -1) : startDate, Calendar.DATE);
        startDate = (startDate.compareTo(endDate) < 0) ? startDate : DateUtils.addMonths(endDate, -1);
        endDate = DateUtils.addSeconds(endDate, -1);

        List<CustomerOrder> reports = new ArrayList<CustomerOrder>();
        if (companies.isEmpty()) {
            if (dateType.equalsIgnoreCase("createdDate")) {
                reports = customerOrderDao.findByCreatedBetweenAndDeletedFalse(startDate, endDate);
            } else {
                reports = customerOrderDao.findByMaintenanceDateBetweenAndDeletedFalse(startDate, endDate);
            }
        } else {
            // 
            if (dateType.equalsIgnoreCase("createdDate")) {
                reports = customerOrderDao.findByCompanyInAndCreatedBetweenAndDeletedFalse(companies, startDate, endDate);
            } else {
                reports = customerOrderDao.findByCompanyInAndMaintenanceDateBetweenAndDeletedFalse(companies, startDate, endDate);
            }
        }

        model.addAttribute("reports", reports);

        return "app/report/list";
    }

    @RequestMapping("/order/{id}")
    @ResponseBody
    public Map id(@PathVariable String id) {
        CustomerOrder order = customerOrderDao.findOne(id);
        return MapUtils.putAll(new HashMap(), new Object[]{
            "success", order != null,
            "order", order
        });
    }

    @RequestMapping(value = "/new", method = RequestMethod.GET)
    public String addNew(ModelMap model, Principal principal) {
        model.addAttribute("mainMenu", "report");
        model.addAttribute("pageHeaderBig", "Report");
        model.addAttribute("pageHeaderSmall", "new report");
        String username = principal.getName();
        User user = userDao.findByUsername(username);
        Company company = companyDao.findByUsers(user);
        List<Mechanic> mechanics = mechanicDao.findByCompanyAndActive(company, true);
        model.addAttribute("company", company);
        model.addAttribute("mechanics", mechanics);
        return "app/report/new";
    }

    @RequestMapping(value = {"/create"}, method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
    @Transactional
    @ResponseBody
    public String create(String name,
        String customerName,
        String documentCode,
        String informedCode,
        int productCategory,
        String vehicleModelName,
        String vehicleBodyId,
        String customerPhoneNo,
        String customerPhoneNo2,
        String customerPhoneNo3,
        Integer maintenanceNumber,
        String maintenanceDate,
        String serviceDate,
        String mechanicId,
        String mechanicName,
        Principal principal) throws ParseException {
        CustomerOrder co = new CustomerOrder();
        co.setName(name);
        co.setDocumentCode(documentCode);
        co.setInformedCode(informedCode);
        co.setProductCategory(productCategory);
        co.setVehicleBodyId(vehicleBodyId);
        co.setVehicleModelName(vehicleModelName);
        co.setCustomerName(customerName);
        co.setCustomerPhoneNo(customerPhoneNo);
        co.setCustomerPhoneNo2(customerPhoneNo2);
        co.setCustomerPhoneNo3(customerPhoneNo3);
        co.setMaintenanceNumber(maintenanceNumber);
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

        Date cMaintenanceDate = (Date) df.parse(maintenanceDate);
        co.setMaintenanceDate(cMaintenanceDate);
        Date cServiceDate = (Date) df.parse(serviceDate);
        co.setServiceDate(cServiceDate);

//      Set order number (orderNo)
        Long todayOrder = customerOrderDao.countByServiceDate(cServiceDate);
        co.setOrderNo((int) (todayOrder + 1));

//      find company by user
        String username = principal.getName();
        User user = userDao.findByUsername(username);
        Company company = companyDao.findByUsers(user);
        co.setCompany(company);

        if (mechanicName == null) {
            Mechanic mechanic = mechanicDao.findById(mechanicId);
            co.setMechanic(mechanic);
        } else {
            Mechanic mechanic = new Mechanic();
            mechanic.setName(mechanicName);
            mechanic.setCompany(company);
            mechanicDao.save(mechanic);
            co.setMechanic(mechanic);
        }

//      save
        customerOrderDao.save(co);

        return "success";
    }

    @RequestMapping(value = {"/{id}/delete"}, method = RequestMethod.POST)
    @Transactional
    @ResponseBody
    public String softDelete(@PathVariable String id) {
        CustomerOrder order = customerOrderDao.findById(id);
        activityService.logEntity(null, "delete", order);
        order.setDeleted(true);
        customerOrderDao.save(order);
        // log delete here
        return "success";
    }

    @RequestMapping(value = {"/{id}/edit"}, method = RequestMethod.POST)
    @Transactional
    @ResponseBody
    public String update(String name,
        String customerName,
        String documentCode,
        String informedCode,
        String vehicleModelName,
        int productCategory,
        String vehicleBodyId,
        String customerPhoneNo,
        String customerPhoneNo2,
        String customerPhoneNo3,
        Integer maintenanceNumber,
        String maintenanceDate,
        String serviceDate,
        @PathVariable String id) throws ParseException {
        CustomerOrder co = customerOrderDao.findById(id);
        co.setName(name);
        co.setDocumentCode(documentCode);
        co.setInformedCode(informedCode);
        co.setProductCategory(productCategory);
        co.setVehicleBodyId(vehicleBodyId);
        co.setVehicleModelName(vehicleModelName);
        co.setCustomerName(customerName);
        co.setCustomerPhoneNo(customerPhoneNo);
        co.setCustomerPhoneNo2(customerPhoneNo2);
        co.setCustomerPhoneNo3(customerPhoneNo3);
        co.setMaintenanceNumber(maintenanceNumber);
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Date cMaintenanceDate = (Date) df.parse(maintenanceDate);
        co.setMaintenanceDate(cMaintenanceDate);
        Date cServiceDate = (Date) df.parse(serviceDate);
        co.setServiceDate(cServiceDate);

//      save
        customerOrderDao.save(co);

        return "success";
    }

    /**
     *
     * @param model
     * @param principal
     * @param request
     * @param year
     * @param month is one-based
     * @param companyId
     * @return
     */
    @RequestMapping(value = "/status", method = RequestMethod.GET)
    public String statusSummary(ModelMap model, Principal principal, HttpServletRequest request,
        @RequestParam(required = false, name = "year") Integer year,
        @RequestParam(required = false, name = "month") Integer month,
        @RequestParam(required = false, name = "companyId") String companyId) {

        model.addAttribute("mainMenu", "status_summary");
        model.addAttribute("pageHeaderBig", "Status report");
        model.addAttribute("pageHeaderSmall", "report of all calling status");

        // generate list of selectable years for frontend
        final int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        List<Integer> years = new ArrayList<>();
        for (int i = currentYear; i >= 2016; i--) {
            years.add(i);
        }
        model.addAttribute("years", years);

        // map "headers" months list
        // gen month headers
        final String[] knwonMonths = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
        int currentMonth = Calendar.getInstance().get(Calendar.MONTH) + 1;
        int endMonth = (month != null && 1 <= month && month <= 12) ? month : currentMonth;
        int startMonth = endMonth % 12;
        List<String> months = new ArrayList<>();
        for (int i = 0; i < 12; i++) {
            months.add(knwonMonths[(startMonth + i) % 12]);
        }
        List<String> headers = new ArrayList<>();
        headers.addAll(months);
        headers.add("Total");
        model.addAttribute("headers", headers);

        // get end date
        int endYear = (year != null && year <= currentYear && year >= 2016) ? year : currentYear;
        String endDateStr = endYear + "-" + ((endMonth % 12) + 1) + "-01";
        Date endDate = new Date();
        try {
            endDate = DateUtils.parseDate(endDateStr, "yyyy-MM-dd");
        } catch (ParseException ex) {
        }

        // get start date
        // find reports between start and end dates (and in companies)
        // group report by month and status
        // map "statuses" status list
        // map "statusxxx" counts list
        // map total counts list
        // get user compay id
        List<Company> accessibleCompanies = new ArrayList<>();
        if (request.isUserInRole("ROLE_USER")) {
            String username = principal.getName();
            List<Company> cs = companyDao.findByUsersUsername(username);
            if (cs != null) {
                accessibleCompanies.addAll(cs);
            }
        } else if (request.isUserInRole("ROLE_ADMIN") || request.isUserInRole("ROLE_EXECUTIVE")) {
            List<Company> cs = IteratorUtils.toList(companyDao.findAll().iterator());
            accessibleCompanies.addAll(cs);
        }
        model.addAttribute("companies", accessibleCompanies);

        // make set of accessiable Ids
        Set<String> accessibleCompanyIds = accessibleCompanies.stream().map((t) -> {
            return t.getId();
        }).collect(Collectors.toSet());

        Set<String> interestedCompanyIds = new HashSet<>();
        List<Company> interestedCompanies = new ArrayList<>();
        if (companyId != null && accessibleCompanyIds.contains(companyId)) {
            interestedCompanyIds.add(companyId);
            interestedCompanies.add(companyDao.findOne(companyId));
        } else {
            interestedCompanyIds.addAll(accessibleCompanyIds);
            interestedCompanies.addAll(accessibleCompanies);
        }
        // map of month, (status, count)
        Map<String, Map<String, Long>> monthCounts = new HashMap<>();
        List<String> dateHeaders = new ArrayList<>();
        Date oldestDate = null;
        try {
            oldestDate = DateUtils.addDays(DateUtils.parseDate("2017-03-01", "yyyy-MM-dd"), -1);
        } catch (ParseException ex) {
        }
        for (int i = 0; i < 12; i++) {
            Date startSessionDate = DateUtils.addMonths(endDate, -12 + i);
            Date endSesssionDate = DateUtils.addMonths(endDate, -11 + i);
            List<CustomerOrder> orders = new ArrayList<>();
            if (startSessionDate.after(oldestDate)) {
                orders = customerOrderDao.findByCompanyInAndCreatedBetweenAndDeletedFalse(
                    interestedCompanies, startSessionDate, endSesssionDate);
            }
            // group by and count
            Map<String, Long> statusCounts = orders.stream().collect(
                Collectors.groupingBy(CustomerOrder::getStatus, Collectors.counting())
            );
            dateHeaders.add(DateFormatUtils.format(startSessionDate, "MMM yyy"));

            // create a result map that contains only those in KNOWN_STATUSES
            Map<String, Long> finalStatusCounts = XLSXOrderStatusReader.KNOWN_STATUSES.stream().collect(
                Collectors.toMap(status -> status, status -> statusCounts.containsKey(status) ? statusCounts.get(status) : 0l)
            );
            finalStatusCounts.put("Total", (long) finalStatusCounts.values().stream().mapToInt(Number::intValue).sum());
            monthCounts.put(months.get(i), finalStatusCounts);
        }
        dateHeaders.add("Total");
        model.addAttribute("dateHeaders", dateHeaders);
        Map<String, Long> totalCounts = new HashMap<>();
        long grandTotal = 0;
        for (String status : XLSXOrderStatusReader.KNOWN_STATUSES) {
            long c = 0;
            for (String m : months) {
                c += monthCounts.get(m).get(status);
            }
            grandTotal += c;
            totalCounts.put(status, c);
        }
        totalCounts.put("Total", grandTotal);
        monthCounts.put("Total", totalCounts);

        model.addAttribute("data", monthCounts);

        List<String> rows = XLSXOrderStatusReader.KNOWN_STATUSES.stream().collect(Collectors.toList());
        rows.add("Total");
        model.addAttribute("rows", rows);

        Date startTitleDate = DateUtils.addMonths(endDate, -12);
        Date endTitleDate = DateUtils.addDays(endDate, -1);

        String title = DateFormatUtils.format(startTitleDate, "dd MMM yyyy") + " - "
            + DateFormatUtils.format(endTitleDate, "dd MMM yyyy");
        model.addAttribute("title", title);

        Map<String, String> params = new HashMap<String, String>();
        params.put("companyId", companyId);
        params.put("month", Integer.toString(endMonth));
        params.put("year", Integer.toString(endYear));
        model.addAttribute("params", params);

        // generate map of reports
        return "app/report/statusSummary";
    }

    @RequestMapping(value = "/upload_status", method = RequestMethod.GET)
    public String uploadStatusView(@RequestParam(required = false, name = "success") boolean success,
        @RequestParam(required = false, name = "successCount") Integer successCount,
        @RequestParam(required = false, name = "failureCount") Integer failureCount,
        ModelMap model) {
        model.put("success", success);
        model.put("successCount", successCount);
        model.put("failureCount", failureCount);
        return "app/report/uploadStatus";
    }

    @RequestMapping(value = "/upload_status", method = RequestMethod.POST)
    @Transactional
    public String uploadStatus(@RequestParam(required = false, name = "file") MultipartFile file, ModelMap model) {
        InputStream inputStream = null;
        List<String> errors = new ArrayList<>();
        try {
            inputStream = file.getInputStream();
            XLSXOrderStatusReader xlsxOrderStatusReader = new XLSXOrderStatusReader(inputStream);
            errors.addAll(xlsxOrderStatusReader.getErrors());
            if (errors.isEmpty()) {
                int successCount = 0;
                int failureCount = 0;
                for (int i = 0; i < xlsxOrderStatusReader.size(); i++) {
                    String id = xlsxOrderStatusReader.getId(i);
                    CustomerOrder customerOrder = customerOrderDao.findOne(id);
                    if (null != customerOrder) {
                        successCount++;
                        customerOrder.setCallingDate(xlsxOrderStatusReader.getCallingDate(i));
                        customerOrder.setNote(xlsxOrderStatusReader.getNote(i));
                        customerOrder.setStatus(xlsxOrderStatusReader.getStatus(i));
                    } else {
                        failureCount++;
                    }
                }

                return "redirect:/report/upload_status?success=true&successCount=" + successCount + "&failureCount=" + failureCount;
            }
        } catch (IOException ex) {
            errors.add("Unable to read the uploaded file.");
        } catch (Exception ex) {
            errors.add("Unknown exception.");
        } finally {
            IOUtils.closeQuietly(inputStream);
        }
        model.put("errors", errors);
        return "app/report/uploadStatus";
    }

    @Autowired
    private CustomerVoiceImporterService csiRawDataService;

    @RequestMapping(value = "/upload_csi", method = RequestMethod.GET)
    public String uploadCsiView(@RequestParam(required = false, name = "success") boolean success,
        @RequestParam(required = false, name = "successCount") Integer successCount,
        @RequestParam(required = false, name = "failureCount") Integer failureCount,
        ModelMap model) {
        model.put("success", success);
        model.put("successCount", successCount);
        model.put("failureCount", failureCount);
        return "app/report/uploadCsi";
    }

    @RequestMapping(value = "/upload_csi", method = RequestMethod.POST)
    @Transactional
    public String uploadCsi(@RequestParam(required = false, name = "file") MultipartFile file, ModelMap model) {
        InputStream inputStream = null;
        List<String> errors = new ArrayList<>();
        try {
            inputStream = file.getInputStream();
            csiRawDataService.importRawData(inputStream);
        } catch (IOException ex) {
            errors.add("Unable to read the uploaded file.");
        } catch (Exception ex) {
            errors.add("Unknown exception.");
        } finally {
            IOUtils.closeQuietly(inputStream);
        }
        model.put("errors", errors);
        return "app/report/uploadCsi";
    }

}
