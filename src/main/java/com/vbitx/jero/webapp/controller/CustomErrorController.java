/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vbitx.jero.webapp.controller;

import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.ErrorAttributes;
import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.ServletRequestAttributes;

@Controller
public class CustomErrorController implements ErrorController {

    private static final String PATH = "/error";

    private boolean debug = true;

    @Autowired
    private ErrorAttributes errorAttributes;

    @RequestMapping(value = {"/Access_Denied", PATH})
    public String error(HttpServletRequest request, HttpServletResponse response, Model model) {
        // new ErrorJson(response.getStatus(), getErrorAttributes(request, debug));
        //return response.getStatus() + "";
        model.addAllAttributes(getErrorAttributes(request, debug));
        return "/error";
    }

    @Override
    public String getErrorPath() {
        return PATH;
    }

    private Map<String, Object> getErrorAttributes(HttpServletRequest request, boolean includeStackTrace) {
        RequestAttributes requestAttributes = new ServletRequestAttributes(request);
        Map<String, Object> errorAttributes1 = errorAttributes.getErrorAttributes(requestAttributes, includeStackTrace);
        for (Map.Entry<String, Object> entry : errorAttributes1.entrySet()) {
            String key = entry.getKey();
            Object value = entry.getValue();
        }
        return  errorAttributes1;
    }

}
