package com.vbitx.jero.webapp.controller;

import com.vbitx.jero.api.activity.Activity;
import com.vbitx.jero.api.activity.ActivityDao;
import com.vbitx.jero.api.customerorder.CustomerOrderDao;
import com.vbitx.jero.api.user.User;
import com.vbitx.jero.api.user.UserDao;
import com.vbitx.jero.api.user.profile.UserProfile;
import com.vbitx.jero.api.user.profile.UserProfileDao;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by gigadot on 11-Dec-16.
 */
@Controller
@RequestMapping("/history")
public class HistoryController {

    @Autowired
    private ActivityDao activityRepository;

    @Autowired
    private UserDao userRepository;

    @Autowired
    private UserProfileDao userProfileRepository;
    
    @Autowired CustomerOrderDao customerOrderRepository;

    @RequestMapping(value = {"", "/"})
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
    public String index(ModelMap map) {
        List<Activity> activities = activityRepository.findByActionAndCreatedGreaterThanEqualOrderByCreatedDesc("delete", DateUtils.addYears(new Date(), -1));
        List<Map> acts = new ArrayList<>();
        Map<String, Map<String, String>> users = new HashMap<>();
        for (Activity activity : activities) {
            Map<String, Object> act = new HashMap<>();
            act.put("deletedDate", activity.getCreated());
            Map<String, String> user = new HashMap<>();
            if (users.containsKey(activity.getCreatedBy())) {
                user = users.get(activity.getCreatedBy());
            } else {
                User u = userRepository.findById(activity.getCreatedBy());
                if (u != null) {
                    UserProfile up = userProfileRepository.findByUser(u);
                    user.put("id", activity.getCreatedBy());
                    user.put("email", u.getEmail());
                    user.put("username", u.getUsername());
                    user.put("firstname", up.getFirstname());
                    user.put("lastname", up.getFullName());
                    user.put("fullname", up.getLastname());
                    users.put(activity.getCreatedBy(), user);
                } else {
                }
            }
            act.put("deletedBy", user);
            act.put("report", customerOrderRepository.findOne(activity.getObjectId()));
            acts.add(act);
        }
        map.addAttribute("activities", acts);
        map.addAttribute("mainMenu", "history");
        return "app/history/list";
    }

}
