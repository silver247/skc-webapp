/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vbitx.jero.webapp.controller;

import com.vbitx.jero.api.mechanic.Mechanic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import com.vbitx.jero.api.mechanic.MechanicRepository;

/**
 *
 * @author sun
 */
@Controller
@RequestMapping("/mechanic")
public class MechanicController {
    
    @Autowired
    private MechanicRepository mechanicDao;
    
    @RequestMapping(value = {"/{id}/edit"}, method = RequestMethod.POST)
    @Transactional
    @ResponseBody
    public String update(@PathVariable String id, String name) {
        Mechanic mechanic = mechanicDao.findById(id);
        mechanic.setName(name);
        mechanicDao.save(mechanic);
        return "success";
    }
    
    @RequestMapping(value = {"/{id}/toggle_active"}, method = RequestMethod.POST)
    @Transactional
    @ResponseBody
    public String toggleActive(@PathVariable String id){
        Mechanic mechanic = mechanicDao.findById(id);
        mechanic.setActive(!mechanic.isActive());
        mechanicDao.save(mechanic);
        return "success";
    }
    
}
