package com.vbitx.jero.webapp.controller;

import com.vbitx.jero.api.authority.Authority;
import com.vbitx.jero.api.authority.AuthorityDao;
import com.vbitx.jero.api.company.Company;
import com.vbitx.jero.api.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import com.vbitx.jero.api.user.UserDao;
import com.vbitx.jero.api.user.authority.UserAuthority;
import com.vbitx.jero.api.user.authority.UserAuthorityDao;
import com.vbitx.jero.api.user.credential.UserCredential;
import com.vbitx.jero.api.user.credential.UserCredentialDao;
import com.vbitx.jero.api.user.credential.UserCredentialService;
import com.vbitx.jero.api.user.profile.UserProfile;
import com.vbitx.jero.api.user.profile.UserProfileDao;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import com.vbitx.jero.api.company.CompanyRepository;

@Controller
@RequestMapping(value = {"/user"})
@PreAuthorize("!isAnonymous()")
public class UserController {

    @Autowired
    private UserDao userDao;

    @Autowired
    private UserProfileDao userProfileDao;

    @Autowired
    private UserCredentialService userCredentialService;

    @Autowired
    private UserCredentialDao userCredentialDao;

    @Autowired
    private CompanyRepository companyDao;

    @Autowired
    private UserAuthorityDao userAuthorityDao;

    @Autowired
    private AuthorityDao authorityDao;

    @RequestMapping(value = {"/new"}, method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public String newUser(ModelMap model) {
        model.addAttribute("mainMenu", "user");
        model.addAttribute("pageHeaderBig", "User");
        model.addAttribute("pageHeaderSmall", "new account");
//      Need implementation  .s

        Iterable<Company> companiesIt = companyDao.findAll();
        List<Company> companies = new ArrayList<>();
        for (Iterator<Company> iterator = companiesIt.iterator(); iterator.hasNext();) {
            companies.add(iterator.next());
        }

        List<Authority> authorities = authorityDao.findAll();

        model.addAttribute("authorities", authorities);
        model.addAttribute("companies", companies);
        return "app/user/new";
    }

    @RequestMapping(value = {"/", ""}, method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public String listAllUsers(ModelMap model) {
        model.addAttribute("mainMenu", "user");
        model.addAttribute("pageHeaderBig", "User");
        model.addAttribute("pageHeaderSmall", "list all users");
        Iterable<UserProfile> userProfileIterable = userProfileDao.findAll();
        List<UserProfile> userProfiles = new ArrayList<>();
        for (Iterator<UserProfile> iterator = userProfileIterable.iterator(); iterator.hasNext();) {
            userProfiles.add(iterator.next());
        }
        model.addAttribute("userProfiles", userProfiles);
        return "app/user/list";
    }

    @RequestMapping(value = {"/me/profile"}, method = RequestMethod.GET)
    public String profile(ModelMap model, Principal principal) {
        model.addAttribute("mainMenu", "profile");
        model.addAttribute("pageHeaderBig", "My Profile");
        model.addAttribute("pageHeaderSmall", "user profile page");

        String username = principal.getName();
        User user = userDao.findByUsername(username);
        UserProfile userprofile = userProfileDao.findByUser(user);
        Company company = companyDao.findByUsers(user);
        UserAuthority userAuthority = userAuthorityDao.findByUser(user).get(0);

        model.addAttribute("authority", userAuthority.getAuthority());
        model.addAttribute("company", company);
        model.addAttribute("userProfile", userprofile);
        model.addAttribute("user", user);
        model.addAttribute("profile", true);
        return "app/user/user-profile";
    }

    @RequestMapping(value = {"/{id}"}, method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public String userProfile(@PathVariable String id, ModelMap model) {
        model.addAttribute("mainMenu", "profile");
        model.addAttribute("pageHeaderBig", "User Profile");
        model.addAttribute("pageHeaderSmall", "user profile");

        User user = userDao.findById(id);
        if (user == null) {
            return "redirect:/user";
        }
        UserProfile userprofile = userProfileDao.findByUser(user);

        Company company = companyDao.findByUsers(user);

        UserAuthority userAuthority = userAuthorityDao.findByUser(user).get(0);

        model.addAttribute("authority", userAuthority.getAuthority());
        model.addAttribute("company", company);
        model.addAttribute("userProfile", userprofile);
        model.addAttribute("user", user);
        model.addAttribute("profile", false);
        return "app/user/user-profile";
    }

    @RequestMapping(value = {"/{id}/change_password"}, method = RequestMethod.GET)
    public String changePassword(@PathVariable String id, Principal principal, ModelMap model) {
        User user = userDao.findById(id);
        if (user == null) {
            return "redirect:/user";
        }
        UserProfile userprofile = userProfileDao.findByUser(user);

        model.addAttribute("mainMenu", "user");
        if (principal.getName().equals(user.getUsername())) {
            model.addAttribute("pageHeaderBig", "My profile");
        } else {
            model.addAttribute("pageHeaderBig", userprofile.getFirstname() + "'s profile");
        }
        model.addAttribute("pageHeaderSmall", "change password");
        model.addAttribute("user", user);

        return "app/user/change-password";
    }

    @RequestMapping(value = {"/{id}/change_password"}, method = RequestMethod.POST)
    @Transactional
    @ResponseBody
    public String changePasswordPost(@PathVariable String id, String password) {
        User user = userDao.findById(id);
        if (user == null) {
            return "redirect:/user";
        }
        String salt = userCredentialService.generateSalt();
        UserCredential userCredential = userCredentialDao.findByUser(user);
        userCredential.setSalt(salt);
        userCredential.setHashedPassword(userCredentialService.encryptPassword(password, salt));
        userCredentialDao.save(userCredential);
        return "success";
    }

    @RequestMapping(value = {"/me/profile/edit"}, method = RequestMethod.GET)
    public String profileEdit(ModelMap model, Principal principal) {
        model.addAttribute("mainMenu", "profile");
        model.addAttribute("pageHeaderBig", "My Profile");
        model.addAttribute("pageHeaderSmall", "edit profile");

        String username = principal.getName();
        User user = userDao.findByUsername(username);
        UserProfile userprofile = userProfileDao.findByUser(user);
        Company company = companyDao.findByUsers(user);
        UserAuthority userAuthority = userAuthorityDao.findByUser(user).get(0);

        List<Company> companies = new ArrayList<>();
        companyDao.findAll().iterator().forEachRemaining(companies::add);
        
        List<Authority> authorities = authorityDao.findAll();

        model.addAttribute("userAuthority", userAuthority);
        model.addAttribute("authorities", authorities);
        model.addAttribute("companies", companies);
        model.addAttribute("company", company);
        model.addAttribute("user", user);
        model.addAttribute("userProfile", userprofile);
        model.addAttribute("edit", true);
        return "app/user/edit";
    }

    @RequestMapping(value = {"/{id}/edit"}, method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public String editUser(@PathVariable String id, ModelMap model) {
        User user = userDao.findOne(id);
        if (user == null) {
            return "redirect:/user";
        }
        UserProfile userprofile = userProfileDao.findByUser(user);
        String name = userprofile.getFirstname() + " " + userprofile.getLastname();
        model.addAttribute("mainMenu", "user");
        model.addAttribute("pageHeaderBig", name);
        model.addAttribute("pageHeaderSmallll", "Edit user");

        Company company = companyDao.findByUsers(user);
        List<Company> companies = new ArrayList<>();
        companyDao.findAll().iterator().forEachRemaining(companies::add);
        List<Authority> authorities = authorityDao.findAll();
        List<UserAuthority> ua = userAuthorityDao.findByUser(user);

        model.addAttribute("userAuthority", ua.get(0));
        model.addAttribute("authorities", authorities);
        model.addAttribute("companies", companies);
        model.addAttribute("company", company);
        model.addAttribute("user", user);
        model.addAttribute("userProfile", userprofile);
        model.addAttribute("edit", true);
        return "app/user/edit";
    }

    @RequestMapping(value = {"/{id}/edit"}, method = RequestMethod.POST)
    @Transactional
    public String updateUser(Principal principal, String firstname, String lastname, String username, String email, String companyId, String authorityId, ModelMap model, @PathVariable String id) {
        User user = userDao.findById(id);
        UserProfile userProfile = userProfileDao.findByUser(user);
        user.setUsername(username);
        user.setEmail(email);
        userProfile.setFirstname(firstname);
        userProfile.setLastname(lastname);
        userDao.save(user);
        userProfileDao.save(userProfile);

        if (companyId == null || authorityId == null) {
            return "redirect:/user/me/profile";
        }

        Company company = companyDao.findByUsers(user);
        Set<User> users = company.getUsers();
        for (Iterator<User> i = users.iterator(); i.hasNext();) {
            User tmpUser = i.next();
            if (tmpUser.getId().equals(user.getId())) {
                i.remove();
            }
        }

        Set<User> newusers = companyDao.findById(companyId).getUsers();
        newusers.add(user);

        UserAuthority ua = userAuthorityDao.findByUser(user).get(0);
        userAuthorityDao.delete(ua);

        UserAuthority adminAuthority = new UserAuthority(user, authorityDao.findById(authorityId));
        userAuthorityDao.save(adminAuthority);

        String ausername = principal.getName();
        User auser = userDao.findByUsername(ausername);
        if (auser.equals(user)) {
            return "redirect:/user/me/profile";
        } else {
            return "redirect:/user/" + user.getId();
        }

    }

    @RequestMapping(value = {"/create"}, method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @Transactional
    public String create(String email, String username, String firstname, String lastname, String password, String companyId, String authorityId) {
        User user = new User(username, email);
        userDao.save(user);

        String salt = userCredentialService.generateSalt();
        UserCredential userCredential = new UserCredential();
        userCredential.setUser(user);
        userCredential.setSalt(salt);
        userCredential.setHashedPassword(userCredentialService.encryptPassword(password, salt));
        userCredentialDao.save(userCredential);

        UserProfile userProfile = new UserProfile();
        userProfile.setFirstname(firstname);
        userProfile.setLastname(lastname);
        userProfile.setUser(user);
        userProfileDao.save(userProfile);

        Company company = companyDao.findById(companyId);
        Set<User> users = company.getUsers();
        users.add(user);

        UserAuthority adminAuthority = new UserAuthority(user, authorityDao.findById(authorityId));
        userAuthorityDao.save(adminAuthority);

        return "redirect:/user";
    }

//    @RequestMapping("/delete")
//    @ResponseBody
//    public String delete(String id) {
//        try {
//            User user = new User(id);
//            userDao.delete(user);
//        } catch (Exception ex) {
//            return "Error deleting the user:" + ex.toString();
//        }
//        return "User succesfully deleted!";
//    }
//    @RequestMapping("/get-by-email")
//    @ResponseBody
//    public String getByEmail(String email) {
//        String userId;
//        try {
//            User user = userDao.findByEmail(email);
//            userId = String.valueOf(user.getId());
//        } catch (Exception ex) {
//            return "User not found";
//        }
//        return "The user id is: " + userId;
//    }
}
