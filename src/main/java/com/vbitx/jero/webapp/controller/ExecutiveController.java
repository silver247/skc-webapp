/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vbitx.jero.webapp.controller;

import com.vbitx.jero.api.company.Company;
import com.vbitx.jero.api.mechanic.Mechanic;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import com.vbitx.jero.api.company.CompanyRepository;
import com.vbitx.jero.api.mechanic.MechanicRepository;
import com.vbitx.jero.skc.voice.MessyCustomerVoiceIndexes;
import com.vbitx.jero.skc.voice.evaluator.AnnualCsiEvalutoreService;
import com.vbitx.jero.skc.voice.evaluator.TopMonthCsiEvaluatorService;
import java.util.ArrayList;
import java.util.stream.Collectors;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import static com.vbitx.jero.skc.voice.MessyCustomerVoiceIndexes.IndexTitleMap;
import com.vbitx.jero.skc.voice.evaluator.CsiDetailsEvaluator;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;

@Controller
@RequestMapping("/executive")
public class ExecutiveController {

    @Autowired
    private MechanicRepository mechanicRepository;

    @Autowired
    private CompanyRepository companyRepository;

    @RequestMapping(value = {"", "/"}, method = RequestMethod.GET)
    public String home(Model model) {
        model.addAttribute("mainMenu", "executive");
        model.addAttribute("pageHeaderBig", "Executive");
        model.addAttribute("pageHeaderSmall", "Executive view");
        model.addAttribute("pageHeaderBig", "Executive");
        return "app/executive/executiveHome";
    }

    @GetMapping(value = {"/{voiceType:negative|positive|suggestion|complaint}"})
    public String csiPage(Model model, @PathVariable(required = true) String voiceType) {
        model.addAttribute("mainMenu", "executive");
        model.addAttribute("pageHeaderBig", "Executive Highlights / " + StringUtils.capitalize(voiceType));
        model.addAttribute("voiceType", voiceType);

        List<Integer> years = Arrays.asList(2017, 2016, 2015);
        model.addAttribute("years", years);

        List<Integer> chkNumbers = Arrays.asList(1, 2, 3);
        model.addAttribute("chkNumbers", chkNumbers);

        List<Mechanic> mechanics = mechanicRepository.findAll();
        model.addAttribute("mechanics", mechanics);

        List<Company> companies = companyRepository.findAllByOrderByName();
        companies = companies.stream().filter((t) -> {
            return !StringUtils.isBlank(t.getName()) && !(t.getName().equals("ABL")
                || t.getName().equals("Kubota")
                || t.getName().equals("vBit")
                || t.getName().equals("TEST"));
        }).collect(Collectors.toList());
        model.addAttribute("companies", companies);

        Object voices = MessyCustomerVoiceIndexes.IndexTitleMap.get(voiceType).entrySet().stream().map((t) -> {
            return MapUtils.putAll(new HashMap(), ArrayUtils.toArray(
                "vid", t.getKey(),
                "title", t.getValue()
            ));
        }).collect(Collectors.toList());
        model.addAttribute("voices", voices);

        return "app/executive/executiveVoices";
    }

    @GetMapping(value = {"/csi_home"})
    @ResponseBody
    public Map csiHome() {
        return annualCsiEvalutoreService.currentMonthMiniCsiByYear();
    }

    @Autowired
    private AnnualCsiEvalutoreService annualCsiEvalutoreService;

    @Autowired
    private TopMonthCsiEvaluatorService topMonthCsiEvaluatorService;
    
    @Autowired
    private CsiDetailsEvaluator csiDetailsEvaluator;

    @RequestMapping(value = {"/csi_{voiceType:negative|positive|suggestion|complaint}"})
    @ResponseBody
    public Map csiAjax(
        @RequestParam(required = false, value = "year") Integer year,
        @RequestParam(required = false, value = "companies[]") String[] companies,
        @RequestParam(required = false, value = "chkNumbers[]") List<Integer> chkNumbers,
        @RequestParam(required = false, value = "mechanics[]") String[] mechanics,
        @RequestParam(required = false, value = "voices[]") List<Integer> voices,
        @PathVariable(required = true) String voiceType) {

        List<String> companyIds = (companies != null) ? Arrays.asList(companies) : new ArrayList<>();

        List<String> mechanicNames = null;
        if (mechanics != null) {
            mechanicNames = new ArrayList<>();
            for (String mechanicId : mechanics) {
                Mechanic mechanic = mechanicRepository.findOne(mechanicId);
                if (mechanic != null) {
                    mechanicNames.add(mechanic.getName());
                }
            }
        }

        return topMonthCsiEvaluatorService.csi(year, companyIds, chkNumbers, mechanicNames, voices, voiceType);
    }

    @RequestMapping(value = {"/csi/people/{voiceType:negative|positive|suggestion|complaint}"})
    @ResponseBody
    public Map people(
        @RequestParam(required = false, value = "year") Integer year,
        @RequestParam(required = false, value = "companies[]") String[] companies,
        @RequestParam(required = false, value = "chkNumbers[]") List<Integer> chkNumbers,
        @RequestParam(required = false, value = "mechanics[]") String[] mechanics,
        @RequestParam(required = false, value = "month") Integer month,
        @RequestParam(required = false, value = "voiceValue") Integer voiceValue,
        @PathVariable(required = true) String voiceType) {
        
        List<String> companyIds = (companies != null) ? Arrays.asList(companies) : new ArrayList<>();
        
        List<String> mechanicNames = null;
        if (mechanics != null) {
            mechanicNames = new ArrayList<>();
            for (String mechanicId : mechanics) {
                Mechanic mechanic = mechanicRepository.findOne(mechanicId);
                if (mechanic != null) {
                    mechanicNames.add(mechanic.getName());
                }
            }
        }

        return csiDetailsEvaluator.people(year, companyIds, chkNumbers, mechanicNames, month, voiceValue, voiceType);
    }

}
