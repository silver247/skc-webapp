/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vbitx.jero.webapp.advice;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

@ControllerAdvice
public class AppDataControllerAdvice {

    @ModelAttribute
    public void addUserToModel(Model model,
        @Value("${env.language.thrid.lang}") String thridLangLang,
        @Value("${env.language.thrid.abbr}") String thridLangAbbr,
        @Value("${env.language.thrid.language}") String thridLangLanguage) {
        try {
            model.addAttribute("thridLangLang", thridLangLang);
            model.addAttribute("thridLangAbbr", thridLangAbbr);
            model.addAttribute("thridLangLanguage", thridLangLanguage);
        } catch (Exception ex) {

        }
    }

}
