/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vbitx.jero.skc;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author gigadot
 */
public class PairCounterTest {
    
    public PairCounterTest() {
    }

    @Test
    public void testAdd() {
        ImmutablePair<String, Integer> i1 = new ImmutablePair<>("xxx", 1);
        ImmutablePair<String, Integer> i2 = new ImmutablePair<>(new String("xxx"), new Integer(1));
        assertEquals(i1, i2);
    }
    
}
