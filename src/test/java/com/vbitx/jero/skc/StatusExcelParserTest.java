/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vbitx.jero.skc;

import com.vbitx.jero.skc.voice.parser.XLSXOrderStatusReader;
import java.io.InputStream;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;

/**
 *
 * @author gigadot
 */
public class StatusExcelParserTest {

    @Test
    @Ignore
    public void testParse() {
        InputStream xlsxInputStream = StatusExcelParserTest.class.getResourceAsStream("status.xlsx");

        XLSXOrderStatusReader xlsxOrderStatusReader = new XLSXOrderStatusReader(xlsxInputStream);
        //xlsxOrderStatusReader.hasErrors();
        //xlsxOrderStatusReader.print();
    }

    @Test
    @Ignore
    public void testNull() {
        try {
            XLSXOrderStatusReader.KNOWN_STATUSES.contains(null);
        } catch (NullPointerException npe) {
            fail();
        }
    }

}
