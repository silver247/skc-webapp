/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vbitx.jero.skc;

import com.vbitx.jero.skc.voice.parser.CustomerVoiceImporterService;
import java.io.InputStream;
import java.util.Map;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 *
 * @author gigadot
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {XlsxCsiRawDataReaderTest.class})
//@Configuration
@EnableAutoConfiguration
@EntityScan(basePackages = {"com.vbitx.jero.skc"})
@ComponentScan(basePackages = {"com.vbitx.jero.skc", "com.vbitx.jero.api.auditor"})
@EnableJpaAuditing
@EnableJpaRepositories("com.vbitx.jero.skc")
@EnableTransactionManagement
public class XlsxCsiRawDataReaderTest {
    
    @Autowired
    private CustomerVoiceImporterService csiRawDataService;

    @Test
    @Ignore
    public void testSize() {
        InputStream xlsxInputStream = XlsxCsiRawDataReaderTest.class.getResourceAsStream("skc-csi.xlsx");
//        XlsxCsiRawDataReader xlsxCsiRawDataReader = new XlsxCsiRawDataReader(xlsxInputStream);
//        System.out.println(xlsxCsiRawDataReader.size());
//        xlsxCsiRawDataReader.print();
        Map<String, Object> m = csiRawDataService.importRawData(xlsxInputStream);
        System.out.println(m);
    }

    @Test
    @Ignore
    public void testPrint() {

    }

    public static void main(String[] args) {
        ApplicationContext ctx = SpringApplication.run(XlsxCsiRawDataReaderTest.class, args);
    }

}
